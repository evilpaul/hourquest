#!/bin/sh

BRANCH=`git branch --show-current`
TAG=`git describe --tags --always`
DATE=`date`

PACKAGE_NAME=hourquest_${BRANCH}_${TAG}
ROM_NAME=${PACKAGE_NAME}.rom
TAP_NAME=${PACKAGE_NAME}.tap
OUTPUT_ARCHIVE=${PACKAGE_NAME}/${PACKAGE_NAME}.zip

echo [] Making release package...
echo Branch: ${BRANCH}
echo Tag: ${TAG}
echo Package name: ${PACKAGE_NAME}
echo Output: ${OUTPUT_ARCHIVE}

make clean
make all
rm -rf ${PACKAGE_NAME}
mkdir ${PACKAGE_NAME}
cp build/hourquest.rom ${PACKAGE_NAME}/${ROM_NAME}
cp build/hourquest.tap ${PACKAGE_NAME}/${TAP_NAME}

cat << EOF > ${PACKAGE_NAME}/instructions.txt
Hour Quest
----------

Hour Quest is a prototype of a Zelda-like RPG for the 16k ZX Spectrum using an Interface 2 ROM cartridge

Keys to play the game:
- Up/down/left/right = QAOP
- A button = N
- B button = M
- MENU button = ENTER

To load the cartridge in Fuse emulator:
- First, go to Preferences->Inputs and tick Interface 2
- Go to machine and select Spectrum 16k
- You can now open ${ROM_NAME} from the build directory

The build is also supplied as a stand-alone .tap file that can be loaded into a 48k Spectrum

For more info:
- @evilpaul_atebit
- evilpaul@evilpaul.org
- https://bitbucket.org/evilpaul/hourquest
- https://spectrumcomputing.co.uk/forums/viewtopic.php?p=75537

This build is:
- Branch ${BRANCH}
- Tag ${TAG}
- Built on ${DATE}

EOF

echo [] Making archive...
zip ${OUTPUT_ARCHIVE} ${PACKAGE_NAME}/${ROM_NAME} ${PACKAGE_NAME}/${TAP_NAME} ${PACKAGE_NAME}/instructions.txt

echo [] Done!
echo Archive is at ${OUTPUT_ARCHIVE}