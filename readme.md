Hour Quest is a prototype of a Zelda-like RPG for the 16k ZX Spectrum using an Interface 2 ROM cartridge.

# Building
The projects currently only supports macOS on x86 platforms. It uses a makefile, so just clone the repo and then type `make` from the root directory. This will create a rom cartridge build of the project - see below for how to run that. Alternatively, you can also build a standard .tap version (use `make ram`) of the project.

At some point I'll add Windows build support.

Instead of building, you can just download pre-built binaries of the latest release from https://bitbucket.org/evilpaul/hourquest/downloads/.


# Running
To load the cartridge in Fuse emulator:
- First, go to Preferences->Inputs and tick Interface 2.
- Go to machine and select Spectrum 16k.
- You can now open main.rom from the build directory.


# Keys to play the game
- Up/down/left/right = QAOP
- A button = N
- B button = M
- MENU button = ENTER


# For more info
- @evilpaul_atebit
- evilpaul@evilpaul.org
- https://bitbucket.org/evilpaul/hourquest
- https://spectrumcomputing.co.uk/forums/viewtopic.php?p=75537
