DELETE = rm -f
PYTHON3 = pypy3
MKDIR = mkdir
SJASMPLUS = bin/macos/sjasmplus
SJASMPLUS_FLAGS = --nofakes

BIN2TAP = ${PYTHON3} bin/python/bin2tap.py
CUTTER = ${PYTHON3} bin/python/cutter.py
TILER = ${PYTHON3} bin/python/tiler.py
SCRIPTGEN = ${PYTHON3} bin/python/scriptgen.py

BUILD_DRECTORY = build

APP_NAME = hourquest
ROM_NAME = ${APP_NAME}.rom
RAM_NAME = main.ram
RAM_TAP_NAME = main.ram.tap
RAM_FINAL_NAME = ${APP_NAME}.tap

DATA_DRECTORY = data
GFX_DRECTORY = gfx
WORLD_DRECTORY = world
SCRIPT_DRECTORY = scripts
SRC_DRECTORY = src

.DELETE_ON_ERROR :
.PHONY : rom ram all clean
default : rom

rom : ${BUILD_DRECTORY} ${BUILD_DRECTORY}/${ROM_NAME}
ram : ${BUILD_DRECTORY} ${BUILD_DRECTORY}/${RAM_FINAL_NAME}
all : rom ram

clean :
	$(DELETE) ${BUILD_DRECTORY}/*

${BUILD_DRECTORY} :
	${MKDIR} ${BUILD_DRECTORY}

${BUILD_DRECTORY}/%.bin : ${GFX_DRECTORY}/%.png
	${CUTTER} $< $@

${BUILD_DRECTORY}/tile*.bin ${BUILD_DRECTORY}/scripts.bin : ${GFX_DRECTORY}/tileset.png ${GFX_DRECTORY}/tilecollision.png ${WORLD_DRECTORY}/world.ldtk ${SCRIPT_DRECTORY}/defines.txt
	${TILER} ${GFX_DRECTORY}/tileset.png ${GFX_DRECTORY}/tilecollision.png ${WORLD_DRECTORY}/world.ldtk ${BUILD_DRECTORY}/tiledata.bin ${BUILD_DRECTORY}/tilemap.bin ${BUILD_DRECTORY}/scripts.bin -i ${SCRIPT_DRECTORY}/defines.txt


ALL_GFX = \
	${BUILD_DRECTORY}/knight.bin \
	${BUILD_DRECTORY}/bomb.bin \
	${BUILD_DRECTORY}/wizard.bin ${BUILD_DRECTORY}/frank.bin ${BUILD_DRECTORY}/waterfall.bin \
	${BUILD_DRECTORY}/font.bin \
	${BUILD_DRECTORY}/hearts.bin \
	${BUILD_DRECTORY}/itemBomb.bin ${BUILD_DRECTORY}/itemBoot.bin ${BUILD_DRECTORY}/itemCross.bin ${BUILD_DRECTORY}/itemGem.bin ${BUILD_DRECTORY}/itemKey.bin ${BUILD_DRECTORY}/itemLeaf.bin ${BUILD_DRECTORY}/itemMoney.bin ${BUILD_DRECTORY}/itemWrench.bin \
	 ${BUILD_DRECTORY}/itemUnknown.bin
ALL_MAP = \
	${BUILD_DRECTORY}/tile*.bin \
	${BUILD_DRECTORY}/scripts.bin
ALL_SRC = ${SRC_DRECTORY}/*.asm
ALL_DATA = ${ALL_GFX} ${ALL_MAP} ${ALL_SRC}

${BUILD_DRECTORY}/${ROM_NAME} : ${ALL_DATA}
	$(SJASMPLUS) $(SJASMPLUS_FLAGS) ${SRC_DRECTORY}/buildConfigRom.asm ${SRC_DRECTORY}/main.asm --raw=$@

${BUILD_DRECTORY}/${RAM_NAME} : ${ALL_DATA}
	$(SJASMPLUS) $(SJASMPLUS_FLAGS) ${SRC_DRECTORY}/buildConfigRam.asm ${SRC_DRECTORY}/main.asm --raw=$@

${BUILD_DRECTORY}/${RAM_TAP_NAME} : ${BUILD_DRECTORY}/${RAM_NAME}
	${BIN2TAP} --source ${BUILD_DRECTORY}/${RAM_NAME} --destination ${BUILD_DRECTORY}/${RAM_TAP_NAME} --address 32768 --name hourquest

${BUILD_DRECTORY}/${RAM_FINAL_NAME} : ${DATA_DRECTORY}/boot.tap ${BUILD_DRECTORY}/${RAM_TAP_NAME}
	cat ${DATA_DRECTORY}/boot.tap ${BUILD_DRECTORY}/${RAM_TAP_NAME} > ${BUILD_DRECTORY}/${RAM_FINAL_NAME}
