; -------------------------------------------------------------------------------------------------
; Scripting functionality
; -------------------------------------------------------------------------------------------------
	MODULE script
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; structures to hold different variable types

; screen local variables
NUM_SCREEN_VARS			equ		8

; global variables
	STRUCT GLOBAL_VARS_STRUCT
CURRENT_SCREEN			byte
TRIGGER_ID				byte
UNNAMED					block	4
	ENDS



; -------------------------------------------------------------------------------------------------
; global variables

	STRUCT MM_SCRIPT
ALL_VARS
SCREEN_VARS				block	NUM_SCREEN_VARS
GLOBAL_VARS				GLOBAL_VARS_STRUCT
	ENDS



; -------------------------------------------------------------------------------------------------
; enterRoom
;
; Clear screen local variables and set global screen number.
; -------------------------------------------------------------------------------------------------
; In:
;	a = Current screen number
; Out:
;	None
; Trashed:
;	bc, de, hl
; -------------------------------------------------------------------------------------------------

enterRoom
			ld		(MM.SCRIPT.GLOBAL_VARS.CURRENT_SCREEN),a

			ld		hl,MM.SCRIPT.SCREEN_VARS
			ld		de,MM.SCRIPT.SCREEN_VARS+1
			ld		(hl),0
			ld		bc,NUM_SCREEN_VARS-1
			ldir

			ret



; -------------------------------------------------------------------------------------------------
; runScript
;
; Runs a script until completion. iy is used as the pc while the script is being run. Every line of
; a script starts with a function, and each function can contain any number of arguments.
; -------------------------------------------------------------------------------------------------
; In:
;	iy => Script to run
; Out:
;	None
; Trashed:
;	Everything
; -------------------------------------------------------------------------------------------------

runScript
			; read next function
.nextFunc	ld		a,(iy)							; read function type
			inc		iy
			or		a
			ret		z								; 0 is end-of-script, so exit..
			ld		h,0
			ld		l,a
			ld		de,func_jtab-2
			add		hl,de							; hl => function address in jt
			ld		a,(hl)
			inc		hl
			ld		h,(hl)
			ld		l,a								; hl => function address
			
			ld		de,.nextFunc
			push	de								; push address of .nextFunc
			jp		(hl)							; call function and then return to .nextFunc



; -------------------------------------------------------------------------------------------------
; Script functions. These functions can trash any registers except iy, which is used as the pc

FUNC_END_OF_SCRIPT				equ		0<<1		; 
FUNC_IF_EQ						equ		1<<1		; NUMBER_1, NUMBER_2, OFFSET
FUNC_IF_NE						equ		2<<1		; NUMBER_1, NUMBER_2, OFFSET
FUNC_JUMP						equ		3<<1		; OFFSET
FUNC_SET						equ		4<<1		; VAR, NUMBER

FUNC_ADD_COLLISION				equ		5<<1		; NUMBER_X, NUMBER_Y, NUMBER_W, NUMBER_H
FUNC_ADD_TRIGGER				equ		6<<1		; NUMBER_ID, NUMBER_X, NUMBER_Y, NUMBER_W, NUMBER_H
FUNC_REMOVE_TRIGGER				equ		7<<1		; NUMBER_ID
FUNC_SPAWN_MOB					equ		8<<1		; NUMBER_TYPE, NUMBER_X, NUMBER_Y
FUNC_TRIGGER_DIALOGUE_SEQUENCE	equ		9<<1		; NUMBER_ID
FUNC_PAUSE						equ		10<<1		; NUMBER_TIME
FUNC_TELEPORT_PLAYER			equ		11<<1		; NUMBER_ROOM, NUMBER_X, NUMBER_Y

func_jtab
			;dw		0								; FUNC_END_OF_SCRIPT
			dw		func_ifEq						; FUNC_IF_EQ
			dw		func_ifNe						; FUNC_IF_NE
			dw		func_jump						; FUNC_JUMP
			dw		func_set						; FUNC_SET
			dw		func_addCollision				; FUNC_ADD_COLLISION
			dw		func_addTrigger					; FUNC_ADD_TRIGGER
			dw		func_removeTrigger				; FUNC_REMOVE_TRIGGER
			dw		func_spawnMob					; FUNC_SPAWN_MOB
			dw		func_triggerDialogueSequence	; FUNC_TRIGGER_DIALOGUE_SEQUENCE
			dw		func_pause						; FUNC_PAUSE
			dw		func_teleportPlayer				; FUNC_TELEPORT_PLAYER


; FUNC_IF_EQ, NUMBER_1, NUMBER_2, OFFSET
; If NUMBER_1 == NUMBER_2 then continue, otherwise jump forward by OFFSET bytes
func_ifEq
			call	readNumber						; read NUMBER_1
			ld		b,a
			call	readNumber						; read NUMBER_2
			cp		b								; compare NUMBER_1 and NUMBER 2
			inc		iy								; step over OFFSET
			ret		z								; NUMBER_1 == NUMBER_2, so continue
			ld		d,0								; NUMBER_1 != NUMBER_2, so jump
			ld		e,(iy-1)						; offset in de
			add		iy,de							; add to pc
			ret


; FUNC_IF_NE, NUMBER_1, NUMBER_2, OFFSET
; If NUMBER_1 == NUMBER_2 then continue, otherwise jump forward by OFFSET bytes
func_ifNe
			call	readNumber						; read NUMBER_1
			ld		b,a
			call	readNumber						; read NUMBER_2
			cp		b								; compare NUMBER_1 and NUMBER 2
			inc		iy								; step over OFFSET
			ret		nz								; NUMBER_1 != NUMBER_2, so continue
			ld		d,0								; NUMBER_1 == NUMBER_2, so jump
			ld		e,(iy-1)						; offset in de
			add		iy,de							; add to pc
			ret


; FUNC_JUMP, OFFSET
; Jump forwards by OFFSET bytes
func_jump
			ld		d,0
			ld		e,(iy)							; OFFSET in de
			inc		iy
			add		iy,de							; add to pc
			ret


; FUNC_SET, VAR, NUMBER
; Set the variable VAR to NUMBER
func_set
			ld		h,0
			ld		l,(iy)							; hl = VAR
			inc		iy
			ld		de,MM.SCRIPT.ALL_VARS
			add		hl,de							; hl => var address
			call	readNumber						; read NUMBER
			ld		(hl),a							; VAR = NUMBER
			ret


; FUNC_ADD_COLLISION, NUMBER_X, NUMBER_Y, NUMBER_W, NUMBER_H
func_addCollision
			call	readNumber						; read NUMBER_X
			ld		e,a
			call	readNumber						; read NUMBER_Y
			ld		d,a
			call	readNumber						; read NUMBER_W
			ld		l,a
			call	readNumber						; read NUMBER_H
			ld		h,a
			jp		room.addCollision


; FUNC_ADD_TRIGGER, NUMBER_ID, NUMBER_X, NUMBER_Y, NUMBER_W, NUMBER_H
; Add a rectangular trigger zone with an id of NUMBER_ID
func_addTrigger
			call	readNumber						; read NUMBER_ID
			ld		b,a
			call	readNumber						; read NUMBER_X
			ld		e,a
			call	readNumber						; read NUMBER_Y
			ld		d,a
			call	readNumber						; read NUMBER_W
			ld		l,a
			call	readNumber						; read NUMBER_H
			ld		h,a
			jp		room.addTrigger


; FUNC_REMOVE_TRIGGER, NUMBER_ID
; Remove a trigger zone by NUMBER_ID
func_removeTrigger
			call	readNumber						; read NUMBER_ID
			ld		b,a
			jp		room.removeTrigger


; FUNC_SPAWN_MOB, NUMBER_TYPE, NUMBER_X, NUMBER_Y
; Spawn a mob of NUMBER_TYPE at NUMBER_X, NUMBER_Y position
func_spawnMob
			call	readNumber						; read NUMBER_TYPE
			ld		b,a
			call	readNumber						; read NUMBER_X
			ld		e,a
			call	readNumber						; read NUMBER_Y
			ld		d,a
			call	mobs.spawn						; spawn
			ret


; FUNC_TRIGGER_DIALOGUE_SEQUENCE, NUMBER_ID
; Trigger dialogue sequence NUMBER_ID
func_triggerDialogueSequence
			call	readNumber						; read NUMBER_ID
			call	dialogue.triggerSequence
			ret


; FUNC_PAUSE, NUMBER_TIME
; Pause for a number of tenths of a second
func_pause
			call	readNumber						; read NUMBER_TIME
.waitAll	ld		b,10
.waitOne	halt									; wait for one tenth of a second
			djnz	.waitOne
			dec		a
			jr		nz,.waitAll						; wait for all requested tenths of a second
			ret


; NUMBER_ROOM, NUMBER_X, NUMBER_Y
; Teleport player to another room
func_teleportPlayer
			call	readNumber
			ld		(MM.ROOM.ROOM_ID),a
			ld		a,1
			ld		(MM.ROOM.CHANGE_ROOM_FLAG),a
			call	readNumber
			ld		(MM.PLAYER.X_POS),a
			call	readNumber
			ld		(MM.PLAYER.Y_POS),a

			ld		a,sound.sound_teleport
			call	sound.playSound
			ret



; -------------------------------------------------------------------------------------------------
; readNumber
;
; Read a number from the script. Can read numbers from variables, immediate values, functions or a
; combination of all three. It is potentially recursive in the case that the source is a function
; -------------------------------------------------------------------------------------------------
; In:
;	iy => Script pc
; Out:
;	a = Value
; Trashed:
;	Everything except iy, bc, de and hl
; -------------------------------------------------------------------------------------------------

NUMBER_SMALL_IMMEDIATE	equ		0<<6				; immediate value of in lower 6 bits
NUMBER_VAR				equ		1<<6				; contents of register in lower 6 bits
NUMBER_BIG_IMMEDIATE	equ		2<<6				; immediate value next byte
NUMBER_FUNCTION			equ		3<<6				; result of function in lower 6 bits

readNumber
			ld		a,(iy)							; what type of number?
			inc		iy
			and		%11000000						; mask to get number type
			jr		z,.smallImmediate				; NUMBER_BIG_IMMEDIATE
			cp		NUMBER_VAR						; NUMBER_VAR?
			jr		z,.var
			cp		NUMBER_BIG_IMMEDIATE			; NUMBER_BIG_IMMEDIATE?
			jr		z,.bigImmediate
			; otherwise it must be a NUMBER_FUNCTION

			; calculate a value using a function
.function	push	bc								; automatically save bc, de and hl
			push	de
			push	hl
			ld		a,(iy-1)						; extract function id
			and		%00111110
			ld		h,0
			ld		l,a								; hl = number function type
			ld		de,numfunc_jtab
			add		hl,de							; hl => number function address in jt
			ld		a,(hl)
			inc		hl
			ld		h,(hl)
			ld		l,a								; hl => function address
			ld		de,.funcReturn					; push return address
			push	de
			jp		(hl)							; call function and then return to .funcReturn
.funcReturn	pop		hl								; restore saved registers
			pop		de
			pop		bc
			ret										; return value in a

			; read value from a variable
.var		push	de
			push	hl
			ld		a,(iy-1)						; extract variable id
			and		%00111111
			ld		d,0
			ld		e,a								; de = variable id
			ld		hl,MM.SCRIPT.ALL_VARS
			add		hl,de							; de => variable
			ld		a,(hl)							; read variable value
			pop		hl
			pop		de
			ret										; return value in a

			; read an immediate value from the instruction byte
.smallImmediate
			ld		a,(iy-1)						; extract value
			and		%00111111
			ret										; return value in a

			; read an immediate value from the next byte
.bigImmediate
			ld		a,(iy)							; read immediate value
			inc		iy
			ret										; return value in a



; -------------------------------------------------------------------------------------------------
; Script number functions. These function should return a result in a and only trash bc, de and hl.
; iy is used as the pc

NUMFUNC_ADD			equ		0<<1					; NUMBER_1, NUMBER_2
NUMFUNC_AND			equ		1<<1					; NUMBER_1, NUMBER_2
NUMFUNC_RANDOM		equ		2<<1					; 

numfunc_jtab
			dw		numfunc_add						; NUMFUNC_ADD
			dw		numfunc_and						; NUMFUNC_AND
			dw		numfunc_random					; NUMFUNC_RANDOM


; NUMFUNC_ADD, NUMBER_1, NUMBER_2
; Returns NUMBER_1 + NUMBER_2
numfunc_add
			call	readNumber						; read NUMBER_1
			ld		b,a
			call	readNumber						; read NUMBER_2
			add		a,b								; +
			ret


; NUMFUNC_AND, NUMBER_1, NUMBER_2
; Returns NUMBER_1 & NUMBER_2
numfunc_and
			call	readNumber						; read NUMBER_1
			ld		b,a
			call	readNumber						; read NUMBER_2
			and		a,b								; &
			ret


; NUMFUNC_RANDOM
; Returns a random number between 0 and 255
numfunc_random
			call	utils.getRand
			ld		a,l
			ret



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
