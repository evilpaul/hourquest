; -------------------------------------------------------------------------------------------------
; Inventory handling
; -------------------------------------------------------------------------------------------------
	MODULE inventory
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables

	STRUCT MM_INVENTORY
CURSOR				byte
	ENDS



; -------------------------------------------------------------------------------------------------

MENU_BOX_WIDTH		equ		18
MENU_BOX_HEIGHT		equ		15
MENU_BOX_X			equ		(32-MENU_BOX_WIDTH)/2
MENU_BOX_Y			equ		(24-MENU_BOX_HEIGHT)/2



; -------------------------------------------------------------------------------------------------
; update
;
; Handle inventory menu. This is called from the main game loop. If the MENU button is pressed then
; we go into a separate "menu loop"
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af - if menu active then everything
; -------------------------------------------------------------------------------------------------

update
			; is the menu button pressed?
			ld		a,(MM.INPUT.PRESSED)
			and		input.MENU_BUTTON_MASK
			ret		z								; no...

			; start the opening noise
			ld		a,sound.sound_menuOpen
			call	sound.playSound

			; yes, draw the menu box
			ld		a,COLOUR_FG_WHITE|COLOUR_BG_BLUE
			ld		bc,MENU_BOX_Y + MENU_BOX_X*256
			ld		de, MENU_BOX_HEIGHT + MENU_BOX_WIDTH*256
			call	screenBlock.config
			call	screenBlock.save
			call	screenBlock.draw

			; draw all items
			ld		ix,items.itemDefs
			ld		b,0
.drawAllItems
			push	bc
			ld		a,b
			call	items.doesPlayerHaveItem
			jr		nz,.hasItem
.doesNotHaveItem
			ld		de,items.itemUnknownGfx
			jr		.gotItemGfx
.hasItem	ld		d,(ix + items.ITEM_DEF.GFX+1)	; get gfx source
			ld		e,(ix + items.ITEM_DEF.GFX+0)
.gotItemGfx	ld		h,1								; get screen destination
			push	de
			call	getItemScreenAddresses

			push	hl								; save pixel address

			ld		a,(ix + items.ITEM_DEF.COLOUR)	; get colour of the item
			ld		(de),a							; draw 2x2 colour attributes
			inc		e
			ld		(de),a
			ld		hl,32
			add		hl,de
			ld		(hl),a
			dec		l
			ld		(hl),a

			pop		hl								; restore pixel address to hl
			pop		de								; restore source gfx address to de

			ld		b,16							; items are 16 lines high
.drawOneItemPixels
			ld		a,(de)
			ld		(hl),a
			inc		e
			inc		l
			ld		a,(de)
			ld		(hl),a
			dec		l
			inc		de
			call	utils.getNextScreenRowDownHl
			djnz	.drawOneItemPixels

			ld		bc,items.ITEM_DEF._SIZE			; point to next item
			add		ix,bc
			pop		bc
			inc		b
			ld		a,b
			cp		items.NUM_ITEMS
			jr		nz,.drawAllItems

			; draw initial cursor
			call	drawCursor

			; describe current item
			call	drawItemDescription

			; wait for menu button to be released
.wait		halt
			call	input.update
			ld		a,(MM.INPUT.DOWN)
			and		input.MENU_BUTTON_MASK
			jr		nz,.wait

			; now run the menu loop..
			call	menuLoop

			; start the closing noise
			ld		a,sound.sound_menuClose
			call	sound.playSound

			; restore the screen and we're done..
			jp		screenBlock.restore



; -------------------------------------------------------------------------------------------------
; menuLoop
;
; The menu loop handles the menu UI. Doesn't exit until the player hits the MENU button
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af - if menu active then everything
; -------------------------------------------------------------------------------------------------

menuLoop
			; sync and read controls
			ld		a,0								; set border to black
			out		(BORDER_PORT),a
			halt
			ld		a,5								; set border to cyan
			out		(BORDER_PORT),a
			call	input.update

			; see if menu button has just been released
			ld		a,(MM.INPUT.RELEASED)
			and		input.MENU_BUTTON_MASK
			ret		nz								; yes, exit the menu loop

			; cursor movement
			ld		a,(MM.INVENTORY.CURSOR)
			ld		d,a								; d = current cursor position
			ld		a,(MM.INPUT.PRESSED)
.tryLeft	bit		input.LEFT_BUTTON,a				; move left?
			jr		z,.tryRight
			ld		a,d
			and		%11
			jr		z,.cantMove
			ld		a,d
			dec		a
			jr		.moveCursor
.tryRight	bit		input.RIGHT_BUTTON,a			; move right?
			jr		z,.tryUp
			ld		a,d
			and		%11
			cp		%11
			jr		z,.cantMove
			ld		a,d
			inc		a
			jr		.moveCursor
.tryUp		bit		input.UP_BUTTON,a				; move up?
			jr		z,.tryDown
			ld		a,d
			and		%1100
			jr		z,.cantMove
			ld		a,d
			sub		4
			jr		.moveCursor
.tryDown	bit		input.DOWN_BUTTON,a				; move down?
			jr		z,.triedAllDirections
			ld		a,d
			and		%1100
			cp		%1000
			jr		z,.cantMove
			ld		a,d
			add		a,4
			;jr		.moveCursor
.moveCursor	ex		af,af'							; cursor did actually move
			call	drawCursor						; erase old cursor
			ex		af,af'
			ld		(MM.INVENTORY.CURSOR),a			; store new cursor index
			call	drawCursor						; redraw new cursor
			ld		a,sound.sound_menuMove			; make a noise
			call	sound.playSound
			call	drawItemDescription				; update description
			jr		.triedAllDirections
.cantMove	
.triedAllDirections

			; check for assigning items
			ld		a,(MM.INVENTORY.CURSOR)			; get item under the cursor in c
			ld		c,a
			ld		a,(MM.INPUT.PRESSED)			; read input

.tryA		bit		input.A_BUTTON,a				; is the A button pressed?
			jr		z,.tryB							; no..
			ld		a,c
			call	items.doesPlayerHaveItem		; yes, does the player have the item?
			jr		z,.doesntHaveItem				; no..
			ld		a,(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_B)	; yes, is the cursor over the item that currently in slot B?
			cp		c
			jr		z,.swapItems					; yes - this is an item swap
.noSwapA	ld		a,c								; no, just assign the item to slot A
			ld		(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_A),a
			jr		.changedItems

.tryB		bit		input.B_BUTTON,a				; is the B button pressed?
			jr		z,.triedAllAssignments			; no..
			ld		a,c
			call	items.doesPlayerHaveItem		; yes, does the player have the item?
			jr		z,.doesntHaveItem			; no..
			ld		a,(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_A)	; yes, is the cursor over the item that currently in slot A?
			cp		c
			jr		z,.swapItems					; yes - this is an item swap
.noSwapB	ld		a,c								; no, just assign the item to slot B
			ld		(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_B),a
			jr		.changedItems

.swapItems	ld		a,(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_A)	; swap items in slots A and B
			ex		af,af
			ld		a,(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_B)
			ex		af,af
			ld		(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_B),a
			ex		af,af
			ld		(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_A),a
			;jr		.changedItems

.changedItems
			call	mobPlayer.drawItemUi			; items were changed so redraw the player's UI
			ld		a,sound.sound_menuSetItem		; make a noise
			call	sound.playSound
			jr		.triedAllAssignments

.doesntHaveItem			
			ld		a,sound.sound_menuCantSetItem	; make a noise
			call	sound.playSound

.triedAllAssignments

			; keep going..
			jp		menuLoop



; -------------------------------------------------------------------------------------------------
; getItemScreenAddresses
;
; Calculate screen address in the menu of an item. Items are arranged in rows of 4
; -------------------------------------------------------------------------------------------------
; In:
;	b = Item ID
; 	h = Offset added to x/y pos
; Out:
;	hl => Screen pixel address
;	de => Screen attribute address
; Trashed:
;	af, bc, de, hl
; -------------------------------------------------------------------------------------------------

getItemScreenAddresses
			ld		a,b

			; calculate y = (id / 4) * 3 + top border + offset
			rrca									; /4
			rrca
			and		%00000011
			ld		l,a
			add		a,a								; *2
			add		a,l								; *3
			add		a,MENU_BOX_Y + 1				; + top border
			add		a,h								; + offset
			ld		c,a

			; calculate x = (id % 3) * 4 + left border + offset
			ld		a,b
			and		%00000011						; % 3
			add		a,a								; *2
			add		a,a								; *4
			add		a,MENU_BOX_X + 1				; + left border
			add		a,h								; + offset
			ld		b,a

			call	utils.getScreenCellAttrAddressInHl
			ex		de,hl								; attribute address in de
			jp		utils.getScreenCellPixelAddressInHl	; pixel address in hl



; -------------------------------------------------------------------------------------------------
; drawCursor
;
; Draw UI cursor
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, bc, de, hl
; -------------------------------------------------------------------------------------------------

drawCursor
			ld		a,(MM.INVENTORY.CURSOR)
			ld		b,a
			ld		h,0
			call	getItemScreenAddresses

			; top row
			ld		de,text.font_base + 96*8
			ld		b,h
			call	utils.fastXorChar
			push	hl
			ld		h,b
			inc		l
			inc		de
			ld		b,h
			ld		c,e
			call	utils.fastXorChar
			ld		e,c
			ld		h,b
			inc		l
			ld		b,h
			call	utils.fastXorChar
			ld		h,b
			inc		e
			inc		l
			call	utils.fastXorChar

			; right edge
			ld		de,text.font_base + 100*8
			call	utils.getNextScreenRowDownHl
			ld		c,e
			call	utils.fastXorChar
			ld		e,c
			call	utils.getNextScreenRowDownHl
			call	utils.fastXorChar

			; left edge
			pop		hl
			ld		de,text.font_base + 99*8
			call	utils.getNextScreenRowDownHl
			ld		c,e
			call	utils.fastXorChar
			ld		e,c
			call	utils.getNextScreenRowDownHl
			call	utils.fastXorChar

			; bottom row
			ld		de,text.font_base + 101*8
			call	utils.getNextScreenRowDownHl
			ld		b,h
			call	utils.fastXorChar
			ld		h,b
			inc		l
			inc		de
			ld		b,h
			ld		c,e
			call	utils.fastXorChar
			ld		e,c
			ld		h,b
			inc		l
			ld		b,h
			call	utils.fastXorChar
			ld		h,b
			inc		e
			inc		l
			jp		utils.fastXorChar



; -------------------------------------------------------------------------------------------------
; drawItemDescription
;
; Draw description of the item under the cursor to the menu. If the item is not held by the player
; then a blank description is shown instead
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, bc, de, hl
; -------------------------------------------------------------------------------------------------

drawItemDescription
			ld		hl,descriptionTextSetup			; put the cursor in the right place
			call	text.print

			ld		a,(MM.INVENTORY.CURSOR)			; does the player have the item?
			call	items.doesPlayerHaveItem
			jr		z,.doesNotHaveItem				; no..

.hasItem	ld		a,(MM.INVENTORY.CURSOR)			; yes, look up the item definition
			call	items.getItemDef
			ld		h,(ix + items.ITEM_DEF.DESCRIPTION+1)	; get the description text
			ld		l,(ix + items.ITEM_DEF.DESCRIPTION+0)
			jp		text.print						; print it

.doesNotHaveItem
			ld		hl,descriptionNoItemText		; get the blank description text
			jp		text.print						; print it



; -------------------------------------------------------------------------------------------------

descriptionTextSetup
			db		text.CC, text.CC_RESET_MARGINS
			db		text.CC, text.CC_SET_POS, MENU_BOX_X+1, MENU_BOX_Y+MENU_BOX_HEIGHT-3
			db		text.EOT

descriptionNoItemText
			db		"      ????      "
			db		text.EOT



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
