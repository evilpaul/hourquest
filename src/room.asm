; -------------------------------------------------------------------------------------------------
; Room routines
; -------------------------------------------------------------------------------------------------
	MODULE room
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables and defines

ROOM_WIDTH 		equ		32
ROOM_HEIGHT		equ		24
ROOM_SIZE		equ		ROOM_WIDTH * ROOM_HEIGHT


	STRUCT MM_ROOM
CHANGE_ROOM_FLAG		byte
ROOM_ID					byte
COLLISION_MAP			block	ROOM_SIZE
ROOM_DATA				block	ROOM_SIZE
	ENDS

	STRUCT TILE_DATA
TILESET					block	8*128
ATTRIBUTES				block	128
COLLISION				block	128
SCREEN_ATTRIBUTES		block	768
	ENDS

VISUALISE_COLLISION_ZONES	equ		1
VISUALISE_TRIGGER_ZONES		equ		1

ROOM_SCRIPT_ONENTER		equ		0
ROOM_SCRIPT_ONTRIGGER	equ		1



; -------------------------------------------------------------------------------------------------
; draw
;
; Draws a single room to the screen. The room is made up of tiles stored in the room data buffer.
; -------------------------------------------------------------------------------------------------
; In:
; 	None
; Out:
; 	None
; Trashed:
;	a, bc, de, hl, bc', de', hl', ix, iy
; -------------------------------------------------------------------------------------------------

draw
			; decompress tile data to the scratch buffer
			ld		hl,tileData
			ld		de,MM.SCRATCH
			call	zx0.dzx0_standard

			exx
			ld		de,SCREEN_BASE_PIXELS			; de' => destination pixels
			ld		ix,MM.SCRATCH + TILE_DATA.SCREEN_ATTRIBUTES	; ix' => destination attributes
			ld		iy,MM.ROOM.COLLISION_MAP		; iy' => room collision map
			exx
			ld		hl,MM.ROOM.ROOM_DATA			; hl => room data source

			; we draw the screen in three 32x8 blocks
			; b is the number of tiles to draw per block (256)
			; c is the number of screen blocks to draw (3)
			ld		bc,3

.oneBlock	ld		a,(hl)							; read next tile
			ld		d,a								; save the original tile index
			inc		hl

			exx

			ld		h,0								; get 16bit tile index
			and		%01111111						; remove the x-flip bit
			ld		l,a
			push	hl								; save tile index

			ld		bc,MM.SCRATCH + TILE_DATA.ATTRIBUTES	; look up the tile attribute value
			add		hl,bc							; hl => tile attribute
			ld		a,(hl)
			ld		(ix),a							; set to screen
			inc		ix

			pop		hl								; restore tile index
			ld		bc,MM.SCRATCH + TILE_DATA.COLLISION	; look up tile collision value
			add		hl,bc							; hl => tile collision
			ld		a,(hl)
			ld		(iy),a							; set to collision map
			inc		iy

			exx
			ld		a,d								; get tile index again
			exx
			add		a,a								; tile index *2, shifts x-flip bit out of the top
			ld		h,0								; restore tile index
			ld		l,a
			ex		af,af'							; save carry flag (x-flip bit)
			add		hl,hl							; *8 to index into tile gfx array
			add		hl,hl
			ld		bc,MM.SCRATCH + TILE_DATA.TILESET	; look up the tile gfx
			add		hl,bc							; hl => tile gfx

			ld		c,d								; save high byte of screen pointer
			ld		b,8								; 8 bytes in a tile
			ex		af,af'							; restore carry flag (x-flip bit)
			jr		nc,.noFlip

			; draw tile with x-flip
.flip		push	bc
			ld		c,(hl)							; read
	REPT	8
			rrc		c								; x-flip the data byte
			rla
	ENDR
			inc		hl
			ld		(de),a							; write
			inc		d								; step down one screen line
			pop		bc
			djnz	.flip
			jr		.drawnCell

			; draw tile with no x-flip
.noFlip		ld		a,(hl)							; read
			inc		hl
			ld		(de),a							; write
			inc		d								; step down one screen line
			djnz	.noFlip

.drawnCell	ld		d,c								; restore screen pointer to start of this character cell..
			inc		e								; ..and point to next character cell
			exx
			djnz	.oneBlock						; draw all 256 tiles in this block

			exx										; one whole screen block completed
			ld		e,0								; point de to start of next screen block
			ld		a,d
			add		a,8
			ld		d,a
			exx

			dec		c
			jr		nz,.oneBlock					; draw all screen blocks..

			; finally, copy attributes across
			halt
			ld		hl,MM.SCRATCH + TILE_DATA.SCREEN_ATTRIBUTES	; ix' => destination attributes
			ld		de,SCREEN_BASE_ATTRS
			ld		bc,SCREEN_SIZE_ATTRS
			ldir
			ret



; -------------------------------------------------------------------------------------------------
; addCollision
;
; Add an area of collision to the room
; -------------------------------------------------------------------------------------------------
; In:
;	d = X position (0..31)
;	e = Y position (0..23)
;	h = Width
;	l = Height
; Out:
; 	None
; Trashed:
;	a, bc, de, hl
; -------------------------------------------------------------------------------------------------

addCollision
			push	bc
			push	hl

			ld		a,d								; calculate top left corner of trigger region in collision map
			add		a,a
			add		a,a
			add		a,a
			ld		h,0
			ld		l,a
			add		hl,hl
			add		hl,hl							; hl = y offset
			ld		bc,MM.ROOM.COLLISION_MAP
			add		hl,bc
			ld		b,0
			ld		c,e								; bc = x offset
			add		hl,bc							; hl => top left corner

			pop		de
			pop		bc

.allRows	push	hl								; save position in collision map

			ld		b,e								; b = width
.oneRow		set		0,(hl)							; set collision bit

			IF	VISUALISE_COLLISION_ZONES != 0
			push	bc
			push	hl
			ld		bc,-(MM.ROOM.COLLISION_MAP - SCREEN_BASE_ATTRS)
			add		hl,bc
			ld		(hl),COLOUR_BG_WHITE|COLOUR_FG_RED
			pop		hl
			pop		bc
			ENDIF

			inc		hl								; next tile..
			djnz	.oneRow							; draw whole row

			pop		hl								; restore position in collision map..
			;push	bc
			ld		bc,32
			add		hl,bc							; ..and step down to the next row
			;pop		bc
			dec		d								; set all rows
			jr		nz,.allRows

			ret



; -------------------------------------------------------------------------------------------------
; addTrigger
;
; Add a trigger to a room
; -------------------------------------------------------------------------------------------------
; In:
; 	b = Trigger ID (1..15)
;	d = X position (0..31)
;	e = Y position (0..23)
;	h = Width
;	l = Height
; Out:
; 	None
; Trashed:
;	a, bc, de, hl
; -------------------------------------------------------------------------------------------------

addTrigger
			push	bc
			push	hl

			ld		a,d								; calculate top left corner of trigger region in collision map
			add		a,a
			add		a,a
			add		a,a
			ld		h,0
			ld		l,a
			add		hl,hl
			add		hl,hl							; hl = y offset
			ld		bc,MM.ROOM.COLLISION_MAP
			add		hl,bc
			ld		b,0
			ld		c,e								; bc = x offset
			add		hl,bc							; hl => top left corner

			pop		de
			pop		bc

			ld		a,b								; mode id to top nibble
			rlca
			rlca
			rlca
			rlca
			and		%11110000
			ld		c,a								; c now has id in top nibble

.allRows	push	hl								; save position in collision map

			ld		b,e								; b = width
.oneRow		ld		a,(hl)							; get current collision value
			and		%00001111						; remove any trigger ids
			or		c								; merge in new trigger id
			ld		(hl),a							; store to collision map

			IF	VISUALISE_TRIGGER_ZONES != 0
			push	bc
			push	hl
			ld		bc,-(MM.ROOM.COLLISION_MAP - SCREEN_BASE_ATTRS)
			add		hl,bc
			ld		(hl),COLOUR_BG_WHITE|COLOUR_FG_BLUE
			pop		hl
			pop		bc
			ENDIF

			inc		hl								; next tile..
			djnz	.oneRow							; draw whole row

			pop		hl								; restore position in collision map..
			push	bc
			ld		bc,32
			add		hl,bc							; ..and step down to the next row
			pop		bc
			dec		d								; set all rows
			jr		nz,.allRows

			ret



; -------------------------------------------------------------------------------------------------
; removeTrigger
;
; Remove a trigger from a room
; -------------------------------------------------------------------------------------------------
; In:
; 	b = Trigger ID (1..15)
; Out:
; 	None
; Trashed:
;	a, bc, de, hl
; -------------------------------------------------------------------------------------------------

removeTrigger
			ld		d,%11110000						; d = mask to extract trigger id from collision tile

			ld		a,b								; mode id to top nibble
			rlca
			rlca
			rlca
			rlca
			and		d
			ld		e,a								; e now has id in top nibble

			ld		hl,MM.ROOM.COLLISION_MAP

			ld		c,ROOM_SIZE/256					; 3 blocks (3*356 = 768)
.loop1		ld		b,0								; one block is 256 tiles
.loop2		ld		a,(hl)							; get collision tile
			and		d								; extract trigger id
			jr		nz,.hasId						; any trigger here?
.doneId		inc		hl								; no.. next tile
			djnz	.loop2							; complete one block
			dec		c
			jr		nz,.loop1						; complete all three blocks
			ret

.hasId		cp		e								; is this our id?
			jr		nz,.doneId						; no..
			ld		a,(hl)							; yes, clear it
			and		%00001111
			ld		(hl),a
			jr		.doneId



; -------------------------------------------------------------------------------------------------
; update
;
; Handles room updates. Checks to see if another room has been entered and, if so, draws and 
; initialises it.
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
; 	None
; Trashed:
;	a, bc, de, hl, bc', de', hl', ix, iy
; -------------------------------------------------------------------------------------------------

update
			; check for moving to a new room
			ld		a,(MM.ROOM.CHANGE_ROOM_FLAG)
			or		a
			ret		z								; no new room
			xor		a								; clear the flag
			ld		(MM.ROOM.CHANGE_ROOM_FLAG),a

			; clear the screen to black
			ld		hl,SCREEN_BASE_ATTRS
			ld		de,SCREEN_BASE_ATTRS+1
			ld		bc,SCREEN_SIZE_ATTRS-1
			ld		(hl),l
			ldir

			; kill everyone
			call	mobs.clearAll

			; decompress the map data to temporary buffer (aka the screen)
			ld		hl,tileMap
			ld		de,SCREEN_BASE_PIXELS
			call	zx0.dzx0_standard

			; find appropriate room data and copy to room data buffer
			ld		a,(MM.ROOM.ROOM_ID)
			ld		hl,0
			ld		b,a
			ld		c,0
			add		hl,bc
			add		hl,bc
			add		hl,bc							; hl = room number * 768
			ld		bc,SCREEN_BASE_PIXELS
			add		hl,bc							; hl => source room data
			ld		de,MM.ROOM.ROOM_DATA
			ld		bc,ROOM_SIZE
			ldir									; copy room data to buffer

			; draw the room
			call	room.draw

			; create the player
			ld		de,(MM.PLAYER.X_POS)
			ld		b,mobs.MOB_TYPE_PLAYER
			call	mobs.spawn

			; call the on-enter script
			ld		b,ROOM_SCRIPT_ONENTER
			call	runScript

			; we're done..
			halt
			ret



; -------------------------------------------------------------------------------------------------
; collidePoint
;
; Collide a single point against the room collision map
; -------------------------------------------------------------------------------------------------
; In:
;	b = X position
;	c = Y position
; Out:
;	Collision tile at the given cell
; Trashed:
;	a, de, hl
; -------------------------------------------------------------------------------------------------

collidePoint
			ld			a,c							; calculate y offset
			and			%11111000					; ignore bottom 3 bits to get cell * 8
			ld			h,0
			ld			l,a
			add			hl,hl						; * 16
			add			hl,hl						; * 32, hl now = y offset

			ld			a,b							; calculate x offset
			rra
			rra
			rra
			and			%00011111					; a = x cell index
			ld			d,0
			ld			e,a							; de = x offset

			add			hl,de						; add together x and y offsets
			ld			de,MM.ROOM.COLLISION_MAP	; add in pointer to collision map
			add			hl,de						; hl => first cell collision in collision

			ld			a,(hl)						; get collision value for this cell
			ret



; -------------------------------------------------------------------------------------------------
; collideVertical
;
; Collide a vertical line segment against the room collision map
; -------------------------------------------------------------------------------------------------
; In:
;	b = X origin
;	c = Y origin
;	d = Height
; Out:
;	Zero flag set if collided, otherwise unset
; Trashed:
;	a, b, de, hl
; -------------------------------------------------------------------------------------------------

collideVertical
			ld			a,c							; calculate number of cells to check
			and			%00000111					; get y position within a cell
			add			a,d							; add height, a now = position in cell of end of segment
			dec			a							; -1
			rra										; / 2
			rra										; / 4
			rra										; / 8
			and			%00011111					; this is now the cell index of end of segment
			inc			a							; +1, a now = number of cells to check
			ex			af,af'						; save it

			ld			a,c							; calculate y offset
			and			%11111000					; ignore bottom 3 bits to get cell * 8
			ld			h,0
			ld			l,a
			add			hl,hl						; * 16
			add			hl,hl						; * 32, hl now = y offset

			ld			a,b							; calculate x offset
			rra
			rra
			rra
			and			%00011111					; a = x cell index
			ld			d,0
			ld			e,a							; de = x offset

			add			hl,de						; add together x and y offsets
			ld			de,MM.ROOM.COLLISION_MAP	; add in pointer to collision map
			add			hl,de						; hl => first cell in collision map

			ex			af,af'						; restore count of blocks to check
			ld			de,32						; how to step down one cell
			ld			b,a
.check		bit			0,(hl)						; check this cell for collision
			ret			nz							; collision found - return with z flag set
			add			hl,de						; step to next cell
			djnz		.check						; check all cells..
			ret										; no collision found - return with z flag clear



; -------------------------------------------------------------------------------------------------
; collideHorizontal
;
; Collide a horizontal line segment against the room collision map
; -------------------------------------------------------------------------------------------------
; In:
;	b = X origin
;	c = Y origin
;	d = Width
; Out:
;	Zero flag set if collided, otherwise unset
; Trashed:
;	a, b, de, hl
; -------------------------------------------------------------------------------------------------

collideHorizontal
			ld			a,b							; calculate number of cells to check
			and			%00000111					; get x position within a cell
			add			a,d							; add width, a now = position in cell of end of segment
			dec			a							; -1
			rra										; / 2
			rra										; / 4
			rra										; / 8
			and			%00011111					; this is now the cell index of end of segment
			inc			a							; +1, a now = number of cells to check
			ex			af,af'						; save it

			ld			a,c							; calculate y offset
			and			%11111000					; ignore bottom 3 bits to get cell * 8
			ld			h,0
			ld			l,a
			add			hl,hl						; * 16
			add			hl,hl						; * 32, hl now = y offset

			ld			a,b							; calculate x offset
			rra
			rra
			rra
			and			%00011111					; a = x cell index
			ld			d,0
			ld			e,a							; de = x offset

			add			hl,de						; add together x and y offsets
			ld			de,MM.ROOM.COLLISION_MAP	; add in pointer to collision map
			add			hl,de						; hl => first cell in collision map

			ex			af,af'						; restore count of blocks to check
			ld			b,a
.check		bit			0,(hl)						; check this cell for collision
			ret			nz							; collision found - return with z flag set
			inc			hl							; step to next cell
			djnz		.check						; check all cells..
			ret										; no collision found - return with z flag clear



; -------------------------------------------------------------------------------------------------
; buildHideMasks
;
; Build a set of "hide masks", suitable for drawing with the masked sprite routines. The hide masks
; are a series of bitmasks stored in one byte per row. Each bit of the bitmask stores whether the
; sprite should be hidden (1) or visible (0) in that character cell. The bitmasks are stored as
; %00001230, where 1 is the first cell, 2 the second and so on. For now, the hide masks are
; always 3 cells wide and 4 cells high
; -------------------------------------------------------------------------------------------------
; In:
;	b = X origin
;	c = Y origin
;	ix => destination
; Out:
; 	None
; Trashed:
;	a, bc, de, hl, ix
; -------------------------------------------------------------------------------------------------

buildHideMasks
			ld			a,c							; calculate y offset
			and			%11111000					; ignore bottom 3 bits to get cell * 8
			ld			h,0
			ld			l,a
			add			hl,hl						; * 16
			add			hl,hl						; * 32, hl now = y offset

			ld			a,b							; calculate x offset
			rra
			rra
			rra
			and			%00011111					; a = x cell index
			ld			d,0
			ld			e,a							; de = x offset

			add			hl,de						; add together x and y offsets
			ld			de,MM.ROOM.COLLISION_MAP	; add in pointer to collision map
			add			hl,de						; hl => first cell in collision map

			ld			de,32-2						; stride to the next map row
			ld			b,4							; always 4 cells high
.oneRow		ld			a,(hl)						; read byte 1
			inc			hl
			and			%00000010					; extract the hide bit
			ld			c,a							; store in c

			ld			a,(hl)						; read byte 2
			inc			hl
			and			%00000010					; extract the hide bit
			sla			c							; shift stored hide bits left one place
			or			c							; combine with new hide bit
			ld			c,a							; store in c

			ld			a,(hl)						; read byte 3
			and			%00000010					; extract the hide bit
			sla			c							; shift stored hide bits left one place
			or			c							; combine with new hide bit

			ld			(ix),a						; save calculated hide bits
			inc			ix

			add			hl,de						; jump to next map row
			djnz		.oneRow

			ret



; -------------------------------------------------------------------------------------------------
; runScript
;
; Run a room script
; -------------------------------------------------------------------------------------------------
; In:
;	b = ROOM_SCRIPT_ONENTER for onEnter, ROOM_SCRIPT_ONTRIGGER for onTrigger
; Out:
;	None
; Trashed:
;	Everything
; -------------------------------------------------------------------------------------------------

runScript
			; calculate script index
			ld		a,(MM.ROOM.ROOM_ID)				; script index is room*2 + type
			add		a,a
			add		a,b
			ld		b,a								; script index is now in b

			; step through the scripts to find the right one
			ld		hl,roomScripts					; the room scripts are all here
			ld		d,0
.findScript	ld		e,(hl)							; get next script length in de
			inc		hl								; point to first byte of script's body
			dec		b
			jp		m,.found						; if b is now -ve we've reached our script
			add		hl,de							; skip over script's body
			jr		.findScript

			; now run the script
.found		ld		a,d								; if the length byte was 0..
			or		e
			ret		z								; ..then there is no script to run
			push	hl
			pop		iy								; put script address into ay
			jp		script.runScript				; run the script



; -------------------------------------------------------------------------------------------------

; Tile data is in TILE_DATA format
tileData		incbin 'build/tiledata.bin'

tileMap			incbin 'build/tilemap.bin'

; Scripts are in the format:
;	length byte, script data
; There are num_rooms * 2 scripts. First script in each room pair is OnEnter, second is OnTrigger
roomScripts		incbin	'build/scripts.bin'



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
