; -------------------------------------------------------------------------------------------------
; Input routines
; -------------------------------------------------------------------------------------------------
	MODULE input
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables

	STRUCT MM_INPUT
DOWN					byte
PRESSED					byte
RELEASED				byte
	ENDS



; input bit flags
MENU_BUTTON			equ		6
UP_BUTTON			equ		5
DOWN_BUTTON			equ		4
LEFT_BUTTON			equ		3
RIGHT_BUTTON		equ		2
A_BUTTON			equ		1
B_BUTTON			equ		0

; input bit masks
MENU_BUTTON_MASK	equ		1<<6
UP_BUTTON_MASK		equ		1<<5
DOWN_BUTTON_MASK	equ		1<<4
LEFT_BUTTON_MASK	equ		1<<3
RIGHT_BUTTON_MASK	equ		1<<2
A_BUTTON_MASK		equ		1<<1
B_BUTTON_MASK		equ		1<<0



; -------------------------------------------------------------------------------------------------
; update
;
; Reads keyboard input and updates DOWN, PRESSED and RELEASED values. Call once per frame
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, bc, de, hl
; -------------------------------------------------------------------------------------------------

update
			ld			hl,inputData				; input data comes from here
			ld			d,0							; store the results as a bitfield in in d
			ld			bc,0x07fe					; b = 7 (number of inputs), c = 0xfe (low byte of input port)
.readOne	push		bc							; save counter
			ld			b,(hl)						; get high byte of port to read
			inc			hl
			in			a,(c)						; read the post
			and			(hl)						; use bitmask to get at the interesting bit
			inc			hl
			jr			nz,.off						; not pressed, carry flag = 0
			scf										; pressed, make carry flag = 1
.off		rl			d							; shift cary flag into bitfield
			pop			bc							; restore counter
			djnz		.readOne					; read all inputs..

			ld			hl,MM.INPUT.DOWN			; this is where we store the input data
			ld			e,(hl)						; now we have new DOWN value in d, old DOWN value in e
			ld			(hl),d						; store new DOWN value

			ld			a,d							; find difference between old and new DOWN values
			xor			e
			and			d							; mask out to get inputs that are DOWN now, ie: just PRESSED
			inc			hl							; point to PRESSED value
			ld			(hl),a						; store PRESSED value

			ld			a,d							; find difference between old and new DOWN values
			xor			e
			and			e							; mask out to get inputs that are not DOWN now, ie: just RELEASED
			inc			hl							; point to RELEASED value
			ld			(hl),a						; store RELEASED value

			ret


			; input data is a list of:
			;	low byte of keyboard input port for the key row that we are interested in
			;	bitmask to extract the key that we are interested in
inputData
			db			0xbf, %00001		; menu: enter
			db			0xfb, %00001		; up: q
			db			0xfd, %00001		; down: a
			db			0xdf, %00010		; left: o
			db			0xdf, %00001		; right: p
			db			0x7f, %01000		; a button: n
			db			0x7f, %00100		; b button: m



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
