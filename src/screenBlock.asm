; -------------------------------------------------------------------------------------------------
; Screen routines
; -------------------------------------------------------------------------------------------------
	MODULE screenBlock
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
	STRUCT BLOCK_DATA
X_POS					byte
Y_POS					byte
WIDTH					byte
HEIGHT					byte
ATTRIBUTES				byte
DATA
	ENDS



; -------------------------------------------------------------------------------------------------
; config
;
; Save configuration for following block calls in scratch memory
; -------------------------------------------------------------------------------------------------
; In:
;	b = char x position
;	c = char y position
;	d = char width
;	e = char height
;	a = attribute colour
; Out:
;	None
; Trashed:
;	ix
; -------------------------------------------------------------------------------------------------

config		ld		ix,MM.SCRATCH
			ld		(ix + BLOCK_DATA.X_POS),b
			ld		(ix + BLOCK_DATA.Y_POS),c
			ld		(ix + BLOCK_DATA.WIDTH),d
			ld		(ix + BLOCK_DATA.HEIGHT),e
			ld		(ix + BLOCK_DATA.ATTRIBUTES),a
			ret



; -------------------------------------------------------------------------------------------------
; save
;
; Save screen area to scratch memory
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, bc, de, hl, ix
; -------------------------------------------------------------------------------------------------

save		ld		ix,MM.SCRATCH

			call	utils.getScreenCellPixelAddressInHl ; dst
			ld		de,MM.SCRATCH+5 ; src

			exx
			ld		b,(ix + BLOCK_DATA.X_POS)
			ld		c,(ix + BLOCK_DATA.Y_POS)
			call	utils.getScreenCellAttrAddressInHl
			exx

			ld		b,(ix + BLOCK_DATA.HEIGHT)
.allRows	push	bc

			ld		c,8
.oneCharRow	ld		b,(ix + BLOCK_DATA.WIDTH)
			push	hl
.oneCharRowLine
			ld		a,(hl)
			ld		(de),a
			inc		de
			inc		l
			djnz	.oneCharRowLine
			pop		hl
			inc		h
			dec		c
			jr		nz,.oneCharRow
			dec		h
			call	utils.getNextScreenRowDownHl

			push	de
			exx
			pop		de
			
			ld		b,(ix + BLOCK_DATA.WIDTH)
			push	hl
.oneAttrRow
			ld		a,(hl)
			ld		(de),a
			inc		de
			inc		l
			djnz	.oneAttrRow
			pop		hl
			ld		bc,32
			add		hl,bc

			push	de
			exx
			pop		de

			pop		bc
			djnz	.allRows

			ret



; -------------------------------------------------------------------------------------------------
; draw
;
; Draw the block UI to the screen
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, af', bc, de, hl, ix
; -------------------------------------------------------------------------------------------------

draw		ld		ix,MM.SCRATCH

			; draw the attributes but paper on paper so that we don't
			; see everything being drawn
			ld		a,(ix + BLOCK_DATA.ATTRIBUTES)
			ld		b,a
			and		%11111000
			ld		c,a
			ld		a,b
			rrca
			rrca
			rrca
			and		%00000111
			or		c
			call	.drawAttributes

			; clear the central section
			call	clear

			; draw the border
			ld		b,(ix + BLOCK_DATA.X_POS)
			ld		c,(ix + BLOCK_DATA.Y_POS)
			push	bc
			call	utils.getScreenCellPixelAddressInHl
			ld		de,text.font_base + 96*8
			call	.drawTopBottomRow

			call	utils.getNextScreenRowDownHl
			ld		de,text.font_base + 100*8
			call	.drawLeftRightColumn

			pop		bc
			inc		c
			call	utils.getScreenCellPixelAddressInHl
			ld		de,text.font_base + 99*8
			call	.drawLeftRightColumn

			ld		de,text.font_base + 101*8
			call	.drawTopBottomRow

			; put the final attribute on screen
			ld		a,(ix + BLOCK_DATA.ATTRIBUTES)
			;call	.drawAttributes						; intentional fall through

.drawAttributes
			ex		af,af'
			ld		b,(ix + BLOCK_DATA.X_POS)
			ld		c,(ix + BLOCK_DATA.Y_POS)
			call	utils.getScreenCellAttrAddressInHl
			ex		af,af'

			ld		c,(ix + BLOCK_DATA.HEIGHT)
.allAttrRows
			ld		e,l
			ld		b,(ix + BLOCK_DATA.WIDTH)
.oneAttrRow	ld		(hl),a
			inc		l
			djnz	.oneAttrRow
			ld		l,e
			ld		de,32
			add		hl,de
			dec		c
			jr		nz,.allAttrRows

			ret

.drawTopBottomRow
			ld		c,h
			call	utils.fastDrawChar
			inc		de
			ld		h,c
			inc		l

			ld		b,(ix + BLOCK_DATA.WIDTH)
			dec		b
			dec		b
.drawTopBottomRowLoop
			ld		c,h
			call	utils.fastDrawChar
			ld		a,e
			sub		7
			ld		e,a
			ld		h,c
			inc		l
			djnz	.drawTopBottomRowLoop

			ld		a,e
			add		a,7
			ld		e,a
			inc		de
			call	utils.fastDrawChar

			ret

.drawLeftRightColumn
			ld		b,(ix + BLOCK_DATA.HEIGHT)
			dec		b
			dec		b
.drawLeftRightColumnLoop
			ld		c,e
			call	utils.fastDrawChar
			ld		e,c
			call	utils.getNextScreenRowDownHl
			djnz	.drawLeftRightColumnLoop
			ret



; -------------------------------------------------------------------------------------------------
; clear
;
; Clear the central area of the block
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, bc, de, hl, ix
; -------------------------------------------------------------------------------------------------

clear		ld		ix,MM.SCRATCH

			ld		b,(ix + BLOCK_DATA.X_POS)
			inc		b
			ld		c,(ix + BLOCK_DATA.Y_POS)
			inc		c
			call	utils.getScreenCellPixelAddressInHl

			ld		e,0
			ld		c,(ix + BLOCK_DATA.HEIGHT)
			dec		c
			dec		c
			sla		c
			sla		c
			sla		c
.allLines	ld		b,(ix + BLOCK_DATA.WIDTH)
			dec		b
			dec		b
			ld		d,l
.oneLine	ld		(hl),e
			inc		l
			djnz	.oneLine
			ld		l,d
			call	utils.getNextScreenRowDownHl
			dec		c
			jr		nz,.allLines

			ret



; -------------------------------------------------------------------------------------------------
; restore
;
; Restore screen area from scratch memory
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, bc, de, hl, ix
; -------------------------------------------------------------------------------------------------

restore		ld		ix,MM.SCRATCH

			ld		b,(ix + BLOCK_DATA.X_POS)
			ld		c,(ix + BLOCK_DATA.Y_POS)
			call	utils.getScreenCellPixelAddressInHl ; dst
			ld		de,MM.SCRATCH+5 ; src

			exx
			ld		b,(ix + BLOCK_DATA.X_POS)
			ld		c,(ix + BLOCK_DATA.Y_POS)
			call	utils.getScreenCellAttrAddressInHl
			exx

			ld		b,(ix + BLOCK_DATA.HEIGHT)
.allRows	push	bc

			ld		c,8
.oneCharRow	ld		b,(ix + BLOCK_DATA.WIDTH)
			push	hl
.oneCharRowLine
			ld		a,(de)
			ld		(hl),a
			inc		de
			inc		l
			djnz	.oneCharRowLine
			pop		hl
			inc		h
			dec		c
			jr		nz,.oneCharRow
			dec		h
			call	utils.getNextScreenRowDownHl

			push	de
			exx
			pop		de

			ld		b,(ix + BLOCK_DATA.WIDTH)
			push	hl
.oneAttrRow
			ld		a,(de)
			ld		(hl),a
			inc		de
			inc		l
			djnz	.oneAttrRow
			pop		hl
			ld		bc,32
			add		hl,bc

			push	de
			exx
			pop		de

			pop		bc
			djnz	.allRows

			ret



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
