; -------------------------------------------------------------------------------------------------
; Sound debugging routines
; -------------------------------------------------------------------------------------------------
	MODULE soundDebug
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables

	STRUCT SOUND_DEBUG_DATA
CURSOR_Y				byte	1
CURSOR_X				byte	1
SOUND_TAB				block	sound.SOUND._SIZE * SOUND_TAB_ENTRIES
	ENDS



; -------------------------------------------------------------------------------------------------

SOUND_TAB_ENTRIES	equ		16

DEBUG_MENU_WIDTH	equ		(sound.SOUND._SIZE * 2) + 2
DEBUG_MENU_HEIGHT	equ		SOUND_TAB_ENTRIES + 2
DEBUG_MENU_X		equ		(32 - DEBUG_MENU_WIDTH) / 2
DEBUG_MENU_Y		equ		(24 - DEBUG_MENU_HEIGHT) / 2



; -------------------------------------------------------------------------------------------------
; debugMenu
;
; Display a debug menu for editing sounds. Use the direction keys to move, A/B to inc/dec numbers
; and MENU to play the sound under the cursor. This menu never exits.
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	Never returns
; Trashed:
;	Everything
; -------------------------------------------------------------------------------------------------

debugMenu
			; draw menu box
			ld		bc,DEBUG_MENU_X*256 + DEBUG_MENU_Y
			ld		de,DEBUG_MENU_WIDTH*256 + DEBUG_MENU_HEIGHT
			ld		a,COLOUR_FG_WHITE|COLOUR_BG_RED
			call	screenBlock.config
			call	screenBlock.draw

			; set attribute colours for sound rows
			ld		bc,(DEBUG_MENU_X+1)*256 + (DEBUG_MENU_Y+1)
			call	utils.getScreenCellAttrAddressInHl
			ld		de,32 - ((sound.SOUND._SIZE * 2) - 1)
			ld		bc,SOUND_TAB_ENTRIES*256 + COLOUR_BG_RED
.stripes	ld		a,COLOUR_FG_CYAN
			or		c
			ld		(hl),a
			inc		l
			ld		(hl),a
			inc		l
			ld		a,COLOUR_FG_YELLOW
			or		c
			ld		(hl),a
			inc		l
			ld		(hl),a
			inc		l
			ld		a,COLOUR_FG_GREEN
			or		c
			ld		(hl),a
			inc		l
			ld		(hl),a
			inc		l
			ld		a,COLOUR_FG_WHITE
			or		c
			ld		(hl),a
			inc		l
			ld		(hl),a
			add		hl,de
			ld		a,c
			xor		COLOUR_BG_RED
			ld		c,a
			djnz	.stripes

			; initialise cursor position
			ld		bc,(DEBUG_MENU_X+1)*256 + (DEBUG_MENU_Y+1)
			ld		(MM.SCRATCH + SOUND_DEBUG_DATA.CURSOR_Y),bc

			; copy the hard-coded sounds to our temporary buffer
			ld		hl,sound.soundTab
			ld		de,MM.SCRATCH + SOUND_DEBUG_DATA.SOUND_TAB
			ld		bc,sound.SOUND._SIZE * SOUND_TAB_ENTRIES
			ldir

			; now the main menu loop..
.loop		halt									; sync and read keys
			call	input.update

			ld		a,6
			out		(BORDER_PORT),a
			call	tryUpdateCursor
			call	tryEditData
			call	tryPlaySound
			call	drawData
			ld		a,0
			out		(BORDER_PORT),a

			jp		.loop



; -------------------------------------------------------------------------------------------------
; tryUpdateCursor
;
; Move cursor on the screen.
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, bc, hl
; -------------------------------------------------------------------------------------------------

tryUpdateCursor
			; clear flash bit from current cursor position
			ld		bc,(MM.SCRATCH + SOUND_DEBUG_DATA.CURSOR_Y)	; get current cursor pos
			call	utils.getScreenCellAttrAddressInHl
			ld		a,(hl)
			and		~COLOUR_FLASH
			ld		(hl),a

			; check all direction inputs and change cursor pos (in bc) accordingly
			ld		a,(MM.INPUT.PRESSED)
			bit		input.UP_BUTTON,a
			jr		nz,.up
			bit		input.DOWN_BUTTON,a
			jr		nz,.down
			bit		input.LEFT_BUTTON,a
			jr		nz,.left
			bit		input.RIGHT_BUTTON,a
			jr		nz,.right
			jr		.drawCursor
.up			ld		a,c
			cp		DEBUG_MENU_Y + 1
			jr		nz,.goUp
			ld		c,DEBUG_MENU_Y + DEBUG_MENU_HEIGHT - 2 + 1
.goUp		dec		c
			jr		.drawCursor
.down		ld		a,c
			cp		DEBUG_MENU_Y + DEBUG_MENU_HEIGHT - 2
			jr		nz,.goDown
			ld		c,DEBUG_MENU_Y + 1 - 1
.goDown		inc		c
			jr		.drawCursor
.left		ld		a,b
			cp		DEBUG_MENU_X + 1
			jr		nz,.goLeft
			ld		b,DEBUG_MENU_X + DEBUG_MENU_WIDTH - 2 + 1
.goLeft		dec		b
			jr		.drawCursor
.right		ld		a,b
			cp		DEBUG_MENU_X + DEBUG_MENU_WIDTH - 2
			jr		nz,.goRight
			ld		b,DEBUG_MENU_X + 1 - 1
.goRight	inc		b
			jr		.drawCursor

			; set flash bit to new cursor position
.drawCursor	ld		(MM.SCRATCH + SOUND_DEBUG_DATA.CURSOR_Y),bc	; save new cursor pos
			call	utils.getScreenCellAttrAddressInHl
			ld		a,(hl)
			or		COLOUR_FLASH
			ld		(hl),a

			ret



; -------------------------------------------------------------------------------------------------
; tryEditData
;
; Edit sound values.
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, af', bc
; -------------------------------------------------------------------------------------------------

tryEditData
			; get y cursor value, exit if out of range
			ld		a,(MM.SCRATCH + SOUND_DEBUG_DATA.CURSOR_Y)
			sub		DEBUG_MENU_Y + 1
			ret		c
			cp		SOUND_TAB_ENTRIES
			ret		nc
			ld		b,a

			; get x cursor value, exit if out of range
			ld		a,(MM.SCRATCH + SOUND_DEBUG_DATA.CURSOR_X)
			sub		DEBUG_MENU_X + 1
			ret		c
			cp		10
			ret		nc
			ld		c,a

			; look up the address of the data under the cursor in hl
			ld		a,b								; y value
			add		a,a
			add		a,a
			ld		d,0
			ld		e,a
			ld		hl,MM.SCRATCH + SOUND_DEBUG_DATA.SOUND_TAB
			add		hl,de
			ld		a,c								; x value
			sra		a
			ld		d,0
			ld		e,a
			add		hl,de
			
			; build a mask in c to extract either the high or the low nibble
			ld		a,%11110000
			bit		0,c
			jr		z,.lowNibble
			cpl
.lowNibble	ld		c,a

			; check with inc/dec button presses
			ld		a,(MM.INPUT.PRESSED)
			bit		input.A_BUTTON,a
			jr		nz,.down
			bit		input.B_BUTTON,a
			jr		nz,.up
			ret

			; increase nibble
.up			ld		a,(hl)
			and		c
			add		a,%00010001
			and		c
			ld		b,a
			ld		a,c
			cpl
			and		(hl)
			or		b
			ld		(hl),a
			ret

			; decrease nibble
.down		ld		a,(hl)
			and		c
			add		a,%11111111
			and		c
			ld		b,a
			ld		a,c
			cpl
			and		(hl)
			or		b
			ld		(hl),a
			ret



; -------------------------------------------------------------------------------------------------
; tryPlaySound
;
; Play sound under te cursor if MENU button pressed.
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, af', bc
; -------------------------------------------------------------------------------------------------

tryPlaySound
			; MENU button pressed?
			ld		a,(MM.INPUT.PRESSED)
			bit		input.MENU_BUTTON,a
			ret		z

			; get id of sound under the cursor (do some range checks for good luck)
			ld		a,(MM.SCRATCH + SOUND_DEBUG_DATA.CURSOR_Y)
			sub		DEBUG_MENU_Y + 1
			ret		c
			cp		SOUND_TAB_ENTRIES
			ret		nc

			; cue the sound up to play
			jp		sound.playSound



; -------------------------------------------------------------------------------------------------
; drawData
;
; Draw the data to the screen.
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, bc, de, hl
; -------------------------------------------------------------------------------------------------

drawData
			; setup
			ld		de,MM.SCRATCH + SOUND_DEBUG_DATA.SOUND_TAB	; where the sound data is stored in de'
			ld		b,%00011110						; mask for extracting nibbles in b'
			exx
			ld		bc,(DEBUG_MENU_X+1)*256 + (DEBUG_MENU_Y+1)	; get screen address in hl
			call	utils.getScreenCellPixelAddressInHl

			; draw all rows
			ld		b,SOUND_TAB_ENTRIES							; this many rows
.drawAll	push	bc
			push	hl
			call	.drawOneRow
			pop		hl
			ld		a,h
			add		a,7
			ld		h,a
			call	utils.getNextScreenRowDownHl
			pop		bc
			djnz	.drawAll
			ret

			; draw a single row of n bytes
.drawOneRow
			ld		b,sound.SOUND._SIZE
			; draw a single byte of two nibbles
.drawOneByte
			; high nibble
			push	hl
			exx
			ld		a,(de)
			rrca
			rrca
			rrca
			and		b
			exx
			ld		h,HIGH (.hexTab)
			ld		l,a
			ld		e,(hl)
			inc		l
			ld		d,(hl)
			pop		hl
			ld		c,h
			call	utils.fastDrawChar
			ld		h,c
			inc		l

			; low nibble
			push	hl
			exx
			ld		a,(de)
			inc		de
			rlca
			and		b
			exx
			ld		h,HIGH (.hexTab)
			ld		l,a
			ld		e,(hl)
			inc		l
			ld		d,(hl)
			pop		hl
			ld		c,h
			call	utils.fastDrawChar
			ld		h,c
			inc		l

			djnz	.drawOneByte

			ret

			; fast lookups to character data for hex digits
			ALIGN	256
.hexTab		dw		text.font_base + '0'*8
			dw		text.font_base + '1'*8
			dw		text.font_base + '2'*8
			dw		text.font_base + '3'*8
			dw		text.font_base + '4'*8
			dw		text.font_base + '5'*8
			dw		text.font_base + '6'*8
			dw		text.font_base + '7'*8
			dw		text.font_base + '8'*8
			dw		text.font_base + '9'*8
			dw		text.font_base + 'A'*8
			dw		text.font_base + 'B'*8
			dw		text.font_base + 'C'*8
			dw		text.font_base + 'D'*8
			dw		text.font_base + 'E'*8
			dw		text.font_base + 'F'*8



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
