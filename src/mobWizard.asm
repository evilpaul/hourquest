; -------------------------------------------------------------------------------------------------
; Wizard mob
; -------------------------------------------------------------------------------------------------
	MODULE mobWizard
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
    STRUCT MOB_WIZARD
STATE              		byte
TIMER					byte
ANIM_FRAME				byte
    ENDS



; -------------------------------------------------------------------------------------------------
init
			ret



; -------------------------------------------------------------------------------------------------
update
			; if the player is using the cross then wizards don't move
			ld		a,(MM.PLAYER.CROSS_ACTIVE)
			or		a
			jr		nz,.skipMovement

			; advance state timer and state
			ld		a,(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_WIZARD.TIMER)
			inc		a
			cp		8
			jr		nz,.stillInState
			inc		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_WIZARD.STATE)
			xor		a
.stillInState
			ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_WIZARD.TIMER),a

			; move the sprite based on state
			ld		a,(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_WIZARD.STATE)
			ld		b,a
			and		%1
			jr		z,.setFrame

			ld		a,b
			and		%1110
			ld		h,0
			ld		l,a
			ld		de,directionTable
			add		hl,de
			ld		e,(hl)
			inc		hl
			ld		d,(hl)
			call	mobs.move

			; advance sprite animation frame
.skipMovement
			ld		a,(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_WIZARD.ANIM_FRAME)
			inc		a
.setFrame	ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_WIZARD.ANIM_FRAME),a	; save anim frame to mob data

			; set gfx for this anim frame
			and		%110							; mask to get anim frame, 0-3 but multiped by 2
			add		a,a								; * 4
			add		a,a								; * 8
			ld		d,a
			add		a,a								; * 16
			add		a,a								; * 32
			add		a,d								; * 32 + 8
			ld		d,0
			ld		e,a								; de = anim frame * 40
			ld		hl,gfx							; base of gfx
			add		hl,de							; point to gfx for this frame
			ld		(ix+mobs.MOB_DATA.GFX_DATA+1),h
			ld		(ix+mobs.MOB_DATA.GFX_DATA+0),l	; save gfx frame to mob data

			ret



; -------------------------------------------------------------------------------------------------
collide
			ld		a,sound.sound_enemyDeath		; make some noise
			call	sound.playSound

			jp		mobDead.init



; -------------------------------------------------------------------------------------------------
directionTable
			db		0,+1
			db		-1,+1
			db		-1,0
			db		-1,-1
			db		0,-1
			db		+1,-1
			db		+1,0
			db		+1,+1



; -------------------------------------------------------------------------------------------------
			; sprite graphics
			ALIGN	2
gfx			incbin	'build/wizard.bin'



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
