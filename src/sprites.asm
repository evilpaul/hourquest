; -------------------------------------------------------------------------------------------------
; Sprite routines
; -------------------------------------------------------------------------------------------------
	MODULE sprites
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; xor16WithHideMasks
;
; Draw a single 2 byte wide sprite using xor. No screen clipping is performed. Hide masks are used
; to mask out entire cells
; -------------------------------------------------------------------------------------------------
; In:
;	b = X pos
;	c = Y pos
;	de => Graphic data, must start on an even byte
;	h = height
;	ix => hide masks
; Out:
;	None
; Trashed:
;	af, bc, de, hl, bc', de', hl',ix
; -------------------------------------------------------------------------------------------------

xor16WithHideMasks
			push	hl								; save height
			call	utils.getScreenAddressInHl		; figure out screen position to draw at

			exx
			ld		hl,utils.scrollTableExtractionMasks	; look up scroll extraction masks for pixel position
			ld		b,0
			ld		c,a								; pixel position in bc
			add		hl,bc							; hl now points to scroll extraction mask for this pixel position
			ld		a,(hl)
			ld		d,a								; scroll extraction mask for first byte in d
			cpl
			ld		e,a								; scroll extraction mask for second byte in e

			ld  	a,HIGH utils.scrollTable		; use hl to access scroll byte table
			add 	a,c								; add pixel position..
			ld  	h,a								; ..to point hl at the correct scroll table
			exx

			ld		a,c								; find out how many lines of the first cell we need to draw
			cpl										; two's complement of the lowest three bits..
			and		%111
			inc		a								; .. gives us the number of lines to draw
			ex		af,af'

			pop		bc								; sprite height in b
			ld		c,b								; c is total height
			ex		af,af'
			ld		b,a								; b is number of lines to draw in first cell

			ld		a,c								; total height remaining = total height - lines in first cell
			sub		b
			ld		c,a

			; just before we enter the min drawing loops..
			;	b = height
			;	c = height remaining to be drawn
			;	de = source
			;	hl = dest
			;	h' = scroll table high byte
			;	de' = scroll extraction masks
.drawCellRows
			exx
			ld		bc,.afterDraw					; push return address to come back to after the draw function
			push	bc
			ld		a,(ix)							; read next hide mask
			inc		ix
			push	hl
			ld		h,HIGH .drawFunctions 			; look up draw routine for this hide mask
			add		a,LOW .drawFunctions
			ld		l,a
			ld		c,(hl)
			inc		l
			ld		b,(hl)							; draw function address now in bc
			pop		hl
			push	bc								; push draw function address
			exx
			ret										; return to draw function, then return from that to .afterDraw
.afterDraw	

			; an unrolled version of getNextScreenRowDownHl but missing the initial
			; inc h, which happens at the end of each draw routine
;			inc		h								; step down one pixel line
;			ld		a,h								; if bottom three bits..
;			and		7								; ..are not-zero then..
;			jr		nz,.stepped						; ..we're in the same char cell, and we're finished
			ld		a,l								; step down one entire char cell
			add		a,32
			ld		l,a
			jp		c,.stepped						; if carry bit set we didn't cross a third
			ld		a,h								; we crossed a third, so we need to fix up the high byte
			sub		8
			ld		h,a
.stepped

			ld		b,8								; 8 fewer lines to draw..
			ld		a,c
			sub		b
			jr		c,.lastCellRow					; we just crossed into the last cell row..
			ld		c,a
			jp		.drawCellRows					; draw another compete cell row..

.lastCellRow
			ld		a,c								; nothing left to draw in this sprite?
			or		a
			ret		z								; no - we're done..
			ld		b,a								; yes - here's how many pixel rows to draw

			exx
			ld		a,(ix)							; read next hide mask
			push	hl
			ld		h,HIGH .drawFunctions 			; look up draw routine for this hide mask
			add		a,LOW .drawFunctions
			ld		l,a
			ld		c,(hl)
			inc		l
			ld		b,(hl)							; draw function address now in bc
			pop		hl
			push	bc								; push draw function address
			exx
			ret										; return to draw function, then return to whatever called this function..



; -------------------------------------------------------------------------------------------------
; there are 8 unique draw routines. each one draws 'b' lines of sprite, and each one masks out
; different bytes of the graphic. this table maps hide masks to draw routines. a 0 in the routine
; name means that that bit is visible, a 1 means hidden. the lines that the routines are asked to
; draw are all guaranteed to be within a single character cell

			ALIGN	16
.drawFunctions
			dw		_drawMasked_000
			dw		_drawMasked_001
			dw		_drawMasked_010
			dw		_drawMasked_011
			dw		_drawMasked_100
			dw		_drawMasked_101
			dw		_drawMasked_110
			dw		_drawMasked_111


; -------------------------------------------------------------------------------------------------
_drawMasked_111
			ld		a,h								; step down 1 * height lines
			add		a,b
			ld		h,a

			ld	a,c									; skip 2 * height source bytes
			ld	c,b
			ld	b,0
			ex	de,hl
			add	hl,bc
			add	hl,bc
			ex	de,hl
			ld	c,a

			ret


; -------------------------------------------------------------------------------------------------
_drawMasked_110
.allLines
			; byte 1
;			ld  	a,(de)							; read sprite source, byte 1
			inc 	e								; next source byte
;			exx
;			ld  	l,a								; calc address scrolled version of source byte 1
;			ld  	a,(hl)							; get scrolled version of source byte 1
;			and 	d								; extract left half of source byte 1
;			exx
;			xor 	(hl)							; xor with screen contents
;			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 2
;			exx
;			ld  	a,(hl)							; get scrolled version of source byte 1 again
;			and 	e								; extract right half of source byte 1
;			ld		c,a								; save it
;			exx

			ld  	a,(de)							; read sprite source, byte 2
			inc 	de								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 2
;			ld  	a,(hl)							; get scrolled version of source byte 2
;			and 	d								; extract left half of source byte 2
;			or		c								; merge with right half of source byte 1
			exx
;			xor 	(hl)							; xor with screen contents
;			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 3
			exx
			ld  	a,(hl)							; get scrolled version of source byte 2 again
			and 	e								; extract right half of source byte 2
			exx
			xor 	(hl)							; xor with screen
			ld  	(hl),a							; set to screen

			dec 	l								; step back to first screen byte of this line
			dec 	l
			inc		h								; step down screen one line within the cell
			djnz	.allLines						; draw all lines..

			ret


; -------------------------------------------------------------------------------------------------
_drawMasked_101
.allLines
			; byte 1
			ld  	a,(de)							; read sprite source, byte 1
			inc 	e								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 1
;			ld  	a,(hl)							; get scrolled version of source byte 1
;			and 	d								; extract left half of source byte 1
			exx
;			xor 	(hl)							; xor with screen contents
;			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 2
			exx
			ld  	a,(hl)							; get scrolled version of source byte 1 again
			and 	e								; extract right half of source byte 1
			ld		c,a								; save it
			exx

			ld  	a,(de)							; read sprite source, byte 2
			inc 	de								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 2
			ld  	a,(hl)							; get scrolled version of source byte 2
			and 	d								; extract left half of source byte 2
			or		c								; merge with right half of source byte 1
			exx
			xor 	(hl)							; xor with screen contents
			ld  	(hl),a							; set to screen
;			inc 	l								; next screen byte

			; byte 3
;			exx
;			ld  	a,(hl)							; get scrolled version of source byte 2 again
;			and 	e								; extract right half of source byte 2
;			exx
;			xor 	(hl)							; xor with screen
;			ld  	(hl),a							; set to screen

;			dec 	l								; step back to first screen byte of this line
			dec 	l
			inc		h								; step down screen one line within the cell
			djnz	.allLines						; draw all lines..

			ret


; -------------------------------------------------------------------------------------------------
_drawMasked_100
.allLines
			; byte 1
			ld  	a,(de)							; read sprite source, byte 1
			inc 	e								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 1
;			ld  	a,(hl)							; get scrolled version of source byte 1
;			and 	d								; extract left half of source byte 1
			exx
;			xor 	(hl)							; xor with screen contents
;			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 2
			exx
			ld  	a,(hl)							; get scrolled version of source byte 1 again
			and 	e								; extract right half of source byte 1
			ld		c,a								; save it
			exx

			ld  	a,(de)							; read sprite source, byte 2
			inc 	de								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 2
			ld  	a,(hl)							; get scrolled version of source byte 2
			and 	d								; extract left half of source byte 2
			or		c								; merge with right half of source byte 1
			exx
			xor 	(hl)							; xor with screen contents
			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 3
			exx
			ld  	a,(hl)							; get scrolled version of source byte 2 again
			and 	e								; extract right half of source byte 2
			exx
			xor 	(hl)							; xor with screen
			ld  	(hl),a							; set to screen

			dec 	l								; step back to first screen byte of this line
			dec 	l
			inc		h								; step down screen one line within the cell
			djnz	.allLines						; draw all lines..

			ret


; -------------------------------------------------------------------------------------------------
_drawMasked_011
.allLines
			; byte 1
			ld  	a,(de)							; read sprite source, byte 1
			inc 	e								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 1
			ld  	a,(hl)							; get scrolled version of source byte 1
			and 	d								; extract left half of source byte 1
			exx
			xor 	(hl)							; xor with screen contents
			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 2
;			exx
;			ld  	a,(hl)							; get scrolled version of source byte 1 again
;			and 	e								; extract right half of source byte 1
;			ld		c,a								; save it
;			exx

;			ld  	a,(de)							; read sprite source, byte 2
			inc 	de								; next source byte
;			exx
;			ld  	l,a								; calc address scrolled version of source byte 2
;			ld  	a,(hl)							; get scrolled version of source byte 2
;			and 	d								; extract left half of source byte 2
;			or		c								; merge with right half of source byte 1
;			exx
;			xor 	(hl)							; xor with screen contents
;			ld  	(hl),a							; set to screen
;			inc 	l								; next screen byte

			; byte 3
;			exx
;			ld  	a,(hl)							; get scrolled version of source byte 2 again
;			and 	e								; extract right half of source byte 2
;			exx
;			xor 	(hl)							; xor with screen
;			ld  	(hl),a							; set to screen

;			dec 	l								; step back to first screen byte of this line
			dec 	l
			inc		h								; step down screen one line within the cell
			djnz	.allLines						; draw all lines..

			ret


; -------------------------------------------------------------------------------------------------
_drawMasked_010
.allLines
			; byte 1
			ld  	a,(de)							; read sprite source, byte 1
			inc 	e								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 1
			ld  	a,(hl)							; get scrolled version of source byte 1
			and 	d								; extract left half of source byte 1
			exx
			xor 	(hl)							; xor with screen contents
			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 2
;			exx
;			ld  	a,(hl)							; get scrolled version of source byte 1 again
;			and 	e								; extract right half of source byte 1
;			ld		c,a								; save it
;			exx

			ld  	a,(de)							; read sprite source, byte 2
			inc 	de								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 2
			ld  	a,(hl)							; get scrolled version of source byte 2
;			and 	d								; extract left half of source byte 2
;			or		c								; merge with right half of source byte 1
			exx
;			xor 	(hl)							; xor with screen contents
;			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 3
			exx
			ld  	a,(hl)							; get scrolled version of source byte 2 again
			and 	e								; extract right half of source byte 2
			exx
			xor 	(hl)							; xor with screen
			ld  	(hl),a							; set to screen

			dec 	l								; step back to first screen byte of this line
			dec 	l
			inc		h								; step down screen one line within the cell
			djnz	.allLines						; draw all lines..

			ret


; -------------------------------------------------------------------------------------------------
_drawMasked_001
.allLines
			; byte 1
			ld  	a,(de)							; read sprite source, byte 1
			inc 	e								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 1
			ld  	a,(hl)							; get scrolled version of source byte 1
			and 	d								; extract left half of source byte 1
			exx
			xor 	(hl)							; xor with screen contents
			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 2
			exx
			ld  	a,(hl)							; get scrolled version of source byte 1 again
			and 	e								; extract right half of source byte 1
			ld		c,a								; save it
			exx

			ld  	a,(de)							; read sprite source, byte 2
			inc 	de								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 2
			ld  	a,(hl)							; get scrolled version of source byte 2
			and 	d								; extract left half of source byte 2
			or		c								; merge with right half of source byte 1
			exx
			xor 	(hl)							; xor with screen contents
			ld  	(hl),a							; set to screen
;			inc 	l								; next screen byte

			; byte 3
;			exx
;			ld  	a,(hl)							; get scrolled version of source byte 2 again
;			and 	e								; extract right half of source byte 2
;			exx
;			xor 	(hl)							; xor with screen
;			ld  	(hl),a							; set to screen

;			dec 	l								; step back to first screen byte of this line
			dec 	l
			inc		h								; step down screen one line within the cell
			djnz	.allLines						; draw all lines..

			ret


; -------------------------------------------------------------------------------------------------
_drawMasked_000
.allLines
			; byte 1
			ld  	a,(de)							; read sprite source, byte 1
			inc 	e								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 1
			ld  	a,(hl)							; get scrolled version of source byte 1
			and 	d								; extract left half of source byte 1
			exx
			xor 	(hl)							; xor with screen contents
			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 2
			exx
			ld  	a,(hl)							; get scrolled version of source byte 1 again
			and 	e								; extract right half of source byte 1
			ld		c,a								; save it
			exx

			ld  	a,(de)							; read sprite source, byte 2
			inc 	de								; next source byte
			exx
			ld  	l,a								; calc address scrolled version of source byte 2
			ld  	a,(hl)							; get scrolled version of source byte 2
			and 	d								; extract left half of source byte 2
			or		c								; merge with right half of source byte 1
			exx
			xor 	(hl)							; xor with screen contents
			ld  	(hl),a							; set to screen
			inc 	l								; next screen byte

			; byte 3
			exx
			ld  	a,(hl)							; get scrolled version of source byte 2 again
			and 	e								; extract right half of source byte 2
			exx
			xor 	(hl)							; xor with screen
			ld  	(hl),a							; set to screen

			dec 	l								; step back to first screen byte of this line
			dec 	l
			inc		h								; step down screen one line within the cell
			djnz	.allLines						; draw all lines..

			ret


; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
