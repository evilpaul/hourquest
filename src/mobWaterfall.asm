; -------------------------------------------------------------------------------------------------
; Waterfall mob
; -------------------------------------------------------------------------------------------------
	MODULE mobWaterfall
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
    STRUCT MOB_WATERFALL
FRAME_COUNTER			byte
    ENDS



; -------------------------------------------------------------------------------------------------
update
			; animate the frame counter
			ld		a,(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_WATERFALL.FRAME_COUNTER)
			inc		a
			ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_WATERFALL.FRAME_COUNTER),a
			ld		b,a
			and		%11								; update image every 4 frames
			ret		nz								; otherwise exit..

			; look up graphic frame
			ld		a,b
			and		%1100							; use two bits to give four frames
			add		a,a								; *8
			ld		b,a
			add		a,b								; *16
			add		a,b								; *24
			ld		d,0
			ld		e,a
			ld		hl,animationFrames				; add to base gfx address
			add		hl,de
			push	hl								; save gfx address

			ld		b,(ix+mobs.MOB_DATA.X_POS)		; calculate screen pos in hl
			ld		c,(ix+mobs.MOB_DATA.Y_POS)
			call	utils.getScreenAddressInHl

			pop		de								; gfx in de
			ld		b,8*3/2							; 3 chars high, draw two lines at a time
.animate	ld		a,(de)							; get odd gfx byte
			inc		de

			ld		(hl),a							; draw three chars wide (left to right)
			inc		l
			ld		(hl),a
			inc		l
			ld		(hl),a

			inc		h								; step down one line

			ld		a,(de)							; get even gfx byte
			inc		de

			ld		(hl),a							; draw three chars wide (right to left)
			dec		l
			ld		(hl),a
			dec		l
			ld		(hl),a

			call	utils.getNextScreenRowDownHl	; step down one line

			djnz	.animate						; draw all 24 lines

init
collide
			ret



; -------------------------------------------------------------------------------------------------
			; sprite graphics
animationFrames
			incbin	'build/waterfall.bin'



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
