; -------------------------------------------------------------------------------------------------
; System definitions and macros
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; defines

; colour attribute defines
COLOUR_FG_BLACK			equ		000000b
COLOUR_FG_BLUE			equ		000001b
COLOUR_FG_RED			equ		000010b
COLOUR_FG_MAGENTA		equ		000011b
COLOUR_FG_GREEN			equ		000100b
COLOUR_FG_CYAN			equ		000101b
COLOUR_FG_YELLOW		equ		000110b
COLOUR_FG_WHITE			equ		000111b
COLOUR_BG_BLACK			equ		000000b
COLOUR_BG_BLUE			equ		001000b
COLOUR_BG_RED			equ		010000b
COLOUR_BG_MAGENTA		equ		011000b
COLOUR_BG_GREEN			equ		100000b
COLOUR_BG_CYAN			equ		101000b
COLOUR_BG_YELLOW		equ		110000b
COLOUR_BG_WHITE			equ		111000b
COLOUR_FLASH			equ		10000000b
COLOUR_BRIGHT			equ		01000000b

; screen addresses
SCREEN_BASE_PIXELS		equ 	16384
SCREEN_SIZE_PIXELS		equ		2048 * 3
SCREEN_BASE_ATTRS		equ 	SCREEN_BASE_PIXELS + SCREEN_SIZE_PIXELS
SCREEN_SIZE_ATTRS		equ		32 * 24
SCREEN_SIZE				equ		SCREEN_SIZE_PIXELS + SCREEN_SIZE_ATTRS

; ports
BORDER_PORT				equ		254



; -------------------------------------------------------------------------------------------------
; macros

ALIGN_AT MACRO		address
			ds		address - $
ENDM


