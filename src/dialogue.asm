; -------------------------------------------------------------------------------------------------
; Dialogue handling
; -------------------------------------------------------------------------------------------------
	MODULE dialogue
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables



; -------------------------------------------------------------------------------------------------

DIALOG_BOX_Y		equ		16
DIALOG_BOX_WIDTH	equ		30
DIALOG_BOX_HEIGHT	equ		7



; -------------------------------------------------------------------------------------------------
; trigger
;
; Trigger a dialogue sequence
; -------------------------------------------------------------------------------------------------
; In:
;	a = Sequence ID
; Out:
;	None
; Trashed:
;	af, bc, de, hl, af', bc', de', hl', ix
; -------------------------------------------------------------------------------------------------

triggerSequence
			; look up text
			ld		hl,dialogueTexts				; base address of all dialogue texts
			ld		c,a								; c is the id
			inc		c								; c = id + 1
.findText	dec		c								; if id-- == 0 then we've got our text
			jr		z,.gotText
			ld		b,text.EOT						; otherwise search for the end of this sequence
.findNextTextLoop
			ld		a,(hl)							; find an EOT
			inc		hl
			cp		b
			jr		nz,.findNextTextLoop			; not an eot. keep searching..
			ld		a,(hl)							; a second EOT means end of sequence
			inc		hl
			cp		b
			jr		nz,.findNextTextLoop			; not a second eot. keep searching..
			jr		.findText						; end of a sequence. now go look for the next one..
.gotText

			; save the background and draw the dialog box
			push	hl
			ld		a,COLOUR_FG_WHITE|COLOUR_BG_BLUE
			ld		bc,DIALOG_BOX_Y + (32-DIALOG_BOX_WIDTH)/2*256
			ld		de, DIALOG_BOX_HEIGHT + DIALOG_BOX_WIDTH*256
			call	screenBlock.config
			call	screenBlock.save
			call	screenBlock.draw
			pop		hl

			; for each string in the dialogue sequence...
.sequenceLoop
			; clear the background and prepare to print the text
			push	hl
			call	screenBlock.clear
			ld		hl,setupText
			call	text.print
			pop		hl

			; small delay so that we hear the sound_dialogueNext noise
			ld	b,5
.audioWait	halt
			djnz	.audioWait

			; make some noise
			ld		a,sound.sound_dialogueSpeak
			call	sound.playSound

			; print dialogue text string
			call	text.print
			push	hl

			; print continue prompt and then immediately hide it
			ld		hl,continueText
			call	text.print
			ld		hl,SCREEN_BASE_ATTRS + 32-(32-DIALOG_BOX_WIDTH)/2-2 + (DIALOG_BOX_Y+DIALOG_BOX_HEIGHT-2)*32
			ld		(hl),COLOUR_FG_BLUE|COLOUR_BG_BLUE

			; short wait before continue button can be pressed
			ld		b,25
.initialWait
			halt
			djnz	.initialWait

			; flash cursor and check for continue button to be pressed
			ld		b,0
.wait		halt
			ld		hl,SCREEN_BASE_ATTRS + 32-(32-DIALOG_BOX_WIDTH)/2-2 + (DIALOG_BOX_Y+DIALOG_BOX_HEIGHT-2)*32
			ld		a,b								; b is the flash counter
			and		%10000
			jr		nz,.blinkOff
			ld		a,COLOUR_FG_WHITE|COLOUR_BG_BLUE
			jr		.setPrompt
.blinkOff	ld		a,COLOUR_FG_BLUE|COLOUR_BG_BLUE
.setPrompt	ld		(hl),a							; set prompt's colour
			inc		b

			push	bc
			call	input.update					; read input
			pop		bc
			ld		a,(MM.INPUT.RELEASED)			; a button pressed?
			and		input.A_BUTTON_MASK
			jr		z,.wait							; no.. keep waiting

			ld		a,sound.sound_dialogueNext		; make some noise
			call	sound.playSound

			pop		hl
			ld		a,(hl)							; was the last dialog text we printed followed by..
			cp		text.EOT						; ..another eot marker?
			jr		nz,.sequenceLoop				; no.. print another one

			; done with this dialogue sequence
			call	screenBlock.restore
			ret



; -------------------------------------------------------------------------------------------------

; prepare margins and cursor, ready to print dialogue text
setupText
			db		text.CC, text.CC_SET_MARGINS, (32-DIALOG_BOX_WIDTH)/2+1, DIALOG_BOX_WIDTH+(32-DIALOG_BOX_WIDTH)/2-2
			db		text.CC, text.CC_SET_POS, (32-DIALOG_BOX_WIDTH)/2+1, DIALOG_BOX_Y+1
			db		text.EOT

; show continue prompt
continueText
			db		text.CC, text.CC_SET_POS, DIALOG_BOX_WIDTH+(32-DIALOG_BOX_WIDTH)/2-2, DIALOG_BOX_Y+DIALOG_BOX_HEIGHT-2
			db		104
			db		text.EOT



; -------------------------------------------------------------------------------------------------

dialogueTexts

._000		db		"HI, MY NAME IS FRANK!", text.CC, text.CC_NEWLINE
			db		text.EOT
			db		"IT'S GREAT TO EAT YOU.. ER..I MEAN MEET YOU!"
			db		text.EOT
			db		"I'M HUNGRY! PLEASE CAN YOU", text.CC, text.CC_NEWLINE,"BRING ME SOME FOOD?"
			db		text.EOT
			db		text.EOT

._001		db		"HI, MY NAME IS STILL FRANK!", text.CC, text.CC_NEWLINE
			db		text.EOT
			db		"AND I'M STILL HUNGRY! PLEASECAN YOU BRING ME SOME FOOD?"
			db		text.EOT
			db		text.EOT



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
