; -------------------------------------------------------------------------------------------------
; Items
; -------------------------------------------------------------------------------------------------
	MODULE items
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables

	STRUCT MM_ITEMS
OWNED_FLAGS			block NUM_ITEMS					; is owned (0 or 1) for each item
HELD_ITEM_A			byte							; id of item held in slot a, or -1 for none
HELD_ITEM_B			byte							; id of item held in slot b, or -1 for none
ACTIVATED_ITEM_A	byte							; id of item activated in slot a, or -1 for none
ACTIVATED_ITEM_B	byte							; id of item activated in slot b, or -1 for none
	ENDS



; -------------------------------------------------------------------------------------------------

	STRUCT ITEM_DEF
GFX						word						; pointer to items graphics (32 bytes)
COLOUR					byte						; attribute colour in ui
DESCRIPTION				word						; pointer to description text + eot
ACTIVATE_FUNCTION		word						; function to call when the item is activated
DEACTIVATE_FUNCTION		word						; function to call when the item is deactivated
_SIZE
	ENDS

NUM_ITEMS	equ			12

itemDefs
			dw		itemBombGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_WHITE
			dw		descriptionBomb
			dw		itemBombActivate
			dw		itemNullFunction

			dw		itemBootGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_YELLOW
			dw		descriptionBoot
			dw		itemBootActivate
			dw		itemBootDeactivate

			dw		itemCrossGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_YELLOW
			dw		descriptionCross
			dw		itemCrossActivate
			dw		itemCrossDeactivate

			dw		itemGemGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_MAGENTA
			dw		descriptionGem
			dw		itemNullFunction
			dw		itemNullFunction

			dw		itemKeyGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_WHITE
			dw		descriptionKey1
			dw		itemNullFunction
			dw		itemNullFunction

			dw		itemKeyGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_CYAN
			dw		descriptionKey2
			dw		itemNullFunction
			dw		itemNullFunction

			dw		itemKeyGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_YELLOW
			dw		descriptionKey3
			dw		itemNullFunction
			dw		itemNullFunction

			dw		itemKeyGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_GREEN
			dw		descriptionKey4
			dw		itemNullFunction
			dw		itemNullFunction

			dw		itemLeafGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_GREEN
			dw		descriptionLeaf
			dw		itemNullFunction
			dw		itemNullFunction

			dw		itemMoneyGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_YELLOW
			dw		descriptionMoney
			dw		itemNullFunction
			dw		itemNullFunction

			dw		itemWrenchGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_CYAN
			dw		descriptionWrench1
			dw		itemNullFunction
			dw		itemNullFunction

			dw		itemWrenchGfx
			db		COLOUR_BG_BLUE|COLOUR_FG_GREEN
			dw		descriptionWrench2
			dw		itemNullFunction
			dw		itemNullFunction


descriptionBomb		db		"     BOMBS      $"
descriptionBoot		db		"  SPEED BOOTS   $"
descriptionCross	db		"     CROSS      $"
descriptionGem		db		"  A LARGE GEM   $"
descriptionKey1		db		"MYSTERIOUS KEY 1$"
descriptionKey2		db		"MYSTERIOUS KEY 2$"
descriptionKey3		db		"MYSTERIOUS KEY 3$"
descriptionKey4		db		"MYSTERIOUS KEY 4$"
descriptionLeaf		db		"  A WAXY LEAF   $"
descriptionMoney	db		"  BAG OF MONEY  $"
descriptionWrench1	db		" SIZE 1 WRENCH  $"
descriptionWrench2	db		" SIZE 2 WRENCH  $"



; -------------------------------------------------------------------------------------------------
; doesPlayerHaveItem
;
; Determines if the player is carrying an item or not
; -------------------------------------------------------------------------------------------------
; In:
;	a = Id of item to check
; Out:
;	Zero flag set if item held, otherwise unset
; Trashed:
;	af, de, hl
; -------------------------------------------------------------------------------------------------

doesPlayerHaveItem
			ld		hl,MM.ITEMS.OWNED_FLAGS			; items flags are held here
			ld		d,0
			ld		e,a
			add		hl,de							; add offset to the item 
			ld		a,(hl)							; get current value
			or		a								; set z flag
			ret



; -------------------------------------------------------------------------------------------------
; getItemDef
;
; Get a pointer to an items definition
; -------------------------------------------------------------------------------------------------
; In:
;	a = Id of item to get
; Out:
;	ix => Item definition
; Trashed:
;	af, de, ix
; -------------------------------------------------------------------------------------------------

getItemDef
			ld		ix,items.itemDefs - items.ITEM_DEF._SIZE	; point one item before base of items 
			ld		de,items.ITEM_DEF._SIZE
.lookup		add		ix,de										; point to next item
			dec		a											; count down
			jp		p,.lookup									; keep going unless we just reached the item we're after
			ret



; -------------------------------------------------------------------------------------------------
; update
;
; Activate and deactivate items based on button presses
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, bc, de, hl, ix
; -------------------------------------------------------------------------------------------------

update
			; deal with A button
.tryA		ld		a,(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_A)	; info for A item
			ld		c,a								; item id in c
			call	getItemDef						; item def in ix
			ld		a,(MM.ITEMS + items.MM_ITEMS.ACTIVATED_ITEM_A)
			ld		b,a								; activated item in b

			cp		-1								; is there any active item in A?
			jr		z,.noSwapA						; no
			cp		c								; yes, has is it different to what we're holding?
			jr		z,.noSwapA						; no
			ld		a,b								; yes - need to deactivate it
			call	getItemDef						; item def for currently active item in ix
			jr		.deactivateA
.noSwapA

			ld		a,(MM.INPUT.DOWN)				; is A button pressed?
			and		a,input.A_BUTTON_MASK
			jr		z,.buttonUpA
.buttonDownA
			ld		a,c
			cp		b
			jr		z,.doneA
.activateA	ld		(MM.ITEMS + items.MM_ITEMS.ACTIVATED_ITEM_A),a	; remember that this item is active on A
			ld		h,(ix + ITEM_DEF.ACTIVATE_FUNCTION + 1)	; yes, get the activate function
			ld		l,(ix + ITEM_DEF.ACTIVATE_FUNCTION + 0)
			ld		de,.doneA
			push	de
			jp		(hl)							; call activate function
.buttonUpA
			ld		a,c
			cp		b
			jr		nz,.doneA
.deactivateA
			ld		a,-1
			ld		(MM.ITEMS + items.MM_ITEMS.ACTIVATED_ITEM_A),a	; remember that no item is active on A
			ld		h,(ix + ITEM_DEF.DEACTIVATE_FUNCTION + 1)	; yes, get the deactivate function
			ld		l,(ix + ITEM_DEF.DEACTIVATE_FUNCTION + 0)
			ld		de,.doneA
			push	de
			jp		(hl)							; call deactivate function
.doneA

			; deal with B button
.tryB		ld		a,(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_B)	; info for B item
			ld		c,a								; item id in c
			call	getItemDef						; item def in ix
			ld		a,(MM.ITEMS + items.MM_ITEMS.ACTIVATED_ITEM_B)
			ld		b,a								; activated item in b

			cp		-1								; is there any active item in B?
			jr		z,.noSwapB						; no
			cp		c								; yes, has is it different to what we're holding?
			jr		z,.noSwapB						; no
			ld		a,b								; yes - need to deactivate it
			call	getItemDef						; item def for currently active item in ix
			jr		.deactivateB
.noSwapB

			ld		a,(MM.INPUT.DOWN)				; is B button pressed?
			and		a,input.B_BUTTON_MASK
			jr		z,.buttonUpB
.buttonDownB
			ld		a,c
			cp		b
			jr		z,.doneB
.activateB	ld		(MM.ITEMS + items.MM_ITEMS.ACTIVATED_ITEM_B),a	; remember that this item is active on B
			ld		h,(ix + ITEM_DEF.ACTIVATE_FUNCTION + 1)	; yes, get the activate function
			ld		l,(ix + ITEM_DEF.ACTIVATE_FUNCTION + 0)
			ld		de,.doneB
			push	de
			jp		(hl)							; call activate function
.buttonUpB
			ld		a,c
			cp		b
			jr		nz,.doneB
.deactivateB
			ld		a,-1
			ld		(MM.ITEMS + items.MM_ITEMS.ACTIVATED_ITEM_B),a	; remember that no item is active on B
			ld		h,(ix + ITEM_DEF.DEACTIVATE_FUNCTION + 1)	; yes, get the deactivate function
			ld		l,(ix + ITEM_DEF.DEACTIVATE_FUNCTION + 0)
			ld		de,.doneB
			push	de
			jp		(hl)							; call deactivate function
.doneB

			ret


; -------------------------------------------------------------------------------------------------
; Null function for an item whose activate and/or deactivate function does nothing

itemNullFunction
			ret



; -------------------------------------------------------------------------------------------------
; Bomb functions

itemBombActivate
			ld		a,sound.sound_bombDrop			; make some noise
			call	sound.playSound

			ld		a,(MM.PLAYER.X_POS)				; get position at bottom of player's feet
			ld		e,a
			ld		a,(MM.PLAYER.Y_POS)
			add		a,7
			ld		d,a
			ld		b,mobs.MOB_TYPE_BOMB			; spawn a bomb
			jp		mobs.spawn



; -------------------------------------------------------------------------------------------------
; Boot functions

itemBootActivate
			ld		a,1
			ld		(MM.PLAYER.BOOTS_ACTIVE),a		; set flag in player
			ret

itemBootDeactivate
			xor		a
			ld		(MM.PLAYER.BOOTS_ACTIVE),a		; clear flag in player
			ret



; -------------------------------------------------------------------------------------------------
; Cross functions

itemCrossActivate
			ld		a,1
			ld		(MM.PLAYER.CROSS_ACTIVE),a		; set flag in player
			ret

itemCrossDeactivate
			xor		a
			ld		(MM.PLAYER.CROSS_ACTIVE),a		; clear flag in player
			ret



; -------------------------------------------------------------------------------------------------
; item graphics

			ALIGN	2
itemBombGfx		incbin	'build/itemBomb.bin'
itemBootGfx		incbin	'build/itemBoot.bin'
itemCrossGfx	incbin	'build/itemCross.bin'
itemGemGfx		incbin	'build/itemGem.bin'
itemKeyGfx		incbin	'build/itemKey.bin'
itemLeafGfx		incbin	'build/itemLeaf.bin'
itemMoneyGfx	incbin	'build/itemMoney.bin'
itemWrenchGfx	incbin	'build/itemWrench.bin'

itemUnknownGfx	incbin	'build/itemUnknown.bin'



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
