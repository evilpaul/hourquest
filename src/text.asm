; -------------------------------------------------------------------------------------------------
; Text printing routines
; -------------------------------------------------------------------------------------------------
	MODULE text
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables

	STRUCT MM_TEXT
YX_POS
Y_POS					byte
X_POS					byte
LEFT_RIGHT_MARGIN
RIGHT_MARGIN			byte
LEFT_MARGIN				byte
	ENDS



; -------------------------------------------------------------------------------------------------
; control codes that the print routine recognises

EOT					equ		'$'						; place this at the end of the message string
CC					equ		'^'						; start control code, then one of:
CC_NEWLINE			equ		0						;	start a new line, data: none
CC_RESET_MARGINS	equ		1						; 	reset margins, data: none
CC_SET_MARGINS		equ		2						;	set margins, data: left margin, right margin
CC_SET_POS			equ		3						;	set cursor position, data: x position, y position



; -------------------------------------------------------------------------------------------------
; print
;
; Print a text string to the screen. Handles embedded control codes
; -------------------------------------------------------------------------------------------------
; In:
;	hl => text message to print
; Out:
;	hl => Next byte after the last EOT marker
; Trashed:
;	af, bc, de, hl, af', bc', de', hl', ix
; -------------------------------------------------------------------------------------------------
print

			; look up and cache the screen position
.cacheAddress	
			push	hl								; save the text pointer
			ld		bc,(MM.TEXT.YX_POS)				; look up pixel address
			call	utils.getScreenCellPixelAddressInHl
			ex		de,hl							; destination pixel address cached in de
			exx
			ld		bc,(MM.TEXT.YX_POS)				; cursor address cached in bc'
			call	utils.getScreenCellAttrAddressInHl	; look up attribute address and cache in hl'
			exx
			pop		hl								; restore the text pointer

			; look up and cache the print style
.cacheStyle	exx
			ld		d,COLOUR_FG_WHITE|COLOUR_BG_BLUE	; çache colour in d'
			ld		a,(MM.TEXT.RIGHT_MARGIN)
			ld		e,a								; cache right margin in e'
			exx

			; process a single character
			; at this point:
			;	hl => text message
			;	de => screen pixel address
			;	b' = cursor x position
			;	c' = cursor y position
			;	d' = attribute colour
			;	hl' => screen attribute address
.processChar
			ld		a,(hl)							; read next character
			inc		hl								; advance text pointer to the next character
			cp		EOT
			jr		z,.end							; handle EOT
			cp		CC
			jr		z,.controlCode					; handle control codes
			
			push	hl								; save the text pointer

			add		a,a								; look up the font address for this character = font base + a*8
			ld		h,0
			ld		l,a								; hl = character *2
			add		hl,hl							; *4
			add		hl,hl							; *8
			ld		a,h
			add		a,HIGH (font_base)				; add in font base (only need the high byte)
			ld		h,a								; hl => character gfx data

			ld		c,d								; save high byte of screen address
	REPT	7										; unrolled loop to draw first 7 rows of character
			ld		a,(hl)							; get source
			inc		l								; advance source pointer
			ld		(de),a							; write destination
			inc		d								; jump destination down one screen line
	ENDR
			ld		a,(hl)							; get source
			ld		(de),a							; write destination
			ld		d,c								; restore high byte of screen address, now we're pointing back at the first line of the character
			inc		e								; advance x cursor position on screen

			pop		hl								; restore the text pointer

			exx
			ld		(hl),d							; write attribute colour to screen
			inc		l								; advance to next attribute

			inc		b								; advance the cached x cursor position
			ld		a,e								; reached past cached value of right margin?
			cp		b
			jr		c,.xWrap						; yes..
			exx										; switch normal register set back in
			jp		.processChar					; next character

.xWrap		ld		a,(MM.TEXT.LEFT_MARGIN)			; wrap back to left margin
			ld		b,a
			inc		c								; and step down one line
			ld		a,c								; reached past end of screen?
			cp		24
			jr		c,.noYWrap						; no..
			ld		c,23							; yes, pin text to last screen line so that it doesn't overflow

.noYWrap	ld		(MM.TEXT.YX_POS),bc				; write cached position back to memory
			exx										; switch normal register set back in
			jp		.cacheAddress					; re-enter the print routine and get screen address again


.end		exx
			ld		(MM.TEXT.YX_POS),bc				; write cached cursor position back to memory
			exx
			ret										; finished printing this message


			; handle a control code using a jump table
.controlCode
			ld		a,(hl)							; read the control code
			inc		hl								; advance to next char..
			push	hl								; save the text pointer
			ld		d,0
			add		a,a
			ld		e,a								; control code *2 in de
			ld		hl,.cc_jtab
			add		hl,de							; add de to base of the jump table, hl now points at address of handler routine
			ld		e,(hl)							; read address to handle this code from the jump table
			inc		hl
			ld		d,(hl)
			pop		hl								; restore the text pointer
			push	de								; push routine address..
			ret										; ..and jump to it

			; jump table for control codes
.cc_jtab	dw		.cc_newline						; CC_NEWLINE
			dw		.cc_reset_margins				; CC_RESET_MARGINS
			dw		.cc_set_margins					; CC_SET_MARGINS
			dw		.cc_set_pos						; CC_SET_POS


			; control code - newline
.cc_newline
			ld		a,(MM.TEXT.LEFT_MARGIN)			; move x position to start of line
			ld		(MM.TEXT.X_POS),a
			exx
			ld		a,c								; get current cached y position
			exx
			cp		23
			jp		nc,.cacheAddress				; if we're already on the bottom line then don't advance
			inc		a								; otherwise, step down a line
			ld		(MM.TEXT.Y_POS),a
			jp		.cacheAddress					; we need to recalculate the screen position now

			; control code - reset margins
.cc_reset_margins
			ld		bc,0*256 + 31*1					; reset margins to full screen width
			ld		(MM.TEXT.LEFT_RIGHT_MARGIN),bc
			jp		.processChar					; carry on processing chars

			; control code - set margins
.cc_set_margins
			ld		b,(hl)							; read new margins
			inc		hl
			ld		c,(hl)
			inc		hl
			ld		(MM.TEXT.LEFT_RIGHT_MARGIN),bc	; set margins
			jp		.processChar					; carry on processing chars

			; control code - set cursor position
.cc_set_pos
			ld		b,(hl)							; read new cursor position
			inc		hl
			ld		c,(hl)
			inc		hl
			ld		(MM.TEXT.YX_POS),bc				; set cursor position
			jp		.cacheAddress					; we need to recalculate the screen position now



; -------------------------------------------------------------------------------------------------
			ALIGN	256
font		incbin	'build/font.bin'
font_base	equ		font - 32*8



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
