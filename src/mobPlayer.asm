; -------------------------------------------------------------------------------------------------
; Player mob
; -------------------------------------------------------------------------------------------------
	MODULE mobPlayer
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables and defines

	STRUCT MM_PLAYER
X_POS					byte
Y_POS					byte
X_VELOCITY				byte
Y_VELOCITY				byte
DAMAGE_FLASH_COUNTER	byte
CURRENT_HEALTH			byte
MAX_HEALTH				byte
BOOTS_ACTIVE			byte
CROSS_ACTIVE			byte
	ENDS


; how long to flash for when taking damage
; also.. how long before player can take damage again
DAMAGE_FLASH_TIME		equ		100


; ui defines
UI_HEALTH_BAR_POS_X		equ		1
UI_HEALTH_BAR_POS_Y		equ		1

UI_ITEM_POS_X			equ		32-7
UI_ITEM_POS_Y			equ		1



; -------------------------------------------------------------------------------------------------
	STRUCT MOB_PLAYER
ANIM_FRAME				byte
LAST_TRIGGER_ID			byte
	ENDS



; -------------------------------------------------------------------------------------------------
init
			; draw the ui
			call	drawHealth
			call	drawItemUi

			ret



; -------------------------------------------------------------------------------------------------
update
			; cheap and temporary button highlights on the ui
			ld		a,(MM.INPUT.DOWN)				; get inputs
			ld		b,a
			bit		input.A_BUTTON,b				; a button pressed?
			ld		a,COLOUR_FG_WHITE|COLOUR_BRIGHT
			jr		nz,.noA
			ld		a,COLOUR_FG_WHITE
.noA		ld		(SCREEN_BASE_ATTRS + UI_ITEM_POS_X+2 + 32*(UI_ITEM_POS_Y+1)),a	; set a button colour
			bit		input.B_BUTTON,b				; b button pressed?
			ld		a,COLOUR_FG_WHITE|COLOUR_BRIGHT
			jr		nz,.noB
			ld		a,COLOUR_FG_WHITE
.noB		ld		(SCREEN_BASE_ATTRS + UI_ITEM_POS_X+5 + 32*(UI_ITEM_POS_Y+1)),a	; set b button colour

			; get move configuration
			ld		a,(MM.PLAYER.BOOTS_ACTIVE)
			or		a								; are boots active?
			ld		a,1*2							; normal max velocity
			ld		c,2								; normal acceleration speed in c
			jr		z,.noBoots
			ld		a,4*2							; speed boots max velocity
			ld		c,4								; speed boots acceleration speed in c
.noBoots	ld		h,a								; max +ve velocity in h
			neg
			ld		l,a								; max -ve velocity in l

			; read input and move
			ld		a,(MM.INPUT.DOWN)
			ld		b,a								; input bits in b
			ld		de,(MM.PLAYER.X_VELOCITY)		; vx in e, vy in d

			; process y velocity
.Y			ld		a,d								; store current y velocity in a
.tryDown	bit		input.DOWN_BUTTON,b				; try accelerating up
			jr		z,.tryUp
			add		a,c								; increase +ve velocity 
			cp		h								; cap it if it exceeds max velocity
			jr		c,.doneY
			ld		a,h
			jr		.doneY
.tryUp		bit		input.UP_BUTTON,b				; try accelerating down
			jr		z,.noY
			sub		c								; increase -ve velocity 
			cp		l								; cap it if it exceeds max velocity
			jr		nc,.doneY
			ld		a,l
			jr		.doneY
.noY		cp		0								; no acceleration, try decelerating
			jr		z,.doneY						; already stationary
			jp		p,.yPos
.yNeg		inc		a								; acceleration is -ve, reduce it towards 0
			jr		.doneY
.yPos		dec		a								; acceleration is +ve, reduce it towards 0
.doneY		ld		d,a								; store new y velocity back into d

			; process x velocity
.x			ld		a,e								; store current x velocity in a
.tryRight	bit		input.RIGHT_BUTTON,b			; try accelerating right
			jr		z,.tryLeft
			add		a,c								; increase +ve velocity 
			cp		h								; cap it if it exceeds max velocity
			jr		c,.doneX
			ld		a,h
			jr		.doneX
.tryLeft	bit		input.LEFT_BUTTON,b				; try accelerating left
			jr		z,.noX
			sub		c								; increase -ve velocity 
			cp		l								; cap it if it exceeds max velocity
			jr		nc,.doneX
			ld		a,l
			jr		.doneX
.noX		cp		0								; no acceleration, try decelerating
			jr		z,.doneX						; already stationary
			jp		p,.xPos
.xNeg		inc		a								; acceleration is -ve, reduce it towards 0
			jr		.doneX
.xPos		dec		a								; acceleration is +ve, reduce it towards 0
.doneX		ld		e,a								; store new x velocity back into e

			; finished updating the velocity, now use it
			ld		(MM.PLAYER.X_VELOCITY),de		; save the new velocity
			sra		d								; divide vy by 2 to get actual vy
			sra		e								; divide vx by 2 to get actual vx
			call	mobs.move						; now do the actual movement, based on velocity

			; advance sprite animation frame if there is any movement, otherwise reset it
			ld		a,(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_PLAYER.ANIM_FRAME)
			inc		a
			ld		b,a
			ld		de,(MM.PLAYER.X_VELOCITY)
			ld		a,d
			or		e
			jr		nz,.setFrame
			ld		b,0								; no velocity, reset anim frame to 0
.setFrame	ld		a,b
			ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_PLAYER.ANIM_FRAME),a ; save anim frame to mob data

			; set gfx for this anim frame
			ld		a,(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_PLAYER.ANIM_FRAME)
			and		%1100							; mask to get anim frame, 0-3 but multiped by 4
			ld		d,a
			add		a,a								; * 8
			add		a,a								; * 16
			add		a,a								; * 32
			add		a,d								; * 32 + 4
			ld		d,0
			ld		e,a								; de = anim frame * 36
			ld		hl,gfx							; base of gfx
			add		hl,de							; point to gfx for this frame
			ld		(ix+mobs.MOB_DATA.GFX_DATA+1),h
			ld		(ix+mobs.MOB_DATA.GFX_DATA+0),l	; save gfx frame to mob data

			; what room are we in and where are we stood?
			ld		a,(MM.ROOM.ROOM_ID)
			ld		d,a
			ld		b,(ix+mobs.MOB_DATA.X_POS)
			ld		c,(ix+mobs.MOB_DATA.Y_POS)

			; check for moving into a new room
.checkX		ld		a,b
.checkLeft	cp		4
			jr		nc,.checkRight
			ld		b,256-4-15
			dec		d
			jr		.jumpRoom
.checkRight	cp		256-4-15
			jr		c,.checkY
			ld		b,4
			inc		d
			jr		.jumpRoom

.checkY		ld		a,c
.checkTop	cp		4
			jr		nc,.checkBot
			ld		c,192-4-15
			ld		a,d
			sub		4
			ld		d,a
			jr		.jumpRoom
.checkBot	cp		192-4-15
			jr		c,.ok
			ld		c,8
			ld		a,d
			add		a,4
			ld		d,a
			;jr		.jumpRoom

			; we changed to a new room, so jump to it
.jumpRoom	ld		a,d
			ld		(MM.ROOM.ROOM_ID),a
			ld		a,1
			ld		(MM.ROOM.CHANGE_ROOM_FLAG),a

			; save position to global state
.ok			ld		a,b
			ld		(MM.PLAYER.X_POS),a
			ld		a,c
			ld		(MM.PLAYER.Y_POS),a

			; check against triggers. we only trigger when the player first enters a new trigger zone
			call	mobs.checkTriggers
			cp		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_PLAYER.LAST_TRIGGER_ID)		; did we enter a different trigger zone?
			ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_PLAYER.LAST_TRIGGER_ID),a	; remember id so that we can compare next frame
			jr		z,.noTrigger													; no change of zone...
			ld		(MM.SCRIPT.GLOBAL_VARS.TRIGGER_ID),a							; new zone! save trigger id
			or		a																; are we in a zone now, or out of one?
			jr		z,.noTrigger													; not in a zone..
			push	ix
			ld		b,room.ROOM_SCRIPT_ONTRIGGER									; run the onTrigger script
			call	room.runScript
			pop		ix
.noTrigger

			; flash if taking damage
			ld		hl,MM.PLAYER.DAMAGE_FLASH_COUNTER
			ld		a,(hl)							; get current value of flash counter
			or		a								; are we sill recovering from a hit?
			jp		z,.noFlash						; no..
			dec		(hl)							; yes, decrease the counter
			and		%100							; flash every 8 frames
			jp		nz,.noFlash
			ld		(ix+mobs.MOB_DATA.GFX_DATA+0),a	; flashing "off" by setting gfx pointer to 0
			ld		(ix+mobs.MOB_DATA.GFX_DATA+1),a
.noFlash
			ret



; -------------------------------------------------------------------------------------------------
collide
			ld		hl,MM.PLAYER.DAMAGE_FLASH_COUNTER
			ld		a,(hl)							; are we recovering from a hit?
			or		a
			ret		nz								; yes - take no more damage
			
			ld		(hl),DAMAGE_FLASH_TIME			; no - set the timer
			ld		hl,MM.PLAYER.CURRENT_HEALTH		; reduce health
			dec		(hl)
			call	drawHealth						; redraw health

			ld		a,sound.sound_playerHurt		; make some noise
			call	sound.playSound

			ret



; -------------------------------------------------------------------------------------------------
; drawItemUi
;
; Draws player's items to the screen
; -------------------------------------------------------------------------------------------------
; In:
; 	None
; Out:
; 	None
; Trashed:
;	af, bc, de, hl
; -------------------------------------------------------------------------------------------------

drawItemUi
			push	ix
			push	iy

			ld		a,(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_A)
			ld		iy,uiConfigItemA
			call	drawSingleUiItem

			ld		a,(MM.ITEMS + items.MM_ITEMS.HELD_ITEM_B)
			ld		iy,uiConfigItemB
			call	drawSingleUiItem

			pop		iy
			pop		ix
			ret



; -------------------------------------------------------------------------------------------------
; drawSingleUiItem
;
; Draw a single ui item to the screen. Also writes 'hide' values to the collision map so that
; sprites will move behind it
; -------------------------------------------------------------------------------------------------
; In:
; 	a = Item id
;	iy => Display config
; Out:
; 	None
; Trashed:
;	af, bc, de, hl
; -------------------------------------------------------------------------------------------------

drawSingleUiItem
			; look up the items definition
			call	items.getItemDef
			ld		h,(ix + items.ITEM_DEF.GFX+1)			; read gfx address
			ld		l,(ix + items.ITEM_DEF.GFX+0)

			ld		d,(iy+UI_CONFIG_ITEM.PIXEL_POSITION+1)	; where to draw the item pixels
			ld		e,(iy+UI_CONFIG_ITEM.PIXEL_POSITION+0)

			; draw 16x16 icon
			ld		c,2								; two char cells high
.allCharLines
			ld		b,8								; 8 line per char cell
.oneCharLine
			ld		a,(hl)							; draw first byte
			ld		(de),a
			inc		l
			inc		e
			ld		a,(hl)							; draw second byte
			ld		(de),a
			inc		hl
			dec		e
			inc		d
			djnz	.oneCharLine					; draw all line in the char cell
			ld		a,d								; step down one line
			sub		8
			ld		d,a
			ld		a,e
			add		a,32
			ld		e,a
			dec		c								; draw all char cells
			jr		nz,.allCharLines

			; draw 8x8 button icon
			ld		hl,-32 + 2						; step to the right of the icon
			add		hl,de
			ex		hl,de							; hl => where to draw the button pixels
			ld		h,(iy+UI_CONFIG_ITEM.BUTTON_ICON+1)	; point to button gfx in font
			ld		l,(iy+UI_CONFIG_ITEM.BUTTON_ICON+0)

			ld		b,8								; 8 lines high
.drawAllButtonChar
			ld		a,(hl)
			ld		(de),a
			inc		l
			inc		d
			djnz	.drawAllButtonChar				; draw all 8 lines

			; draw colours
			ld		h,(iy+UI_CONFIG_ITEM.ATTRIBUTE_POSITION+1)	; where to draw colours
			ld		l,(iy+UI_CONFIG_ITEM.ATTRIBUTE_POSITION+0)
			ld		b,(ix + items.ITEM_DEF.COLOUR)	; item colour
			ld		(hl),b							; icon colour 11
			inc		l
			ld		(hl),b							; icon colour 21
			ld		de,32-1
			add		hl,de
			ld		(hl),b							; icon colour 12
			inc		l
			ld		(hl),b							; icon colour 22
			inc		l
			ld		(hl),COLOUR_FG_WHITE|COLOUR_BG_BLUE	; button colour 23

			; draw collision			
			ld		h,(iy+UI_CONFIG_ITEM.COLLISION_MAP_POSITION+1)	; where to draw collision
			ld		l,(iy+UI_CONFIG_ITEM.COLLISION_MAP_POSITION+0)
			ld		b,00000010						; mask for hide bit
			ld		a,(hl)							; set hide bit 11
			or		b
			ld		(hl),a
			inc		l
			ld		a,(hl)							; set hide bit 12
			or		b
			ld		(hl),a
			ld		de,32-1
			add		hl,de
			ld		a,(hl)							; set hide bit 21
			or		b
			ld		(hl),a
			inc		l
			ld		a,(hl)							; set hide bit 22
			or		b
			ld		(hl),a
			inc		l
			ld		a,(hl)							; set hide bit 23
			or		b
			ld		(hl),a

			ret



; -------------------------------------------------------------------------------------------------
; drawHealth
;
; Draws player's health bar to the screen. Also writes 'hide' values to the collision map so that
; sprites will move behind it. Each heart container equals two health points
; -------------------------------------------------------------------------------------------------
; In:
; 	None
; Out:
; 	None
; Trashed:
;	af, bc, de, hl, bc', de', hl'
; -------------------------------------------------------------------------------------------------

drawHealth
			exx
			ld			hl,SCREEN_BASE_ATTRS + UI_HEALTH_BAR_POS_X + 32*UI_HEALTH_BAR_POS_Y		; hl => screen attributes for health bar
			ld			b,COLOUR_FG_RED|COLOUR_BG_BLUE		; colour
			ld			de,MM.ROOM.COLLISION_MAP + UI_HEALTH_BAR_POS_X + 32*UI_HEALTH_BAR_POS_Y	; de => collision map for health bar
			ld			c,%00000010							; masks for collision map
			exx
			ld			de,SCREEN_BASE_PIXELS + UI_HEALTH_BAR_POS_X + 32*UI_HEALTH_BAR_POS_Y	; de => screen pixels for health bar

			ld			a,(MM.PLAYER.MAX_HEALTH)			; maximum health
			ld			b,a
			ld			a,(MM.PLAYER.CURRENT_HEALTH)		; current health
			ld			c,a

.drawAll	dec			c
			push		bc

			; figure out the correct heart graphic
			ld			hl,heartGfx+8*0
			bit			7,c
			jr			nz,.gotGfx
			ld			a,c
			ld			hl,heartGfx+8*1
			or			a
			jr			z,.gotGfx
			ld			hl,heartGfx+8*2
.gotGfx

			; draw pixels for heart gfx
			ld			b,8
.drawOne	ld			a,(hl)						; read source
			inc			hl
			ld			(de),a						; set destination
			inc			d
			djnz		.drawOne					; draw all 8 rows
			ld			a,d							; move back up 8 rows
			sub			8
			ld			d,a
			inc			e							; step right one cell

			; set colour and collision
			exx
			ld			(hl),b						; set screen colour
			inc			l
			ld			a,(de)						; read collision value
			or			c							; add in 'hide' bit
			ld			(de),a						; set collision value
			inc			e
			exx

			pop			bc
			dec			c
			dec			b
			djnz		.drawAll

			ret



; -------------------------------------------------------------------------------------------------
; Config data to describe where the item icons should be drawn on the screen

	STRUCT UI_CONFIG_ITEM
PIXEL_POSITION			word
ATTRIBUTE_POSITION		word
BUTTON_ICON				word
COLLISION_MAP_POSITION	word
	ENDS

uiConfigItemA
			dw		SCREEN_BASE_PIXELS + UI_ITEM_POS_X + 32*UI_ITEM_POS_Y
			dw		SCREEN_BASE_ATTRS + UI_ITEM_POS_X + 32*UI_ITEM_POS_Y
			dw		text.font + 72*8
			dw		MM.ROOM.COLLISION_MAP + UI_ITEM_POS_X + 32*UI_ITEM_POS_Y

uiConfigItemB
			dw		SCREEN_BASE_PIXELS + UI_ITEM_POS_X + 32*UI_ITEM_POS_Y + 3
			dw		SCREEN_BASE_ATTRS + UI_ITEM_POS_X + 32*UI_ITEM_POS_Y + 3
			dw		text.font + 73*8
			dw		MM.ROOM.COLLISION_MAP + UI_ITEM_POS_X + 32*UI_ITEM_POS_Y + 3



; -------------------------------------------------------------------------------------------------
			; sprite graphics
			ALIGN	2
gfx			incbin	'build/knight.bin'
heartGfx	incbin	'build/hearts.bin'



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
