; -------------------------------------------------------------------------------------------------
; Dead mob
; -------------------------------------------------------------------------------------------------
	MODULE mobDead
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
    STRUCT MOB_DEAD
TIMER					byte
    ENDS



; -------------------------------------------------------------------------------------------------
; dead mob
; a mob that has no update and no collide, and no gfx. use this to clear out an existing mob when
; it dies. just call the init function from inside another mob's update/collide/whatever function
; note that the mob's original type is retained
; -------------------------------------------------------------------------------------------------

; -------------------------------------------------------------------------------------------------
init
			xor		a
			ld		(ix+mobs.MOB_DATA.GFX_DATA+1),a			; clear gfx
			ld		(ix+mobs.MOB_DATA.GFX_DATA+0),a
			ld		(ix+mobs.MOB_DATA.COLLISION_CAST),a		; clear collision
			ld		(ix+mobs.MOB_DATA.COLLISION_RECEIVE),a
			ld		hl,update								; set update
			ld		(ix+mobs.MOB_DATA.FUNC_UPDATE+1),h
			ld		(ix+mobs.MOB_DATA.FUNC_UPDATE+0),l
			ld		hl,collide								; set collide
			ld		(ix+mobs.MOB_DATA.FUNC_COLLIDE+1),h
			ld		(ix+mobs.MOB_DATA.FUNC_COLLIDE+0),l

			; keep this 'dead' mob around for two frames
			ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_DEAD.TIMER),2
			ret

update
			dec		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_DEAD.TIMER)	; one less frame to live..
			ret		nz
			ld		(ix+mobs.MOB_DATA.TYPE),0						; ..and we're done
collide
			ret



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
