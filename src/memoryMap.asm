; -------------------------------------------------------------------------------------------------
; RAM Memory map
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; define the layout of the RAM memory map

	STRUCT	MEMORY_MAP
			; here are all the variables from the various game systems
INPUT		input.MM_INPUT
INVENTORY	inventory.MM_INVENTORY
ITEMS		items.MM_ITEMS
MOBS		mobs.MM_MOBS
PLAYER		mobPlayer.MM_PLAYER
ROOM		room.MM_ROOM
SOUND		sound.MM_SOUND
SCRIPT		script.MM_SCRIPT
TEXT		text.MM_TEXT
UTILS		utils.MM_UTILS
SCRATCH		block 1024*3							; 3k scratch buffer

			; finally, some system variables
			block	32*2							; space for the stack
STACK												; initial stack head
_SIZE
	ENDS



; -------------------------------------------------------------------------------------------------
; place global variables right after the screen
MM_START	equ			SCREEN_BASE_PIXELS + SCREEN_SIZE
MM			MEMORY_MAP = MM_START
