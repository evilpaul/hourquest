; -------------------------------------------------------------------------------------------------
; Bomb mob
; -------------------------------------------------------------------------------------------------
	MODULE mobBomb
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables and defines

    STRUCT MOB_BOMB
STATE					byte
STATE_TIMER				byte
    ENDS

; states
STATE_BURNING_FUSE		equ		0
STATE_FINAL_COUNTDOWN	equ		1
STATE_EXPLODING			equ		2

; how long in each state
BURNING_FUSE_TIME		equ		125
FINAL_COUNTDOWN_TIME	equ		25
EXPLODING_TIME			equ		25



; -------------------------------------------------------------------------------------------------
; bomb mob
; mob has three states: burning_fuse, then on to final_countdown after a long time, then finally to
; exploding after a short time. in exploding state the bomb causes damage to players and enemies.
; if the bomb is hit by any damage (eg: another exploding bomb) then it jumps to final_countdown
; state
; -------------------------------------------------------------------------------------------------

; -------------------------------------------------------------------------------------------------
init
			; mob will be initialised into state 0, with a state timer
			ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE_TIMER),BURNING_FUSE_TIME
			; fall through to update..

			; figure out the state and update as appropriate
update		ld		a,(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE)
			dec		a
			jr		z,.stateFinalCountdown
			dec		a
			jr		z,.stateExploding

			; STATE_BURNING_FUSE
.stateBurningFuse
			dec		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE_TIMER)	; reached end of state timer?
			jr		z,startFinalCountdown									; yes, enter STATE_FINAL_COUNTDOWN
			ld		a,(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE_TIMER)	; no, get current timer
			rrca															; use it to select current gfx frame
			rrca
			rrca
			rrca
			and		%00100000
			ld		d,0
			ld		e,a								; de = frame offset
			ld		hl,gfx							; hl => gfx base
			add		hl,de							; hl => gfx frame
			ld		(ix+mobs.MOB_DATA.GFX_DATA+1),h	; set gfx to mob data
			ld		(ix+mobs.MOB_DATA.GFX_DATA+0),l
			ret

			; STATE_FINAL_COUNTDOWN
.stateFinalCountdown
			dec		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE_TIMER)	; reached end of state timer?
			ret		nz
			jp		startExplode											; yes, enter STATE_EXPLODING

			; STATE_EXPLODING
.stateExploding
			dec		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE_TIMER)	; reached end of state timer?
			ret		nz
			jp		mobDead.init											; yes, kill this mob


			; collide function
collide
			ld		a,(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE)
			or		a ;	STATE_BURNING_FUSE			; if we're in the burning fuse state..
			ret		nz
			jp		startFinalCountdown				; ..then skip to final countdown


			; set bomb into STATE_FINAL_COUNTDOWN state
startFinalCountdown
			ld		hl,gfx							; gfx
			ld		(ix+mobs.MOB_DATA.GFX_DATA+1),h
			ld		(ix+mobs.MOB_DATA.GFX_DATA+0),l	; set gfx to mob data
			ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE),STATE_FINAL_COUNTDOWN		; set state
			ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE_TIMER),FINAL_COUNTDOWN_TIME	; set state timer
			ret


			; set bomb into EXPLODING_TIME state
startExplode
			ld		hl,gfx+32*2						; gfx
			ld		(ix+mobs.MOB_DATA.GFX_DATA+1),h
			ld		(ix+mobs.MOB_DATA.GFX_DATA+0),l	; set gfx to mob data
			ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE),STATE_EXPLODING		; set state
			ld		(ix+mobs.MOB_DATA.PRIVATE_DATA+MOB_BOMB.STATE_TIMER),EXPLODING_TIME	; set state timer
			ld		(ix+mobs.MOB_DATA.COLLISION_CAST),mobs.CMASK_PLAYER_ATTACK|mobs.CMASK_ENEMY_ATTACK	; change collision casting to cause damage

			ld		a,sound.sound_bombExplode		; make some noise
			call	sound.playSound
			ret



; -------------------------------------------------------------------------------------------------
			; sprite graphics
			ALIGN	2
gfx			incbin	'build/bomb.bin'



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
