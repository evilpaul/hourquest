; -------------------------------------------------------------------------------------------------
; General utilities
; -------------------------------------------------------------------------------------------------
	MODULE utils
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables

	STRUCT MM_UTILS
RAND_SEED_1				dw		0
RAND_SEED_2				dw		0
	ENDS



; -------------------------------------------------------------------------------------------------
; initalise
;
; Initialise any util related tables, seeds, etc.. Call once at program initialisation
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	hl
; -------------------------------------------------------------------------------------------------

initialise
			ld		hl,123
			ld		(MM.UTILS.RAND_SEED_1),hl
			ld		hl,456
			ld		(MM.UTILS.RAND_SEED_2),hl
			ret



; -------------------------------------------------------------------------------------------------
; getScreenAddressInHl
;
; Convert X/Y pixel position into screen pixel position
; -------------------------------------------------------------------------------------------------
; In:
;	b = X position (0..255)
;	c = Y position (0..191)
; Out:
;	a = Pixel position (0-7)
;	hl => Screen pixel position
; Trashed:
;	af, hl
; -------------------------------------------------------------------------------------------------

getScreenAddressInHl
			ld		a,c								; calculate high byte y position
			and		%00000111
			ld		h,a
			ld		a,c
			rra
			rra
			rra
			and		%00011000
			or		h
			or		HIGH SCREEN_BASE_PIXELS			; add in base screen address
			ld		h,a								; high byte of address is now set

			ld		a,c								; calculate low byte of y position
			rla
			rla
			and		%11100000
			ld		l,a								; low byte of address is now set

			ld		a,b								; calculate x position
			rra										; shift out bottom 3 bits
			rra
			rra
			and		%00011111						; now we have x character cell (0..31)
			or		l								; merge with low byte of screen address
			ld		l,a								; final screen address is now set

			ld		a,b								; get y pos again
			and		%00000111						; bottom three bits are the pixel position within a cell

			ret



; -------------------------------------------------------------------------------------------------
; getNextScreenRowDownHl
;
; Move down the screen by one row
; -------------------------------------------------------------------------------------------------
; In:
;	hl => Screen position
; Out:
;	hl => One line below input screen position
; Trashed:
;	af, hl
; -------------------------------------------------------------------------------------------------

getNextScreenRowDownHl
			inc		h								; step down one pixel line
			ld		a,h								; if bottom three bits..
			and		7								; ..are not-zero then..
			ret		nz								; ..we're in the same char cell, and we're finished

			ld		a,l								; step down one entire char cell
			add		a,32
			ld		l,a
			ret		c								; if carry bit set we didn't cross a third

			ld		a,h								; we crossed a third, so we need to fix up the high byte
			sub		8
			ld		h,a

			ret



; -------------------------------------------------------------------------------------------------
; getScreenCellPixelAddressInHl
;
; Convert X/Y cell position into screen pixel position
; -------------------------------------------------------------------------------------------------
; In:
;	b = X position (0..31)
;	c = Y position (0..23)
; Out:
;	hl => Screen pixel position
; Trashed:
;	af, hl
; -------------------------------------------------------------------------------------------------

getScreenCellPixelAddressInHl
			ld		a,c
			and		%00000111						; take lowest three bits of y position
			rla										; * 2
			rla										; * 4
			rla										; * 8
			rla										; * 16
			rla										; * 32
			ld		l,a

			ld		a,c								; take top two bits of y position
			and		%00011000
			add		a,HIGH SCREEN_BASE_PIXELS		; add in screen base
			ld		h,a								; high byte of screen position now set

			ld		a,b								; calculate x position
			and		%00011111						; it's just the bottom five bits
			or		l								; combine with y part
			ld		l,a								; final screen address is now set

			ret



; -------------------------------------------------------------------------------------------------
; getScreenCellAttrAddressInHl
;
; Convert X/Y cell position into screen attribute position
; -------------------------------------------------------------------------------------------------
; In:
;	b = X position (0..31)
;	c = Y position (0..23)
; Out:
;	hl => Screen attribute position
; Trashed:
;	af, hl
; -------------------------------------------------------------------------------------------------

getScreenCellAttrAddressInHl
			ld		a,c								; take top two bits of y position
			and		%00011000
			rra										; / 2
			rra										; / 4
			rra										; / 8
			or		HIGH (SCREEN_BASE_ATTRS)		; add to screen base
			ld		h,a								; high byte of screen position now set

			ld		a,c								; get bottom three bits of y position
			and		%00000111
			rla										; * 2
			rla										; * 4
			rla										; * 8
			rla										; * 16
			rla										; * 32
			ld		l,a

			ld		a,b								; calculate x position
			and		%00011111						; it's just the bottom five bits
			or		l								; combine with y part
			ld		l,a								; final screen address is now set

			ret




; -------------------------------------------------------------------------------------------------
; fastDrawChar
;
; Quickly draw a single char to the screen.
; -------------------------------------------------------------------------------------------------
; In:
;	hl => destination screen address
;	de => source gfx data on 8 byte aligned address
; Out:
;	hl => bottom line of character cell
;	de => last byte of source gfx
; Trashed:
;	af
; -------------------------------------------------------------------------------------------------

fastDrawChar
		REPT	7
			ld		a,(de)
			ld		(hl),a
			inc		e
			inc		h
		ENDR
			ld		a,(de)
			ld		(hl),a
			ret



; -------------------------------------------------------------------------------------------------
; fastDrawChar
;
; Quickly draw a single char to the screen using xor.
; -------------------------------------------------------------------------------------------------
; In:
;	hl => destination screen address
;	de => source gfx data on 8 byte aligned address
; Out:
;	hl => bottom line of character cell
;	de => last byte of source gfx
; Trashed:
;	af
; -------------------------------------------------------------------------------------------------

fastXorChar
		REPT	7
			ld		a,(de)
			xor		(hl)
			ld		(hl),a
			inc		e
			inc		h
		ENDR
			ld		a,(de)
			xor		(hl)
			ld		(hl),a
			ret



; -------------------------------------------------------------------------------------------------
; getRand
;
; Returns a 16 bit random number in hl
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	hl = random number
; Trashed:
;	af, bc, hl
; -------------------------------------------------------------------------------------------------

getRand
			ld		hl,(MM.UTILS.RAND_SEED_1)
			ld		b,h
			ld		c,l
			add		hl,hl
			add		hl,hl
			inc		l
			add		hl,bc
			ld		(MM.UTILS.RAND_SEED_1),hl
			ld		hl,(MM.UTILS.RAND_SEED_2)
			add		hl,hl
			sbc		a,a
			and		%00101101
			xor		l
			ld		l,a
			ld		(MM.UTILS.RAND_SEED_2),hl
			add		hl,bc
			ret



; -------------------------------------------------------------------------------------------------

			; the scroll table is a lookup table of all 256 possible 8bit values in all 8 possible
			; shift positions. the shifted value would normally take up two bytes, but in this
			; table we combine them together and use the scrollTableExtractionMasks to extract
			; the first byte, and the inverse of scrollTableExtractionMasks to extract the second
			; byte. thus the table takes 256 * 8 = 2k

scrollTableExtractionMasks
			db		%11111111
			db		%01111111
			db		%00111111
			db		%00011111
			db		%00001111
			db		%00000111
			db		%00000011
			db		%00000001

			ALIGN	256
scrollTable	LUA ALLPASS
				for rshift = 0, 7 do
					for src = 0, 255 do
						lshift = 8 - rshift
						v1 = (src * 2 ^ lshift) % 256
						v2 = math.floor(src / 2 ^ rshift)
						v = v1 + v2
						_pl(" db "..v)
					end
				end
			ENDLUA



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
