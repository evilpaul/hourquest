; -------------------------------------------------------------------------------------------------
; Mobile object management code
; -------------------------------------------------------------------------------------------------
	MODULE mobs
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
	STRUCT MOB_DATA
SPRITE_MASKS			block	4

TYPE					byte

FUNC_UPDATE				word
FUNC_COLLIDE			word

COLLISION_CAST			byte
COLLISION_RECEIVE		byte

X_POS					byte
Y_POS					byte
GFX_DATA				word

OLD_X_POS				byte
OLD_Y_POS				byte
OLD_GFX_DATA			word

GFX_HEIGHT				byte
GFX_WIDTH				byte

PRIVATE_DATA			block	4

_SIZE
	ENDS

MAX_MOBS	equ			5



; -------------------------------------------------------------------------------------------------
; global variables and defines

	STRUCT MM_MOBS
ALL_MOB_DATA			block	MOB_DATA._SIZE * MAX_MOBS
	ENDS


; collision masks
CMASK_NONE				equ		0
CMASK_PLAYER			equ		1<<0
CMASK_PLAYER_ATTACK		equ		1<<2
CMASK_ENEMY				equ		1<<3
CMASK_ENEMY_ATTACK		equ		1<<4


VISUALISE_COLLISION_TESTS	equ		0



; -------------------------------------------------------------------------------------------------
; clearAll
;
; Clear all mobs
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	bc, de, hl
; -------------------------------------------------------------------------------------------------

clearAll
			ld		hl,MM.MOBS.ALL_MOB_DATA
			ld		de,MM.MOBS.ALL_MOB_DATA + 1
			ld		(hl),0
			ld		bc,(MOB_DATA._SIZE * MAX_MOBS) - 1
			ldir
			ret



; -------------------------------------------------------------------------------------------------
; spawn
;
; Attempt to spawn a new mob
; -------------------------------------------------------------------------------------------------
; In:
;	b = type
;	d = Initial Y pos
;	e = Initial X pos
; Out:
;	ix => Mob data
;	carry = Clear if mob spawned, otherwise set
; Trashed:
;	af, bc', de', hl', ix
; -------------------------------------------------------------------------------------------------

spawn
			exx										; save the inputs

			ld		ix,MM.MOBS.ALL_MOB_DATA			; point to all mob data
			ld		de,MOB_DATA._SIZE
			ld		b,MAX_MOBS						; we'll check all available slots

.allMobs	ld		a,(ix+MOB_DATA.TYPE)			; is there an active mob in this slot?
			or		a
			jr		z,.emptySlot					; no..
			add		ix,de							; yes, point to next mob slot
			djnz	.allMobs						; check all slots..

			scf										; failed to spawn, set carry flag to indicate failure
			exx										; restore inputs
			ret										; and we're done..


			; we've found a spare spot to spawn a new mob
.emptySlot

			; clear the mobs data (todo: don't need to clear all of this..)
			push	ix
			pop		hl
			ld		d,h
			ld		e,l
			inc		de
			ld		bc,MOB_DATA._SIZE-1
			ld		(hl),b							; set first byte to 0
			ldir

			exx										; restore inputs

			ld		(ix+MOB_DATA.Y_POS),d			; store initial position
			ld		(ix+MOB_DATA.X_POS),e
			ld		(ix+MOB_DATA.TYPE),b			; store type

			ld		a,b								; look up mob type data (type * 10)
			ASSERT(TYPE_DATA_SIZE == 10)			; need to change the next few lines if mob size changes
			add		a,a
			add		a,a
			add		a,a
			add		a,b
			add		a,b
			ld		d,0
			ld		e,a
			ld		hl,typeData-TYPE_DATA_SIZE
			add		hl,de							; hl > mob type data
			ld		a,(hl)
			inc		hl
			ld		(ix+MOB_DATA.GFX_WIDTH),a		; store gfx size
			ld		a,(hl)
			inc		hl
			ld		(ix+MOB_DATA.GFX_HEIGHT),a
			ld		a,(hl)
			inc		hl
			ld		(ix+MOB_DATA.FUNC_UPDATE+0),a	; store update function
			ld		a,(hl)
			inc		hl
			ld		(ix+MOB_DATA.FUNC_UPDATE+1),a
			ld		a,(hl)
			inc		hl
			ld		(ix+MOB_DATA.FUNC_COLLIDE+0),a	; store collide function
			ld		a,(hl)
			inc		hl
			ld		(ix+MOB_DATA.FUNC_COLLIDE+1),a
			ld		a,(hl)
			inc		hl
			ld		(ix+MOB_DATA.COLLISION_CAST),a	; store collide masks
			ld		a,(hl)
			inc		hl
			ld		(ix+MOB_DATA.COLLISION_RECEIVE),a

			ld		a,(hl)							; get address of the init function
			inc		hl
			ld		h,(hl)
			ld		l,a
			ld		de,.initDone
			push	de								; push address to return to after calling init function
			push	hl								; push address of init function
			ret										; call init function
.initDone

			xor		a								; clear carry flag to indicate success
			ret										; and we're done..



; -------------------------------------------------------------------------------------------------
; renderAll
;
; Render all mobs
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	Everything..
; -------------------------------------------------------------------------------------------------

renderAll
			ld		ix,MM.MOBS.ALL_MOB_DATA			; point to all mob data
			ld		b,MAX_MOBS						; we'll render all available slots
.allMobs	push	bc								; save counter

			ld		a,(ix+MOB_DATA.TYPE)			; is there an active mob in this slot?
			or		a
			jp		z,.notAlive						; no active mob here, skip it..

			ld		b,(ix+MOB_DATA.X_POS)			; fetch current gfx data in bc, de
			ld		c,(ix+MOB_DATA.Y_POS)
			ld		d,(ix+MOB_DATA.GFX_DATA+1)
			ld		e,(ix+MOB_DATA.GFX_DATA+0)
			ld		hl,0							; compute a hash of the data
			add		hl,bc
			add		hl,de

			push	de
			exx
			ld		b,(ix+MOB_DATA.OLD_X_POS)		; fetch last frame's gfx data in bc, de
			ld		c,(ix+MOB_DATA.OLD_Y_POS)
			ld		d,(ix+MOB_DATA.OLD_GFX_DATA+1)
			ld		e,(ix+MOB_DATA.OLD_GFX_DATA+0)
			ld		hl,0							; compute a hash of the data
			add		hl,bc
			add		hl,de
			push	hl
			exx
			pop		de								; old data hash is in de, new data hash is in hl
			xor		a
			sbc		hl,de							; compare hashes
			pop		de
			jr		z,.noChange						; no difference = no need to render this frame

			push	bc								; save new gfx data
			push	de
			exx
			ld		a,d								; was last frame's gfx data 0? ie: is this the first frame
			or		e
			jr		z,.firstFrameSkipRemove			; yes, skip the erase step..
			ld		h,(ix+MOB_DATA.GFX_HEIGHT)		; erase mob using last frame's gfx data
			push	ix
			call	sprites.xor16WithHideMasks
			pop		ix
.firstFrameSkipRemove
			exx
			pop		de								; restore new gfx data
			pop		bc

			ld		(ix+MOB_DATA.OLD_X_POS),b		; save new data over old
			ld		(ix+MOB_DATA.OLD_Y_POS),c
			ld		(ix+MOB_DATA.OLD_GFX_DATA+1),d
			ld		(ix+MOB_DATA.OLD_GFX_DATA+0),e

			ld		a,d								; don't draw if gfx address is 0
			or		e
			jr		z,.noDraw

			ld		h,(ix+MOB_DATA.GFX_HEIGHT)		; render mob using this frame's gfx data
			push	bc
			push	de
			push	hl
			push	ix
			call	room.buildHideMasks
			pop		ix
			pop		hl
			pop		de
			pop		bc
			push	ix
			call	sprites.xor16WithHideMasks
			pop		ix
.noDraw
.notAlive
.noChange

			ld		bc,MOB_DATA._SIZE				; point to next mob slot
			add		ix,bc
			pop		bc								; restore counter
			dec		b
			jp		nz,.allMobs						; check all slots..

			ret



; -------------------------------------------------------------------------------------------------
; updateAll
;
; Update all mobs
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	Everything..
; -------------------------------------------------------------------------------------------------

updateAll
			ld		ix,MM.MOBS.ALL_MOB_DATA			; point to all mob data
			ld		b,MAX_MOBS						; we'll update all available slots
.allMobs	push	bc								; save counter

			ld		a,(ix+MOB_DATA.TYPE)			; is there an active mob in this slot?
			or		a
			jr		z,.notAlive						; no...
			ld		de,.returnFromUpdate			; fake a call (hl) instruction..
			ld		h,(ix+MOB_DATA.FUNC_UPDATE+1)
			ld		l,(ix+MOB_DATA.FUNC_UPDATE+0)
			push	de
			jp		(hl)							; call this mobs update function
.returnFromUpdate
.notAlive

			ld		bc,MOB_DATA._SIZE				; point to next mob slot
			add		ix,bc
			pop		bc								; restore counter
			djnz	.allMobs						; check all slots..

			ret



; -------------------------------------------------------------------------------------------------
; collideAll
;
; Collide all mobs against each other, calling collide functions when they hit. Collision between
; all mobs is performed, but we save time by only checking each possible collision pair once and
; calling collide on both mobs. ie: with four mobs, naive checking would perform 4x4=16 tests but
; here we only do (4-1)!=6
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	Everything..
; -------------------------------------------------------------------------------------------------

collideAll
			IF	VISUALISE_COLLISION_TESTS != 0
			ld	hl,SCREEN_BASE_ATTRS+32*4+1
			exx
			ENDIF

			ld		ix,MM.MOBS.ALL_MOB_DATA			; all mob data (for outer loop) in ix
			ld		c,MAX_MOBS						; and this many mobs
.outerLoop
			ld		b,c								; inner loop counter is outer loop -1
			dec		b
			ret		z								; if nothing in the inner loop then we're done

			IF	VISUALISE_COLLISION_TESTS != 0
			exx
			push	hl
			exx
			ENDIF

			push	ix								; copy current outer loop value from ix to iy
			pop		iy

.innerLoop
			ld		de,MOB_DATA._SIZE				; step to next mob in iy
			add		iy,de

			IF	VISUALISE_COLLISION_TESTS != 0
			exx
			ld		(hl),COLOUR_BG_BLACK|COLOUR_FG_BLACK
			exx
			ENDIF

			xor		a
			cp		(ix+MOB_DATA.TYPE)				; if neither mob is alive, skip this pair
			jp		z,.noCollidePair				; (could do this more efficiently, but..
			or		(iy+MOB_DATA.TYPE)				; ..then the visualisation would get..
			jp		z,.noCollidePair				;..harder)

			IF	VISUALISE_COLLISION_TESTS != 0
			exx
			ld		(hl),COLOUR_BG_BLUE|COLOUR_FG_BLUE
			exx
			ENDIF

			; check cast and receive masks to see if there is any chance that
			; we might be interested in a collision between these two mobs
			ld		a,(ix+MOB_DATA.COLLISION_CAST)
			or		(iy+MOB_DATA.COLLISION_CAST)
			ld		d,a								; d is a combination of both cast masks
			ld		a,(ix+MOB_DATA.COLLISION_RECEIVE)
			or		(iy+MOB_DATA.COLLISION_RECEIVE)	; a is a combination of bast receives
			and		d								; anyone interested?
			jp		z,.dontCollidePair				; no..

			; check x range
			ld		a,(ix+MOB_DATA.GFX_WIDTH)
			add		a,(iy+MOB_DATA.GFX_WIDTH)
			ld		d,a
			ld		a,(ix+MOB_DATA.X_POS)
			sub		(iy+MOB_DATA.X_POS)
			add		a,(ix+MOB_DATA.GFX_WIDTH)
			cp		d
			jr		nc,.noCollidePair				; no overlap = no collision

			; check y range
			ld		a,(ix+MOB_DATA.GFX_HEIGHT)
			add		a,(iy+MOB_DATA.GFX_HEIGHT)
			ld		d,a
			ld		a,(ix+MOB_DATA.Y_POS)
			sub		(iy+MOB_DATA.Y_POS)
			add		a,(ix+MOB_DATA.GFX_HEIGHT)
			cp		d
			jr		nc,.noCollidePair				; no overlap = no collision

			; there's a collision!
			push	bc								; save loop counters
			IF	VISUALISE_COLLISION_TESTS != 0
			exx
			push	bc
			push	de
			push	hl
			exx
			ENDIF

			; check if mob1 receives what mob2 is casting
			ld		a,(ix+MOB_DATA.COLLISION_RECEIVE)
			and		(iy+MOB_DATA.COLLISION_CAST)
			jr		z,.collideSkip1					; no...

			; yes. call mob1's collide function with ix=mob1, iy=mob2
			ld		h,(ix+MOB_DATA.FUNC_COLLIDE+1)
			ld		l,(ix+MOB_DATA.FUNC_COLLIDE+0)
			ld		de,.collideDone1
			push	de
			jp		(hl)
.collideDone1
			IF	VISUALISE_COLLISION_TESTS != 0
			exx
			pop		hl
			ld		a,(hl)
			or		COLOUR_BG_GREEN|COLOUR_FG_GREEN
			ld		(hl),a
			push	hl
			exx
			ENDIF
.collideSkip1

			; check if mob2 receives what mob1 is casting
			ld		a,(iy+MOB_DATA.COLLISION_RECEIVE)
			and		(ix+MOB_DATA.COLLISION_CAST)
			jr		z,.collideSkip2					; no...

			; yes. call mob2's collide function with ix=mob2, iy=mob1
			push	ix								; swap ix/iy
			push	iy
			pop		ix
			pop		iy
			ld		h,(ix+MOB_DATA.FUNC_COLLIDE+1)
			ld		l,(ix+MOB_DATA.FUNC_COLLIDE+0)
			ld		de,.collideDone2
			push	de
			jp		(hl)
.collideDone2
			push	ix								; swap ix/iy back
			push	iy
			pop		ix
			pop		iy
			IF	VISUALISE_COLLISION_TESTS != 0
			exx
			pop		hl
			ld		a,(hl)
			or		COLOUR_BG_RED|COLOUR_FG_RED
			ld		(hl),a
			push	hl
			exx
			ENDIF
.collideSkip2
			IF	VISUALISE_COLLISION_TESTS != 0
			exx
			pop		hl
			pop		de
			pop		bc
			exx
			ENDIF

			pop		bc								; restore loop counters

.noCollidePair
.dontCollidePair
			IF	VISUALISE_COLLISION_TESTS != 0
			exx
			inc		l
			exx
			ENDIF

			dec		b								; do all mobs in inner loop
			jp		nz,.innerLoop

			ld		de,MOB_DATA._SIZE				; step to next mob in ix (outer loop)
			add		ix,de

			IF	VISUALISE_COLLISION_TESTS != 0
			exx
			pop		hl
			ld		de,33
			add		hl,de
			exx
			ENDIF

			dec		c								; do all mobs in outer loop
			jp		.outerLoop



; -------------------------------------------------------------------------------------------------
; move
;
; Move a mob, respecting the room collision map
; -------------------------------------------------------------------------------------------------
; In:
;	d = dy (between -7 and +7)
;	e = dx (between -7 and +7)
;	ix => Mob data
; Out:
;	None
; Trashed:
;	a, bc, de, hl
; -------------------------------------------------------------------------------------------------

move
			ld		b,(ix+mobs.MOB_DATA.X_POS)		; get current position
			ld		c,(ix+mobs.MOB_DATA.Y_POS)

			; move and collide left/right
.moveX		ld		a,e								; get dx
			or		a								; any x movement at all?
			jr		z,.movedX						; no..
			add		a,b								; a = x + dx
			ld		b,a								; b is now requested x position
			bit		7,e								; was this a -ve or +ve move?
			jr		z,.right
.left		push    bc								; moving left
			push	de
			ld		d,(ix+mobs.MOB_DATA.GFX_HEIGHT)	; check left vertical edge
			call	room.collideVertical
			pop		de
			pop		bc
			jr		z,.movedX						; no collision..
			ld		a,b								; get x
			and		%11111000						; snap to collision cell
			add		a,8								; push into cell to the right
			ld		b,a								; collision resolved
			jr		.movedX
.right		push    bc								; moving right
			push	de
			ld		a,b
			add		a,(ix+mobs.MOB_DATA.GFX_WIDTH)
			dec		a
			ld		b,a
			ld		d,(ix+mobs.MOB_DATA.GFX_HEIGHT)
			call	room.collideVertical			; check right vertical edge
			pop		de
			pop		bc
			jr		z,.movedX						; no collision..
			ld		a,b								; get x
			add		a,(ix+mobs.MOB_DATA.GFX_WIDTH)	; find the right edge
			dec		a
			and		%11111000						; snap to cell
			sub		(ix+mobs.MOB_DATA.GFX_WIDTH)	; step back to get left edge
			ld		b,a								; collision resolved
.movedX

			; move and collide up/down
.moveY		ld		a,d								; get dy
			or		a								; any y movement at all?
			jr		z,.movedY						; no..
			add		a,c								; a = y + dy
			ld		c,a								; c is now request y position
			bit		7,d								; was this a -ve or +ve move?
			jr		z,.down
.up			push    bc								; moving up
			;push	de
			ld		d,(ix+mobs.MOB_DATA.GFX_WIDTH)	; check top horizontal edge
			call	room.collideHorizontal
			;pop		de
			pop		bc
			jr		z,.movedY						; no collision..
			ld		a,c								; get y
			and		%11111000						; snap to collision cell
			add		a,8								; push into cell above
			ld		c,a								; collision resolved
			jr		.movedY
.down		push    bc								; moving down
			;push	de
			ld		a,c
			add		a,(ix+mobs.MOB_DATA.GFX_HEIGHT)
			dec		a
			ld		c,a
			ld		d,(ix+mobs.MOB_DATA.GFX_WIDTH)
			call	room.collideHorizontal			; check bottom horizontal edge
			;pop		de
			pop		bc
			jr		z,.movedY						; no collision..
			ld		a,c								; get y
			add		a,(ix+mobs.MOB_DATA.GFX_HEIGHT)	; find the bottom edge
			dec		a
			and		%11111000						; snap to cell
			sub		(ix+mobs.MOB_DATA.GFX_HEIGHT)	; step back to get top edge
			ld		c,a								; collision resolved
.movedY

			ld		(ix+mobs.MOB_DATA.X_POS),b		; update with new position
			ld		(ix+mobs.MOB_DATA.Y_POS),c
			ret



; -------------------------------------------------------------------------------------------------
; checkTriggers
;
; Check to see if a mob is inside a trigger. Checks the middle of the mob only
; -------------------------------------------------------------------------------------------------
; In:
;	ix => Mob data
; Out:
;	a = Trigger id
;	Zero flag set if collided, otherwise unset
; Trashed:
;	a, bc, de, hl
; -------------------------------------------------------------------------------------------------

checkTriggers
			ld		a,(ix+mobs.MOB_DATA.GFX_WIDTH)	; get middle position of mob
			srl		a
			add		a,(ix+mobs.MOB_DATA.X_POS)
			ld		b,a
			ld		a,(ix+mobs.MOB_DATA.GFX_HEIGHT)
			srl		a
			add		a,(ix+mobs.MOB_DATA.Y_POS)
			ld		c,a

			call	room.collidePoint				; get collision tile for this point

			and		%11110000						; mask out to get trigger id
			ret		z								; no trigger id..

			rrca									; shift trigger id in bottom nibble
			rrca
			rrca
			rrca
			and		%00001111						; mask and set z flag
			ret



; -------------------------------------------------------------------------------------------------

TYPE_DATA MACRO width, height, initFunction, updateFunction, collideFunction, collisionMaskCast, collisionMaskReceive
			db		width, height
			dw		updateFunction
			dw		collideFunction
			db		collisionMaskCast, collisionMaskReceive
			dw		initFunction
ENDM
TYPE_DATA_SIZE		equ		10

typeData
MOB_TYPE_INACTIVE	equ		0

MOB_TYPE_PLAYER		equ		1
			TYPE_DATA		15, 18, mobPlayer.init, mobPlayer.update, mobPlayer.collide, CMASK_PLAYER, CMASK_ENEMY|CMASK_ENEMY_ATTACK

MOB_TYPE_BOMB		equ		2
			TYPE_DATA		15, 15, mobBomb.init, mobBomb.update, mobBomb.collide, CMASK_NONE, CMASK_ENEMY_ATTACK|CMASK_PLAYER_ATTACK

MOB_TYPE_WIZARD		equ		3
			TYPE_DATA		15, 20, mobWizard.init, mobWizard.update, mobWizard.collide, CMASK_ENEMY, CMASK_PLAYER_ATTACK

MOB_TYPE_FRANK		equ		4
			TYPE_DATA		15, 25, mobFrank.init, mobFrank.update, mobFrank.collide, CMASK_NONE, CMASK_NONE

MOB_TYPE_WATERFALL	equ		5
			TYPE_DATA		10, 10, mobWaterfall.init, mobWaterfall.update, mobWaterfall.collide, CMASK_NONE, CMASK_NONE


; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
