; -------------------------------------------------------------------------------------------------
; Sound routines
; -------------------------------------------------------------------------------------------------
	MODULE sound
; -------------------------------------------------------------------------------------------------



; -------------------------------------------------------------------------------------------------
; global variables

	STRUCT MM_SOUND
CURRENT_SOUND_ID		byte						; currently playing sound id, or -1 for none
NEW_SOUND_ID			byte						; request new sound to play, or -1 for none
SOUND_PTR				word						; points to currently playing sound entry in sound table
LEN						byte						; number of frames played of the current sound
FREQ_COUNTER			word						; running total of frequency counter for current sound
PITCH					word						; pitch of current sound
PITCH_MOD				word						; added to pitch after each frame has been played
	ENDS


	STRUCT SOUND
LENGTH					byte						; number of frames to play the sound, 0 means end-of-sound
PITCH					byte						; pitch, 8 bit unsigned (higher numbers mean higher pitch, 0 means keep last pitch)
PITCH_MOD				byte						; pitch mod, 8 bit signed
RANDOM_MASK				byte						; and'd with random number and added to pitch each frame
_SIZE
	ENDS



; -------------------------------------------------------------------------------------------------
; initialise
;
; Set up the sound system.
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af
; -------------------------------------------------------------------------------------------------

initialise
			ld		a,-1							; set current and new sound to none
			ld		(MM.SOUND.CURRENT_SOUND_ID),a
			ld		(MM.SOUND.NEW_SOUND_ID),a
			ret



; -------------------------------------------------------------------------------------------------
; playSound
;
; Cue up a sound to be played. The sound will be played if it is higher priority (id is less than)
; the currently playing sound, or if no sound is currently playing.
; -------------------------------------------------------------------------------------------------
; In:
;	a = Id of sound to play
; Out:
;	None
; Trashed:
;	af, af', b
; -------------------------------------------------------------------------------------------------

playSound	ex		af,af'							; save the id
			ld		a,(MM.SOUND.CURRENT_SOUND_ID)	; is there a sound currently playing?
			cp		-1
			jr		nz,.check						; yes, we need to check priority
.noCheck	ex		af,af'							; no, restore the requested sound id..
			ld		(MM.SOUND.NEW_SOUND_ID),a		; ..and set the new sound id
			ret

.check		ld		b,a								; remember currently playing sound id
			ex		af,af'							; restore required sound id
			cp		b
			ret		nc								; if new sound is lower priority then don't play it
.do			ld		(MM.SOUND.NEW_SOUND_ID),a		; set the new sound id
			ret



; -------------------------------------------------------------------------------------------------
; isrUpdate
;
; Output sound to the speaker. Also handles starting new sounds that were requested with playSound
; -------------------------------------------------------------------------------------------------
; In:
;	None
; Out:
;	None
; Trashed:
;	af, bc, de, hl, ix
; -------------------------------------------------------------------------------------------------

isrUpdate
			; deal with new sounds being triggered
			ld		a,(MM.SOUND.NEW_SOUND_ID)		; is there a new sound being triggered?
			cp		-1
			jr		z,.noNewSound					; no..
			ld		(MM.SOUND.CURRENT_SOUND_ID),a	; yes, remember new sound id
			ld		de,0							; reset sound freq
			ld		(MM.SOUND.FREQ_COUNTER),de
			add		a,a								; look up data in sound table
			add		a,a
			ld		e,a
	IFNDEF SOUND_DEBUGGING
			ld		ix,soundTab
	ELSE
			ld		ix,MM.SCRATCH + soundDebug.SOUND_DEBUG_DATA.SOUND_TAB
	ENDIF
			add		ix,de
			call	.fetchData						; read sound data
			ld		a,-1							; reset 'new sound' flag
			ld		(MM.SOUND.NEW_SOUND_ID),a
.noNewSound

			; if no sound is playing then we can stop right now
			ld		a,(MM.SOUND.CURRENT_SOUND_ID)
			cp		-1
			ret		z

			ld		ix,(MM.SOUND.SOUND_PTR)			; point to sound data
.read		ld		a,(ix + SOUND.LENGTH)			; 0 in length means this is the last sound in a chain
			or		a
			jr		nz,.notDone
			ld		a,-1							; so stop playing sounds and exit
			ld		(MM.SOUND.CURRENT_SOUND_ID),a
			ret
.notDone	ld		a,(MM.SOUND.LEN)				; have we played enough frames of this sound?
			cp		(ix + SOUND.LENGTH)
			jr		c,.noAdv						; no..
			ld		de,SOUND._SIZE					; yes, point to next sound in table
			add		ix,de
			call	.fetchData						; fetch new sound data
			jp		.read							; and check again..
.noAdv		ld		hl,MM.SOUND.LEN					; increase count of frames played for this sound
			inc		(hl)

			; if the pitch is 0 then we may as well skip playing the sound
			ld		de,(MM.SOUND.PITCH)
			ld		a,d
			or		e
			ret		z

			; add in some randomness to the base pitch
			call	utils.getRand					; get random
			ld		h,0								; clear the high byte
			ld		a,l								; mask the low byte 
			and		(ix + SOUND.RANDOM_MASK)
			ld		l,a
			add		hl,de							; add random to base pitch
			ex		de,hl							; pitch now in de

			ld		hl,(MM.SOUND.FREQ_COUNTER)		; restore current freq counter to hl

			; the sound output loop
			ld		bc,254							; repeat 256 times, c points to the speaker port
.loop		add 	hl,de 							; add freq to freq counter		(11)
			ld		a,h								; get high byte of freq counter	(4)
			and		%00010000						; mask to get black border		(7)
			out 	(c),a							; set speaker bit				(12)
			dec 	b 								; loop							(4)
			jp 		nz,.loop 						; 								(10)

			ld		(MM.SOUND.FREQ_COUNTER),hl		; save new freq counter
			ld		de,(MM.SOUND.PITCH)				; get current base pitch again
			ld		hl,(MM.SOUND.PITCH_MOD)			; add the pitch mod
			add		hl,de
			ld		(MM.SOUND.PITCH),hl				; save new base pitch

			; and we're done..
			ret


			; fetch sound data from ix
.fetchData	ld		(MM.SOUND.SOUND_PTR),ix			; save address
			xor		a
			ld		(MM.SOUND.LEN),a				; reset length counter to 0
			ld		a,(ix + SOUND.PITCH)			; get new pitch
			or		a								; zero pitch means keep current pitch
			jr		z,.keepPitch
			ld		e,A								; pitch is 8 bit extended to 16 bit
			ld		d,0
			ld		(MM.SOUND.PITCH),de				; save pitch
.keepPitch	ld		a,(ix + SOUND.PITCH_MOD)		; pitch mod is 8 bit extended to 16 bit
			ld		e,a								; store low byte
			add		a,a								; push sign into carry
			sbc		a								; turn it into 0 or -1
			ld		d,a								; sign extended high byte
			ld		(MM.SOUND.PITCH_MOD),de			; save pitch mode
			ret



; -------------------------------------------------------------------------------------------------

soundTab

sound_menuOpen				equ		0
		db		0x04,0x60,0x00,0x00
		db		0x04,0x90,0x00,0x00
		db		0x04,0xc0,0x00,0x00
		db		0x00,0x00,0x00,0x00

sound_menuClose				equ		4
		db		0x04,0xc0,0x00,0x00
		db		0x04,0x90,0x00,0x00
		db		0x04,0x60,0x00,0x00
		db		0x00,0x00,0x00,0x00

sound_menuSetItem			equ		8
		db		0x03,0xa0,0x00,0x00
		db		0x03,0x01,0x00,0x00
		db		0x03,0xb0,0x00,0x00
		db		0x00,0x00,0x00,0x00

sound_menuCantSetItem		equ		12
		db		0x03,0xa0,0x00,0x00
		db		0x03,0x01,0x00,0x00
		db		0x03,0x90,0x00,0x00
		db		0x00,0x00,0x00,0x00

sound_menuMove				equ		16
		db		0x05,0x40,0x10,0x00
		db		0x00,0x00,0x00,0x00

sound_dialogueSpeak			equ		18
		db		0x1f,0x50,0x00,0x30
		db		0x00,0x00,0x00,0x00

sound_dialogueNext			equ		20
		db		0x03,0xf0,0x30,0x00
		db		0x00,0x00,0x00,0x00

sound_playerHurt			equ		22
		db		0x03,0xa0,0x00,0x00
		db		0x05,0x00,0xe0,0x00
		db		0x00,0x00,0x00,0x00

sound_teleport				equ		25
		db		0x02,0xb0,0x00,0x00
		db		0x05,0x60,0xf0,0x00
		db		0x00,0x00,0x00,0x00

sound_bombDrop				equ		28
		db		0x04,0x60,0xf0,0x00
		db		0x00,0x00,0x00,0x00

sound_enemyDeath			equ		30
		db		0x05,0xc0,0xf4,0x00
		db		0x05,0x01,0x00,0x00
		db		0x05,0x90,0xf4,0x00
		db		0x05,0x01,0x00,0x00
		db		0x05,0x60,0xf4,0x00
		db		0x00,0x00,0x00,0x00

sound_bombExplode			equ		36
		db		0x1f,0x10,0x00,0x1f
		db		0x00,0x00,0x00,0x00



; -------------------------------------------------------------------------------------------------
	ENDMODULE
; -------------------------------------------------------------------------------------------------
