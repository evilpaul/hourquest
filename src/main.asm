; -------------------------------------------------------------------------------------------------
; Build configuration

;	DEFINE	SOUND_DEBUGGING



; -------------------------------------------------------------------------------------------------
			include	'defines.asm'



; -------------------------------------------------------------------------------------------------
			org		BASE_ADDR

start		di										; disabled interrupts immediately
			ld		sp,MM.STACK						; set the stack to RAM

			ld		hl,16384						; clear all upper memory to 0
			ld		de,16385
			ld		(hl),l
			ld		bc,16384-1
			ldir

			call	utils.initialise
			call	sound.initialise

			; initialise interrupts
	IF INTERRUPT_MODE == 1
			im		1								; set interrupt mode 1, where 0x38 will be called every frame
	ENDIF
	IF INTERRUPT_MODE == 2
			im		2								; set interrupt mode 2, where the isr is looked up from a table
VBL_TABLE	equ		49152							; place the isr table here
			ld		hl,isr							; fill out the isr table by pointing it at our isr routine
			ld		(VBL_TABLE-1),hl
			ld		hl,VBL_TABLE-1
			ld		de,VBL_TABLE+2-1
			ld		bc,257
			ldir
			ld		a,HIGH (VBL_TABLE)				; point i to the interrupt table
			ld		i,a
	ENDIF

			ei										; re-enable interrupts now that everything is set up

	IFDEF SOUND_DEBUGGING
			jp		soundDebug.debugMenu
	ELSE
			jp		mainProg
	ENDIF



; -------------------------------------------------------------------------------------------------
			; the interrupt service routine is placed at $38 for interrupt mode 1
			ALIGN_AT	BASE_ADDR + 0x38
isr			push		af
			push		bc
			push		de
			push		hl
			push		ix

			call	sound.isrUpdate

			pop			ix
			pop			hl
			pop			de
			pop			bc
			pop			af
			ei
			reti



; -------------------------------------------------------------------------------------------------
mainProg
			; initial player room and position
			ld		a,1
			ld		(MM.ROOM.CHANGE_ROOM_FLAG),a
			ld		a,1
			ld		(MM.ROOM.ROOM_ID),a
			ld		de,0x8060
			ld		(MM.PLAYER.X_POS),de
			ld		a,10
			ld		(MM.PLAYER.MAX_HEALTH),a
			ld		(MM.PLAYER.CURRENT_HEALTH),a

			; gift some items..
			ld		ix,MM.ITEMS
			ld		(ix + items.MM_ITEMS.OWNED_FLAGS+0),1
			ld		(ix + items.MM_ITEMS.OWNED_FLAGS+1),1
			ld		(ix + items.MM_ITEMS.OWNED_FLAGS+2),1
			ld		(ix + items.MM_ITEMS.OWNED_FLAGS+4),1
			ld		(ix + items.MM_ITEMS.OWNED_FLAGS+7),1
			ld		(ix + items.MM_ITEMS.OWNED_FLAGS+11),1
			ld		(ix + items.MM_ITEMS.HELD_ITEM_A),0
			ld		(ix + items.MM_ITEMS.HELD_ITEM_B),1
			ld		(ix + items.MM_ITEMS.ACTIVATED_ITEM_A),-1
			ld		(ix + items.MM_ITEMS.ACTIVATED_ITEM_B),-1

			; the main loop..
.loop		halt									; snc to start of frame

			ld		a,6								; set border to yellow
			out		(BORDER_PORT),A
			call	room.update

			ld		a,7								; set border to white
			out		(BORDER_PORT),a
			call	input.update

			ld		a,1								; set border to blue
			out		(BORDER_PORT),a
			call	mobs.updateAll

			ld		a,2								; set border to red
			out		(BORDER_PORT),a
			call	mobs.renderAll

			ld		a,4								; set border to green
			out		(BORDER_PORT),a
			call	mobs.collideAll

			ld		a,5								; set border to cyan
			out		(BORDER_PORT),a
			call	inventory.update

			ld		a,3								; set border to magenta
			out		(BORDER_PORT),a
			call	items.update

			ld		a,0								; set border back to black
			out		(BORDER_PORT),a
			jp		.loop							; back round the main loop once more..



; -------------------------------------------------------------------------------------------------
			include	'dialogue.asm'
			include	'input.asm'
			include	'inventory.asm'
			include	'items.asm'
			include	'mobs.asm'
			include	'mobBomb.asm'
			include	'mobDead.asm'
			include	'mobPlayer.asm'
			include	'mobFrank.asm'
			include	'mobWaterfall.asm'
			include	'mobWizard.asm'
			include	'room.asm'
			include	'screenBlock.asm'
			include	'script.asm'
			include	'sound.asm'
	IFDEF SOUND_DEBUGGING
			include	'soundDebug.asm'
	ENDIF
			include	'sprites.asm'
			include	'text.asm'
			include	'utils.asm'

        	MODULE  zx0
			include	'dzx0_standard.asm'
			ENDMODULE

			include	'memoryMap.asm'



; -------------------------------------------------------------------------------------------------
END_ADDR		equ		BASE_ADDR + 16384
ROM_REMAINING	equ		(END_ADDR) - $
RAM_REMAINING	equ		(16384 - SCREEN_SIZE) - MEMORY_MAP._SIZE
			DISPLAY	'                                        ', /D, RAM_REMAINING, " bytes (", /D, (RAM_REMAINING / 1024), "k) RAM remaining"
			DISPLAY	'                                        ', /D, ROM_REMAINING, " bytes (", /D, (ROM_REMAINING / 1024), "k) ROM remaining"
			ALIGN_AT	END_ADDR						; pad the binary out to 16k
