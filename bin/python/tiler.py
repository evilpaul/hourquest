import argparse
import collections
import json
import logging
import sys
from PIL import Image

import zx0
import scriptgen



palette = [
	(0, 0, 0),
	(0, 0, 192),
	(192, 0, 0),
	(192, 0, 192),
	(0, 192, 0),
	(0, 192, 192),
	(192, 192, 0),
	(192, 192, 192),
	(0, 0, 0),
	(0, 0, 255),
	(255, 0, 0),
	(255, 0, 255),
	(0, 255, 0),
	(0, 255, 255),
	(255, 255, 0),
	(255, 255, 255),
]

def match_palette_colour(r, g, b):
	closestIndex = -1
	closestDistance = 9999
	for index in range(len(palette)):
		distance = abs(r - palette[index][0]) + abs(g - palette[index][1]) + abs(b - palette[index][2])
		if distance < closestDistance:
			closestIndex = index
			closestDistance = distance
	return closestIndex


def extract_tile(image, x, y):
	colours = []
	for iy in range(8):
		for ix in range(8):
			(r, g, b) = image.getpixel((x * 8 + ix, y * 8 + iy))
			index = match_palette_colour(r, g, b)
			if index not in colours:
				colours.append(index)

	if len(colours) == 1:
		colours.insert(0, 0)
	elif len(colours) > 2:
		raise Exception("Tile %d,%d (%d,%d) has more than two colours" % (x, y, x * 8, y * 8))
	elif colours[0]&7 == colours[1]&7:
		raise Exception("Tile %d,%d (%d,%d) has bright and non-bright versions of the same colour" % (x, y, x * 8, y * 8))

	tile_data = {
		"ink": colours[0],
		"paper": colours[1],
		"bright": False,
		"bytes": []
	}

	for iy in range(8):
		byte = 0
		for ix in range(8):
			(r, g, b) = image.getpixel((x * 8 + ix, y * 8 + iy))
			index = match_palette_colour(r, g, b)
			byte <<= 1
			if index == colours[1]:
				byte |= 1
		tile_data["bytes"].append(byte)

	if tile_data["ink"] > 7 or tile_data["paper"] > 7:
		tile_data["ink"] &= 7
		tile_data["paper"] &= 7
		tile_data["bright"] = True

	return tile_data


def read_tiles_from_image(filename):
	# load the source image
	logging.info("Loading image '%s'.." % (filename))
	try:
		image = Image.open(filename)
	except:
		raise Exception("Couldn't load image %r" %  filename)
	logging.debug("Converting image..")
	rgb_image = image.convert(mode="RGB")

	# gather details about the image
	tiles_wide = int(image.width / 8)
	tiles_high = int(image.height / 8)
	if tiles_wide * 8 != image.width:
		raise Exception("Image width of %s is not a multiple of 8" % (image.width))
	if tiles_high * 8 != image.height:
		raise Exception("Image height of %s is not a multiple of 8" % (image.height))
	logging.info("Image is %dx%d tiles" % (tiles_wide, tiles_high))

	# read tiles to create tileset and tilemap
	logging.debug("Reading tile data..")
	tiles = []
	for y in range(tiles_high):
		for x in range(tiles_wide):
			tiles.append(extract_tile(rgb_image, x, y))

	return {
		"tiles": tiles,
		"width": tiles_wide,
		"height": tiles_high
	}


def create_tileset_and_collision(tileset, tilecollision):
	if (tileset["width"] != tilecollision["width"] or tileset["height"] != tilecollision["height"]):
		raise Exception("tileset and tilecollision are not the same size")

	collision_tiles = [
		{
			"ink": 0,
			"paper": 0,
			"bright": False,
			"bytes": [255,255,255,255,255,255,255,255]
		},
		{
			"ink": 0,
			"paper": 2,
			"bright": True,
			"bytes": [255,255,255,255,255,255,255,255]
		},
		{
			"ink": 0,
			"paper": 4,
			"bright": True,
			"bytes": [255,255,255,255,255,255,255,255]
		}
	]

	tiles_list = []
	collision_list = []
	i = 0
	for y in range(tileset["height"]):
		for x in range(tileset["width"]):
			tile = tileset["tiles"][i]
			collision = tilecollision["tiles"][i]
			if collision not in collision_tiles:
				raise Exception("Collision tile %d,%d (%d,%d) unknown" % (x, y, x * 8, y * 8))
			collision_index = collision_tiles.index(collision)
			tiles_list.append(tile)
			collision_list.append(collision_index)
			i += 1

	return {
		"tiles": tiles_list,
		"collision": collision_list
	}


def read_ldtk_world_file(filename, script_include_lines):
	# load the world file
	logging.info("Loading world file '%s'.." % (filename))
	try:
		file = open(filename)
		world = json.load(file)
	except Exception as e:
		raise Exception("Couldn't load world file %r: %s" % (filename, e))

	# get the level
	num_levels = len(world["levels"])
	if num_levels != 1:
		raise Exception("World has %d levels, expecting just 1" % (num_levels))
	level = world["levels"][0]

	# read in the entire tileset into a more useful format
	tiles_layer = list(filter(lambda layerInstance : layerInstance["__identifier"] == "Tiles", level["layerInstances"]))[0]
	tiles_wide = tiles_layer["__cWid"]
	tiles_high = tiles_layer["__cHei"]
	if tiles_wide != (4*32+4+1) or tiles_high != (2*24+2+1):
		raise Exception("Level is %dx%d tiles, expecting %dx%d" % (tiles_wide, tiles_high, (4*32*4+1), (2*24+2+1)))
	tiles = {repr(tile["px"]):tile for tile in tiles_layer["gridTiles"]}

	# build the rooms
	levels = []
	for level_y in range(2):
		for level_x in range(4):
			tilemap = []

			for y in range(24):
				for x in range(32):
					px = [(level_x*33 + x + 1) * 8, (level_y*25 + y + 1) * 8]
					tile = tiles[repr(px)]
					tile_id = tile["t"]
					tile_flip = tile["f"]
					if tile_flip != 0 and tile_flip != 1:
						raise Exception("Invalid tile flip in room %d,%d at %d,%d. Only X flip is supported" % (level_x, level_y, x, y))
					tilemap.append(
						{
							"tile_id": tile_id,
							"x_flip": tile_flip == 1
						}
					)

			levels.append(
				{
					"tilemap": tilemap,
					"include_script": [],
					"onEnter": {
						"auto_script": [],
						"user_script": [],
						"byte_code": []
					},
					"onTrigger": {
						"auto_script": [],
						"user_script": [],
						"byte_code": []
					}
				}
			)

	# read in the entities
	entities_layer = list(filter(lambda layerInstance : layerInstance["__identifier"] == "Entities", level["layerInstances"]))[0]
	for entity in entities_layer["entityInstances"]:
		entity_type = entity["__identifier"]
		rx = int((entity["__grid"][0] - 1) / (32+1))
		ry = int((entity["__grid"][1] - 1) / (24+1))
		tx = entity["__grid"][0] - (rx * (32+1) + 1)
		ty = entity["__grid"][1] - (ry * (24+1) + 1)
		tw = int(entity["width"] / 8)
		th = int(entity["height"] / 8)
		room_id = rx + ry*4
		logging.info("Found entity %s in room %d,%d at %d,%d." % (entity_type, rx, ry, tx, ty))

		if entity_type == "OnEnter":
			levels[room_id]["onEnter"]["user_script"].extend(entity["fieldInstances"][0]["__value"].splitlines())
		elif entity_type == "OnTrigger":
			levels[room_id]["onTrigger"]["user_script"].extend(entity["fieldInstances"][0]["__value"].splitlines())
		elif entity_type == "TriggerZone":
			id = entity["fieldInstances"][0]["__value"]
			auto_spawn = entity["fieldInstances"][1]["__value"]
			trigger_script = entity["fieldInstances"][2]["__value"]
			if trigger_script:
				trigger_script = trigger_script.splitlines()
			else:
				trigger_script = []
			levels[room_id]["include_script"].extend([
				"@number TRIGGERZONE_%d_ID %d" % (id, id),
				"@number TRIGGERZONE_%d_X %d" % (id, tx),
				"@number TRIGGERZONE_%d_Y %d" % (id, ty),
				"@number TRIGGERZONE_%d_W %d" % (id, tw),
				"@number TRIGGERZONE_%d_H %d" % (id, th)
			])
			if auto_spawn:
				levels[room_id]["onEnter"]["auto_script"].extend([
					"add_trigger(TRIGGERZONE_%d_ID, TRIGGERZONE_%d_X, TRIGGERZONE_%d_Y, TRIGGERZONE_%d_W, TRIGGERZONE_%d_H)" % (id, id, id, id, id)
				])
			if len(trigger_script) > 0:
				levels[room_id]["onTrigger"]["auto_script"].extend([
					"if trigger_id == TRIGGERZONE_%d_ID" % (id),
				])
				levels[room_id]["onTrigger"]["auto_script"].extend(trigger_script)
				levels[room_id]["onTrigger"]["auto_script"].extend([
					"endif",
				])
		elif entity_type == "Collision":
			id = entity["fieldInstances"][0]["__value"]
			auto_spawn = entity["fieldInstances"][1]["__value"]
			levels[room_id]["include_script"].extend([
				"@number COLLISION_%d_ID %d" % (id, id),
				"@number COLLISION_%d_X %d" % (id, tx),
				"@number COLLISION_%d_Y %d" % (id, ty),
				"@number COLLISION_%d_W %d" % (id, tw),
				"@number COLLISION_%d_H %d" % (id, th)
			])
			if auto_spawn:
				levels[room_id]["onEnter"]["auto_script"].extend([
					"add_collision(COLLISION_%d_X, COLLISION_%d_Y, COLLISION_%d_W, COLLISION_%d_H)" % (id, id, id, id)
				])
		elif entity_type == "Mob":
			id = entity["fieldInstances"][0]["__value"]
			mob_type = entity["fieldInstances"][1]["__value"]
			levels[room_id]["include_script"].extend([
				"@number MOB_%d_ID %d" % (id, id),
				"@number MOB_%d_X %d" % (id, tx * 8),
				"@number MOB_%d_Y %d" % (id, ty * 8),
				#"@number MOB_%d_TYPE %d" % (id, mob_type)
			])
			if auto_spawn:
				levels[room_id]["onEnter"]["auto_script"].extend([
					"spawn_mob(%s, MOB_%d_X, MOB_%d_Y)" % (mob_type, id, id)
				])
		else:
			raise Exception("Unknown entity type %s in room %d,%d at %d,%d" % (entity_type, rx, ry, tx, ty))

	# compile level scripts
	for level in levels:
		if len(level["onEnter"]["auto_script"]) or len(level["onEnter"]["user_script"]):
			logging.info('Compiling OnEnter script..')
			scriptgen.reset() # ugh!
			all_tokenised_lines = []
			all_tokenised_lines.extend(scriptgen.tokenise('include', script_include_lines))
			all_tokenised_lines.extend(scriptgen.tokenise('autoIncludes', level["include_script"]))
			all_tokenised_lines.extend(scriptgen.tokenise('auto', level["onEnter"]["auto_script"]))
			all_tokenised_lines.extend(scriptgen.tokenise('user', level["onEnter"]["user_script"]))
			byte_code = scriptgen.compile(all_tokenised_lines)
			logging.info("Compiled OnEnter script to %r bytes" % (len(byte_code)))
			level["onEnter"]["byte_code"] = byte_code
		if len(level["onTrigger"]["auto_script"]) or len(level["onTrigger"]["user_script"]):
			logging.info('Compiling OnTrigger script..')
			scriptgen.reset() # ugh!
			all_tokenised_lines = []
			all_tokenised_lines.extend(scriptgen.tokenise('include', script_include_lines))
			all_tokenised_lines.extend(scriptgen.tokenise('autoIncludes', level["include_script"]))
			all_tokenised_lines.extend(scriptgen.tokenise('auto', level["onTrigger"]["auto_script"]))
			all_tokenised_lines.extend(scriptgen.tokenise('user', level["onTrigger"]["user_script"]))
			byte_code = scriptgen.compile(all_tokenised_lines)
			logging.info("Compiled OnTrigger script to %r bytes" % (len(byte_code)))
			level["onTrigger"]["byte_code"] = byte_code

	return {
		"levels": levels
	}


def output(tileset_and_collision, world, tiledata_filename, tilemap_filename, scripts_filename, show_stats):
	# pack the tileset gfx
	tile_set = []
	for tile in tileset_and_collision["tiles"]:
		for i in range(8):
			tile_set.append(tile["bytes"][i])

	# pack the tileattrs
	tile_attrs = []
	for tile in tileset_and_collision["tiles"]:
		attr = tile["paper"] + (tile["ink"]<<3)
		if tile["bright"]:
			attr += 64
		tile_attrs.append(attr)

	# output stats?
	if show_stats:
		tile_counts_dict = {}
		for level in world["levels"]:
			for tile in level["tilemap"]:
				tile_index = tile["tile_id"]
				if tile_index in tile_counts_dict:
					tile_counts_dict[tile_index] += 1
				else:
					tile_counts_dict[tile_index] = 1
		tile_counts = []
		tile_count = len(tileset_and_collision["tiles"])
		for i in range(tile_count):
			if i in tile_counts_dict:
				tile_counts.append(tile_counts_dict[i])
			else:
				tile_counts.append(0)
		tile_counts.sort()
		tile_counts.reverse()
		tile_counts = collections.Counter(tile_counts)
		print('Tile usage counts:')
		for count in tile_counts:
			print(" %d tile(s) with %d use(s)" % (tile_counts[count], count))

	# write destination files
	logging.info("Compressing tileset, attributes and collision..")
	tile_data = []
	tile_data.extend(tile_set)
	tile_data.extend(tile_attrs)
	tile_data.extend(tileset_and_collision["collision"])
	tile_data_compressor = zx0.zx0_compress(tile_data)
	logging.info("Tile data compressed %r bytes -> %r bytes" % (len(tile_data), len(tile_data_compressor.output_data)))
	logging.info("Writing %s bytes to tileset file '%s'" % (len(tile_data_compressor.output_data), tiledata_filename))
	destination_file = open(tiledata_filename, "wb")
	destination_file.write(bytearray(tile_data_compressor.output_data))

	logging.info("Compressing all level tilemaps..")
	all_level_tilemaps = []
	for level in world["levels"]:
		for tile in level["tilemap"]:
			all_level_tilemaps.append(tile["tile_id"] + (128 if tile["x_flip"] else 0))
	tilemap_compressor = zx0.zx0_compress(all_level_tilemaps)
	logging.info("All level tilemaps compressed %r bytes -> %r bytes" % (len(world["levels"]) * len(world["levels"][0]["tilemap"]), len(tilemap_compressor.output_data)))

	logging.info("Writing %s bytes to tilemap file '%s'" % (len(tilemap_compressor.output_data), tilemap_filename))
	destination_file = open(tilemap_filename, "wb")
	destination_file.write(bytearray(tilemap_compressor.output_data))

	logging.info("Compressing all scripts..")
	all_level_scripts = []
	for level in world["levels"]:
		on_enter_byte_code = level["onEnter"]["byte_code"]
		on_trigger_byte_code = level["onTrigger"]["byte_code"]
		all_level_scripts.append(len(on_enter_byte_code))
		all_level_scripts.extend(on_enter_byte_code)
		all_level_scripts.append(len(on_trigger_byte_code))
		all_level_scripts.extend(on_trigger_byte_code)
#	scripts_compressor = zx0.zx0_compress(all_level_scripts)
#	logging.info("All level scripts compressed %r bytes -> %r bytes" % (len(all_level_scripts), len(scripts_compressor.output_data)))
	destination_file = open(scripts_filename, "wb")
#	destination_file.write(bytearray(scripts_compressor.output_data))
	destination_file.write(bytearray(all_level_scripts))


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="--- 8< --- Tiler --- 8< ---")
	parser.add_argument("source_tileset", help='source tileset image file (png, bitmap, etc.)')
	parser.add_argument("source_tilecollision", help='source tileset collision image file (png, bitmap, etc.)')
	parser.add_argument("source_ldtk", help='source ldtk world file')
	parser.add_argument("tiledata", help="tiledata destination file")
	parser.add_argument("tilemap", help="tilemap destination file")
	parser.add_argument("scripts", help="scripts destination file")
	parser.add_argument("-i", "--include", action="append", help='script include file', default=[])
	parser.add_argument("-s", "--stats", action="store_true", default=False, help="output stats")
	parser.add_argument("-v", "--verbose", action="count", default=0, help="turn on verbose output, use more to turn up the verbosity level")
	args = parser.parse_args()

	if args.verbose >= 2:
		logging.basicConfig(level=logging.DEBUG, format='[%(asctime)s] [%(levelname)s] [%(module)s] %(message)s')
	elif args.verbose >= 1:
		logging.basicConfig(level=logging.INFO, format='[%(levelname)s] [%(module)s] %(message)s')

	logging.info("Tiling %s, %s and %s to %s and %s" % (args.source_tileset, args.source_tilecollision, args.source_ldtk, args.tiledata, args.tilemap))
	try:
		logging.info("Reading script include files..")
		script_include_lines = []
		for filename in args.include:
			logging.info("Reading source %r" % (filename))
			source = open(filename, "rt")
			script_include_lines.extend(source.readlines())

		logging.info("Reading tileset tiles..")
		tileset = read_tiles_from_image(args.source_tileset)

		logging.info("Reading tileset colliison tiles..")
		tilecollision = read_tiles_from_image(args.source_tilecollision)

		logging.info("Creating tileset colliison data..")
		tileset_and_collision = create_tileset_and_collision(tileset, tilecollision)

		logging.info("Reading ldtk world file..")
		world = read_ldtk_world_file(args.source_ldtk, script_include_lines)

		logging.info("Writing results..")
		output(tileset_and_collision, world, args.tiledata, args.tilemap, args.scripts, args.stats)
	except Exception as e:
		logging.error("Failed: %s" % (e))
		sys.exit(-1)
	
	logging.info("Done")


