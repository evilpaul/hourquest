#! /usr/bin/env python3

import argparse
import functools



def make_tap(name, data, load_address):
	if len(name) > 10:
		raise Exception("Name must be 10 chars or less")

	def make_word(value):
		return [value & 0xff, (value >> 8) & 0xff]

	def wrap_data(flag, in_data):
		data = []
		data.extend(make_word(len(in_data) + 2))
		data.append(flag)
		data.extend(in_data)
		data.append(functools.reduce(lambda a,b: a^b, data[2:]))
		return data

	header = []
	header.append(0x03)
	header.extend(bytearray(name.ljust(10), 'utf-8'))
	header.extend(make_word(len(data)))
	header.extend(make_word(load_address))
	header.extend(make_word(32768))

	out_data = []
	out_data.extend(wrap_data(0x00, header))
	out_data.extend(wrap_data(0xff, data))

	return out_data



parser = argparse.ArgumentParser(description='bin 2 tap')
parser.add_argument('--source', type=str, help='the source file to use', required=True)
parser.add_argument('--destination', type=str, help='the destination file to write to', required=True)
parser.add_argument('--address', type=int, help='base address of code', required=True)
parser.add_argument('--name', type=str, help='name of the file', required=True)
args = parser.parse_args()

in_file = open(args.source, 'rb')
in_bytes = in_file.read()
in_file.close()

tap_data = make_tap(args.name, in_bytes, args.address)

out_file = open(args.destination,'wb') 
out_file.write(bytearray(tap_data))
out_file.close()
