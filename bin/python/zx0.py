class zx0_compress:
    def __init__(self, input_data):
        optimal = self.optimize(input_data, 0, 32640)
        self.compress(optimal, input_data, len(input_data), 0, 0)

    INITIAL_OFFSET = 1

    class BLOCK:
        __slots__ = 'bits', 'index', 'offset', 'length', 'chain'
        def __init__(self, bits, index, offset, length, chain):
            self.bits = bits
            self.index = index
            self.offset = offset
            self.length = length
            self.chain = chain

    def offset_ceiling(self, index, offset_limit):
        if index > offset_limit:
            return offset_limit
        elif index < self.INITIAL_OFFSET:
            return self.INITIAL_OFFSET
        else:
            return index

    def optimize(self, input_data, skip, offset_limit):
        input_size = len(input_data)
        best_length_size = 0
        bits = 0
        index = 0
        offset = 0
        length = 0
        bits2 = 0
        max_offset = self.offset_ceiling(input_size-1, offset_limit)

        precalculated_elias_gamma_bits = [(value.bit_length()-1)*2+1 for value in range(10000)]

        # allocate all main data structures at once
        last_literal = [None] * (max_offset+1)
        last_match = [None] * (max_offset+1)
        optimal = [None] * (input_size+1)
        match_length = [0] * (max_offset+1)
        best_length = [0] * (input_size+1)
        best_length[2] = 2

        # start with fake block
        last_match[self.INITIAL_OFFSET] = self.BLOCK(-1, skip-1, self.INITIAL_OFFSET, 0, None)

        # process remaining bytes
        for index in range(skip, input_size):
            best_length_size = 2
            max_offset = self.offset_ceiling(index, offset_limit)
            for offset in range(1, max_offset + 1):
                if index >= offset and input_data[index] == input_data[index-offset] and index != skip:
                    # copy from last offset
                    if last_literal[offset]:
                        length = index-last_literal[offset].index
                        bits = last_literal[offset].bits + 1 + precalculated_elias_gamma_bits[length]
                        last_match[offset] = self.BLOCK(bits, index, offset, length, last_literal[offset])
                        if not optimal[index] or optimal[index].bits > bits:
                            optimal[index] = last_match[offset]

                    # copy from new offset
                    match_length[offset] += 1
                    if match_length[offset] > 1:
                        if best_length_size < match_length[offset]:
                            bits = optimal[index-best_length[best_length_size]].bits + precalculated_elias_gamma_bits[best_length[best_length_size]-1]
                            while best_length_size < match_length[offset]:  # questionable change?
                                best_length_size += 1
                                bits2 = optimal[index-best_length_size].bits + precalculated_elias_gamma_bits[best_length_size-1]
                                if bits2 <= bits:
                                    best_length[best_length_size] = best_length_size
                                    bits = bits2
                                else:
                                    best_length[best_length_size] = best_length[best_length_size-1]

                        length = best_length[match_length[offset]]
                        bits = optimal[index-length].bits + 8 + precalculated_elias_gamma_bits[(((offset-1)>>7)+1)] + precalculated_elias_gamma_bits[length-1]
                        if not last_match[offset] or last_match[offset].index != index or last_match[offset].bits > bits:
                            last_match[offset] = self.BLOCK(bits, index, offset, length, optimal[index-length])
                            if not optimal[index] or optimal[index].bits > bits:
                                optimal[index] = last_match[offset]
                else:
                    # copy literals
                    match_length[offset] = 0
                    if last_match[offset]:
                        length = index-last_match[offset].index
                        bits = last_match[offset].bits + 1 + precalculated_elias_gamma_bits[length] + length*8
                        last_literal[offset] = self.BLOCK(bits, index, 0, length, last_match[offset])
                        if not optimal[index] or optimal[index].bits > bits:
                            optimal[index] = last_literal[offset]

        return optimal[input_size-1]



#unsigned char* output_data;
#int output_index;
#int input_index;
#int bit_index;
#int bit_mask;
#int diff;
#int backtrack;
    output_data = None
    output_index = 0
    input_index = 0
    bit_index = 0
    bit_mask = 0
    diff = 0
    backtrack = 0

#void read_bytes(int n, int *delta) {
#    input_index += n;
#    diff += n;
#    if (diff > *delta)
#        *delta = diff;
#}
    def read_bytes_return(self, n, delta):
        #global input_index, diff
        self.input_index += n
        self.diff += n
        if self.diff > delta:
            delta = self.diff
        return delta

#void write_byte(int value) {
#    output_data[output_index++] = value;
#    diff--;
#}
    def write_byte(self, value):
        #global output_data, output_index, diff
        self.output_data[self.output_index] = value
        self.output_index += 1
        self.diff -= 1

#void write_bit(int value) {
#    if (backtrack) {
#        if (value)
#            output_data[output_index-1] |= 1;
#        backtrack = FALSE;
#    } else {
#        if (!bit_mask) {
#            bit_mask = 128;
#            bit_index = output_index;
#            write_byte(0);
#        }
#        if (value)
#            output_data[bit_index] |= bit_mask;
#        bit_mask >>= 1;
#    }
    #}
    def write_bit(self, value):
        #global output_data, output_index, bit_index, bit_mask, diff, backtrack
        if self.backtrack:
            if value:
                self.output_data[self.output_index-1] |= 1
            self.backtrack = False
        else:
            if self.bit_mask == 0:
                self.bit_mask = 128
                self.bit_index = self.output_index
                self.write_byte(0)
            if value:
                self.output_data[self.bit_index] |= self.bit_mask
            self.bit_mask >>= 1

#void write_interlaced_elias_gamma(int value, int backwards_mode) {
#    int i;
#
#    for (i = 2; i <= value; i <<= 1)
#        ;
#    i >>= 1;
#    while ((i >>= 1) > 0) {
#        write_bit(backwards_mode);
#        write_bit(value & i);
#    }
#    write_bit(!backwards_mode);
#}
    def write_interlaced_elias_gamma(self, value, backwards_mode):
        i = 2
        while i <= value:
            i <<= 1
        i >>= 1
        while True:
            i >>= 1
            if i == 0:
                break
            self.write_bit(backwards_mode)
            self.write_bit(value & i)
        self.write_bit(not backwards_mode)

    #unsigned char *compress(BLOCK *optimal, unsigned char *input_data, int input_size, int skip, int backwards_mode, int *output_size, int *delta) {
    def compress(self, optimal, input_data, input_size, skip, backwards_mode):
        #global diff, input_index, output_data, output_index, bit_mask, backtrack

        c = optimal
        while c:
            #print(c.bits,c.index,c.length,c.offset)
            c = c.chain

    #   BLOCK *next;
    #   BLOCK *prev;
    #   int last_offset = INITIAL_OFFSET;
    #   int first = TRUE;
    #   int i;
        next = None
        prev = None
        last_offset = self.INITIAL_OFFSET
        first = True
        #i

    #
    #   /* calculate and allocate output buffer */
    #   *output_size = (optimal->bits+18+7)/8;
    #   output_data = (unsigned char *)malloc(*output_size);
    #   if (!output_data) {
    #        fprintf(stderr, "Error: Insufficient memory\n");
    #        exit(1);
    #   }
        output_size = int((optimal.bits+18+7)/8)
        self.output_data = [0] * output_size


    #
    #   /* initialize delta */
    #   diff = *output_size-input_size+skip;
    #   *delta = 0;
        self.diff = output_size-input_size+skip
        delta = 0


    #   /* un-reverse optimal sequence */
    #   next = NULL;
    #   while (optimal) {
    #       prev = optimal->chain;
    #       optimal->chain = next;
    #       next = optimal;
    #       optimal = prev;
    #   }
        next = None
        while (optimal):
            prev = optimal.chain
            optimal.chain = next
            next = optimal
            optimal = prev

    #
    #   input_index = skip;
    #   output_index = 0;
    #   bit_mask = 0;
        self.input_index = skip
        self.output_index = 0
        self.bit_mask = 0

    #
    #   for (optimal = next->chain; optimal; optimal = optimal->chain) {
        optimal = next.chain
        while True:
            if not optimal:
                break

    #       if (!optimal->offset) {
    #           /* copy literals indicator */
    #           if (first)
    #               first = FALSE;
    #           else
    #               write_bit(0);
            if optimal.offset == 0:
                if first:
                    first = False
                else:
                    self.write_bit(0)
    #
    #           /* copy literals length */
    #           write_interlaced_elias_gamma(optimal->length, backwards_mode);
                self.write_interlaced_elias_gamma(optimal.length, backwards_mode)

    #           /* copy literals values */
    #           for (i = 0; i < optimal->length; i++) {
    #               write_byte(input_data[input_index]);
    #               read_bytes(1, delta);
    #           }
                for i in range(0, optimal.length):
                    self.write_byte(input_data[self.input_index])
                    delta = self.read_bytes_return(1, delta)

    #       } else if (optimal->offset == last_offset) {
            elif optimal.offset == last_offset:
    #           /* copy from last offset indicator */
    #           write_bit(0);
                self.write_bit(0)
    #
    #           /* copy from last offset length */
    #           write_interlaced_elias_gamma(optimal->length, backwards_mode);
    #           read_bytes(optimal->length, delta);
                self.write_interlaced_elias_gamma(optimal.length, backwards_mode)
                delta = self.read_bytes_return(optimal.length, delta)

    #       } else {
            else:
    #           /* copy from new offset indicator */
    #           write_bit(1);
                self.write_bit(1)
    #
    #           /* copy from new offset MSB */
    #           write_interlaced_elias_gamma((optimal->offset-1)/128+1, backwards_mode);
                self.write_interlaced_elias_gamma(int((optimal.offset-1)/128+1), backwards_mode)
    #
    #           /* copy from new offset LSB */
    #           if (backwards_mode)
    #               write_byte(((optimal->offset-1)%128)<<1);
    #           else
    #               write_byte((255-((optimal->offset-1)%128))<<1);
    #           backtrack = TRUE;
                if backwards_mode:
                    self.write_byte(((optimal.offset-1)%128)<<1)
                else:
                    #print("wb3",optimal.offset)
                    self.write_byte(((255-((optimal.offset-1)%128))<<1) & 0xff)
                self.backtrack = True
    #
    #           /* copy from new offset length */
    #           write_interlaced_elias_gamma(optimal->length-1, backwards_mode);
    #           read_bytes(optimal->length, delta);
                self.write_interlaced_elias_gamma(optimal.length-1, backwards_mode)
                delta = self.read_bytes_return(optimal.length, delta)
    #
    #           last_offset = optimal->offset;
                last_offset = optimal.offset

            # end for..
            optimal = optimal.chain
    #       }
    #   }
    #
    #   /* end marker */
    #   write_bit(1);
    #   write_interlaced_elias_gamma(256, backwards_mode);
        self.write_bit(1)
        self.write_interlaced_elias_gamma(256, backwards_mode)

    #   return output_data;
        #return (output_data, delta)


import hashlib
#import time
if __name__ == "__main__":

    #(filename, expectedmd5) = ("main.rom", "774671ccc6841286291de66a46c1ce6b")
    #(filename, expectedmd5) = ("tilemap.bin", "55abf48c63c8b92539ca572a814a6b85")
    (filename, expectedmd5) = ("48.rom", "3ff0f9efd036e44243a1c10493663a97")

    fh = open("build/%s" % (filename), "rb")
    data = bytearray(fh.read())
    print(filename, len(data))

#    start = time.perf_counter()
    z = zx0_compress(data)
    result = z.output_data
#    end = time.perf_counter()
#    print("all", end-start)

    md5 = hashlib.md5(bytearray(result)).hexdigest()
    print(expectedmd5 == md5)

    fh = open("build/%s.pyzx0" % (filename), "wb")
    fh.write(bytearray(result))
