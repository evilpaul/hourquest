import argparse
from enum import Enum
import logging
import re
import sys



class ScriptException(Exception):
	def __init__(self, message, char = None, line = None, filename = None):
		self._message = message
		self._char = char
		self._line = line
		self._filename = filename



TokenType = Enum('TokenType',
	'IF ELIF ELSE ENDIF ' +
	'EQUALS NOT_EQUALS ' +
	'VARIABLE IMMEDIATE_NUMBER ' +
	'ASSIGNMENT '
	'NUMBER_FUNCTION USER_FUNCTION ' +
	'OPEN_BRACKET COMMA CLOSE_BRACKET ' +
	'MATHS_OP ' +
	'END_SCRIPT ' +
	#'RAW ' +
	'PREPROCESSOR ' +
	'EOL'
)

logic_functions = {
	"if": TokenType.IF,
	"elif": TokenType.ELIF,
	"else": TokenType.ELSE,
	"endif": TokenType.ENDIF
}
equalities = {
	"==": TokenType.EQUALS,
	"!=": TokenType.NOT_EQUALS
}

number_functions = {}
user_functions = {}
variableDefines = {}
numberDefines = {}


def reset(): # ugh!
	global number_functions, user_functions, variableDefines, numberDefines
	number_functions = {}
	user_functions = {}
	variableDefines = {}
	numberDefines = {}


class ByteCode:
	FUNC_END_OF_SCRIPT = 0<<1
	FUNC_IF_EQ = 1<<1
	FUNC_IF_NE = 2<<1
	FUNC_JUMP = 3<<1
	FUNC_SET = 4<<1

	NUMBER_SMALL_IMMEDIATE = 0<<6
	NUMBER_VAR = 1<<6
	NUMBER_BIG_IMMEDIATE = 2<<6
	NUMBER_FUNCTION = 3<<6



def tokenise_line(line):
	# strip comments and trailing space
	line = line.split('#')[0].rstrip()

	# tokenise the entire line
	index = 0
	tokens = []
	while index < len(line):
		# get next token
		value = ""
		state = "NEW"
		got_token = False
		start_index = index + 1
		while not got_token and state != "ERROR" and index < len(line):
			next_char = line[index]
			index += 1

			if state == "NEW":
				if next_char in [" ", "\t"]:
					start_index += 1
				elif next_char.isalpha():
					state = "STRING"
					value += next_char
				elif next_char.isdigit():
					state = "NUMBER"
					value += next_char
				elif next_char in "!=":
					state = "EQUALITY"
					value += next_char
				elif next_char == "(":
					state = "OPEN_BRACKET"
					value += next_char
					got_token = True
				elif next_char == ")":
					state = "CLOSE_BRACKET"
					value += next_char
					got_token = True
				elif next_char == ",":
					state = "COMMA"
					value += next_char
					got_token = True
				elif next_char in "+-":
					state = "MATHS_OP"
					value += next_char
					got_token = True
				elif next_char in "@":
					state = "PREPROCESSOR"
				else:
					raise ScriptException("Unknown character %r",  index)
			elif state == "STRING":
				if next_char.isalpha() or next_char == "_" or next_char.isdigit():
					value += next_char
				else:
					index -= 1
					got_token = True
			elif state == "PREPROCESSOR":
				if next_char.isalpha() or next_char == "_" or next_char.isdigit():
					value += next_char
				else:
					index -= 1
					got_token = True
			elif state == "NUMBER":
				if next_char.isdigit():
					value += next_char
				else:
					index -= 1
					got_token = True
			elif state == "EQUALITY":
				if next_char in "!=":
					value += next_char
				else:
					index -= 1
					got_token = True

		# what type of token is this really?
		token_type = None
		if state == "NUMBER":
			value = int(value)
			token_type = TokenType.IMMEDIATE_NUMBER
		elif state == "STRING":
			if value in logic_functions:
				token_type = logic_functions[value]
				value = None
			elif value in variableDefines:
				token_type = TokenType.VARIABLE
				value = variableDefines[value]
			elif value in number_functions:
				token_type = TokenType.NUMBER_FUNCTION
				value = value
			elif value in user_functions:
				token_type = TokenType.USER_FUNCTION
				value = value
			elif value == "end_script":
				token_type = TokenType.END_SCRIPT
				value = None
			elif value in numberDefines:
				token_type = TokenType.IMMEDIATE_NUMBER
				value = numberDefines[value]
		elif state == "PREPROCESSOR":
			token_type = TokenType.PREPROCESSOR
			value = value
		elif state == "EQUALITY":
			if value in equalities:
				token_type = equalities[value]
				value = None
			elif value == "=":
				token_type = TokenType.ASSIGNMENT
				value = None
		elif state == "OPEN_BRACKET":
			token_type = TokenType.OPEN_BRACKET
			value = None
		elif state == "COMMA":
			token_type = TokenType.COMMA
			value = None
		elif state == "CLOSE_BRACKET":
			token_type = TokenType.CLOSE_BRACKET
			value = None
		elif state == "MATHS_OP":
			token_type = TokenType.MATHS_OP
			value = value
		else:
			raise Exception("Unexpected state %r" % (state))


		if type(token_type) != TokenType:
			raise ScriptException("Token %r was not understood" % (value), start_index)
		elif token_type == TokenType.PREPROCESSOR:
			if len(tokens) != 0:
				raise ScriptException("Preprocessor directive %r must be first token on a line" % (value), start_index)
			if value == "number_function":
				rest_of_line = line[index:] 
				match = re.match("^\s+(\w+)\s+(\d+)\s+(\d+)$", rest_of_line)
				if match:
					name = match.group(1)
					if name in number_functions:
						raise ScriptException("Cannot redefine number_function %r" % (name), start_index)
					index = int(match.group(2))
					if index >= 32:
						raise ScriptException("Number function %r index %d out of range" % (name, index), start_index)
					arg_count = int(match.group(3))
					number_functions[name] = {
						"index": index,
						"arg_count": arg_count
					}
				else:
					raise ScriptException("Preprocessor number_function could not be parsed", start_index)
			elif value == "user_function":
				rest_of_line = line[index:] 
				match = re.match("^\s+(\w+)\s+(\d+)\s+(\d+)$", rest_of_line)
				if match:
					name = match.group(1)
					if name in user_functions:
						raise ScriptException("Cannot redefine user_function %r" % (name), start_index)
					index = int(match.group(2))
					if index >= 128:
						raise ScriptException("User function %r index %d out of range" % (name, index), start_index)
					arg_count = int(match.group(3))
					user_functions[name] = {
						"index": index,
						"arg_count": arg_count
					}
				else:
					raise ScriptException("Preprocessor user_function could not be parsed", start_index)
			elif value == "variable":
				rest_of_line = line[index:] 
				match = re.match("^\s+(\w+)\s+(\d+)$", rest_of_line)
				if match:
					name = match.group(1)
					if name in variableDefines:
						raise ScriptException("Cannot redefine variable %r" % (name), start_index)
					number = int(match.group(2))
					if number >= 64:
						raise ScriptException("Variable %r value %d out of range" % (name, number), start_index)
					variableDefines[name] = number
				else:
					raise ScriptException("Preprocessor variable could not be parsed", start_index)
			elif value == "number":
				rest_of_line = line[index:] 
				match = re.match("^\s+(\w+)\s+(\d+)$", rest_of_line)
				if match:
					name = match.group(1)
					if name in numberDefines:
						raise ScriptException("Cannot redefine number %r" % (name), start_index)
					number = int(match.group(2))
					numberDefines[name] = number
				else:
					raise ScriptException("Preprocessor number could not be parsed", start_index)
			else:
				raise ScriptException("Unexpected preprocessor directive %r" % (value), start_index)
			break
		else:
			# remember the result
			tokens.append({
				"type": token_type,
				"value": value,
				"source_char": start_index
			})

	# add an EOL token
	if (len(tokens) > 0):
		tokens.append({
			"type": TokenType.EOL,
			"value": None,
			"source_char": start_index
		})

	return tokens



def parse_line_tokens(line_tokens):
	token_index = 0
	def get_next_token():
		nonlocal token_index
		next_token = line_tokens[token_index]
		token_index = token_index + 1
		next_token_type = next_token["type"]
		return (next_token, next_token_type)
	def peek_next_token_type():
		nonlocal token_index
		return line_tokens[token_index]["type"]
	def get_args(arg_count):
		code = []
		next_token, next_token_type = get_next_token()
		if next_token_type != TokenType.OPEN_BRACKET:
			raise ScriptException("Expected open bracket", next_token["source_char"])
		for i in range(arg_count):
			code.extend(get_number())
			if i < (arg_count - 1):
				next_token, next_token_type = get_next_token()
				if next_token_type != TokenType.COMMA:
					raise ScriptException("Expected comma", next_token["source_char"])
		next_token, next_token_type = get_next_token()
		if (next_token_type != TokenType.CLOSE_BRACKET):
			raise ScriptException("Expected closed bracket", next_token["source_char"])
		return code
	def get_number():
		code = []
		next_token, next_token_type = get_next_token()
		if next_token_type == TokenType.IMMEDIATE_NUMBER:
			number_start = next_token["source_char"]
			value = next_token["value"]
			while peek_next_token_type() == TokenType.MATHS_OP:
				maths_op_token, _ = get_next_token()
				maths_op_type = maths_op_token["value"]
				next_token, next_token_type = get_next_token()
				if next_token_type != TokenType.IMMEDIATE_NUMBER:
					raise ScriptException("Expected number", next_token["source_char"])
				if maths_op_token["value"] == "+":
					value = value + next_token["value"]
				elif maths_op_token["value"] == "-":
					value = value - next_token["value"]
				else:
					raise ScriptException("Unexpected maths op %r" % (maths_op_type), maths_op_token["source_char"])
			if value < 0 or value > 255:
				raise ScriptException("Number %r out of range" % (value), number_start)
			if value < 64:
				code.append(ByteCode.NUMBER_SMALL_IMMEDIATE + value)
			else:
				code.append(ByteCode.NUMBER_BIG_IMMEDIATE)
				code.append(value)
		elif next_token_type == TokenType.VARIABLE:
			code.append(ByteCode.NUMBER_VAR + next_token["value"])
		elif next_token_type == TokenType.NUMBER_FUNCTION:
			number_function = number_functions[next_token["value"]]
			code.append(ByteCode.NUMBER_FUNCTION + (number_function["index"]<<1))
			code.extend(get_args(number_function["arg_count"]))
		else:
			raise ScriptException("Expected number, variable or number function", next_token["source_char"])
		return code

	code = []

	next_token, next_token_type = get_next_token()
	if next_token_type == TokenType.USER_FUNCTION:
		# --- USER_FUNCTION ---
		user_function = user_functions[next_token["value"]]
		code.append(user_function["index"]<<1)
		code.extend(get_args(user_function["arg_count"]))
	elif next_token_type == TokenType.END_SCRIPT:
		# --- END_SCRIPT ---
		code.append(ByteCode.FUNC_END_OF_SCRIPT)
	elif next_token_type == TokenType.VARIABLE:
		# --- VARIABLE ASSIGNMENT ---
		code.append(ByteCode.FUNC_SET)
		code.append(next_token["value"])
		next_token, next_token_type = get_next_token()
		if next_token_type != TokenType.ASSIGNMENT:
			raise ScriptException("Expected =", next_token["source_char"])
		code.extend(get_number())
	elif next_token_type == TokenType.IF:
		# --- IF ---
		left_side = get_number()
		next_token, next_token_type = get_next_token()
		if next_token_type == TokenType.EQUALS:
			test_code = ByteCode.FUNC_IF_EQ
		elif next_token_type == TokenType.NOT_EQUALS:
			test_code = ByteCode.FUNC_IF_NE
		else:
			raise ScriptException("Expected test condition", next_token["source_char"])
		right_side = get_number()
		code.append(test_code)
		code.extend(left_side)
		code.extend(right_side)
		code.append('IF')
	elif next_token_type == TokenType.ELIF:
		# --- ELIF ---
		left_side = get_number()
		next_token, next_token_type = get_next_token()
		if next_token_type == TokenType.EQUALS:
			test_code = ByteCode.FUNC_IF_EQ
		elif next_token_type == TokenType.NOT_EQUALS:
			test_code = ByteCode.FUNC_IF_NE
		else:
			raise ScriptException("Expected test condition", next_token["source_char"])
		right_side = get_number()
		code.append(ByteCode.FUNC_JUMP)
		code.append('ELIF')
		code.append(test_code)
		code.extend(left_side)
		code.extend(right_side)
		code.append('ELIF')
	elif next_token_type == TokenType.ELSE:
		code.append(ByteCode.FUNC_JUMP)
		code.append('ELSE')
	elif next_token_type == TokenType.ENDIF:
		# --- ENDIF ---
		pass
	else:
		raise ScriptException("Unexpected first token", next_token["source_char"])

	# check that we've consumed all tokens from this line
	next_token, next_token_type = get_next_token()
	if next_token_type != TokenType.EOL:
		raise ScriptException("Unexpected tokens after end of line", next_token["source_char"])

	return code



def resolve_jumps(lines):
	def find_next_matching(starting_line, token_types):
		nonlocal lines
		indent = 0
		for line_index in range(starting_line + 1, len(lines)):
			first_token_type = lines[line_index]["tokens"][0]["type"]
			if first_token_type == TokenType.IF:
				indent += 1
			elif indent == 0 and first_token_type in token_types:
				return (first_token_type, line_index)
			elif first_token_type in [TokenType.ENDIF]:
				indent -= 1
		return None

	for line_index in range(len(lines)):
		line = lines[line_index]
		first_token = line["tokens"][0]
		first_token_type = first_token["type"]
		if first_token_type == TokenType.IF:
			# --- IF ---
			next_block_token_type, next_block_line = find_next_matching(line_index, [TokenType.ELIF, TokenType.ELSE, TokenType.ENDIF])
			if next_block_line == None:
				raise ScriptException("IF without matching ENDIF", first_token["source_char"], line["source_line"])
			offset = 0
			for i in range(line_index + 1, next_block_line):
				offset += len(lines[i]["code"])
			if next_block_token_type in [TokenType.ELIF, TokenType.ELSE]:
				offset += 2
			line["code"][-1] = offset
		elif first_token_type == TokenType.ELIF:
			# --- ELIF ---
			next_block_token_type, next_block_line = find_next_matching(line_index, [TokenType.ENDIF]) # add ELIF & ELSE?
			if next_block_line == None:
				raise ScriptException("ELIF without matching ENDIF", first_token["source_char"], line["source_line"])
			offset = 0
			for i in range(line_index, next_block_line + 1):
				offset += len(lines[i]["code"])
			offset -= 2
			line["code"][1] = offset

			next_block_token_type, next_block_line = find_next_matching(line_index, [TokenType.ELIF, TokenType.ELSE, TokenType.ENDIF])
			if next_block_line == None:
				raise ScriptException("ELIF without matching ENDIF", first_token["source_char"], line["source_line"])
			offset = 0
			for i in range(line_index + 1, next_block_line + 1):
				offset += len(lines[i]["code"])
			line["code"][-1] = offset
		elif first_token_type == TokenType.ELSE:
			# --- ELSE ---
			next_block_token_type, next_block_line = find_next_matching(line_index, [TokenType.ENDIF])
			if next_block_line == None:
				raise ScriptException("ELSE without matching ENDIF", first_token["source_char"], line["source_line"])
			offset = 0
			for i in range(line_index + 1, next_block_line + 1):
				offset += len(lines[i]["code"])
			line["code"][1] = offset



def check_end_script(lines):
	indent = 0
	end_found = False
	for line_index in range(len(lines)):
		first_token_type = lines[line_index]["tokens"][0]["type"]
		if first_token_type == TokenType.IF:
			indent += 1
		elif first_token_type in [TokenType.ENDIF]:
			indent -= 1
		elif indent == 0 and first_token_type == TokenType.END_SCRIPT:
			end_found = True
			if line_index != len(lines) - 1:
				logging.warning("Extra code found after end_script on line %d" % (lines[line_index]["source_line"]))
			break

	# add end_of_script if one wasn't found
	if not end_found:
		lines.append({
			"code": [ByteCode.FUNC_END_OF_SCRIPT]
		})



def tokenise(source_filename, source_lines):
	# read the code and parse into tokens
	lines = []
	source_line_number = 1
	for source_line in source_lines:
		try:
			tokens = tokenise_line(source_line)
		except ScriptException as e:
			raise ScriptException(e._message, e._char, source_line_number)
		if len(tokens):
			lines.append({
				"tokens": tokens,
				"source_filename": source_filename,
				"source_line": source_line_number
			})
		source_line_number += 1

	logging.debug("Generated tokens results:")
	for line in lines:
		logging.debug(line)

	return lines



def compile(lines):
	# first pass - compile into code
	logging.info("Pass 1")
	for line in lines:
		try:
			line["code"] = parse_line_tokens(line["tokens"])
		except ScriptException as e:
			raise ScriptException(e._message, e._char, line["source_line"], line["source_filename"])
	logging.debug("First path codegen results:")
	for line in lines:
		logging.debug(line)

	# second pass - resolve jumps
	logging.info("Pass 2")
	try:
		resolve_jumps(lines)
		check_end_script(lines)
	except ScriptException as e:
		raise ScriptException(e._message, e._char, e._line)

	# write out the code
	code = []
	for line in lines:
		code.extend(line["code"])
		logging.debug(line["code"])
	logging.debug("Final code:")
	logging.debug(code)
	return code



if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Script Compiler")
	parser.add_argument("-s", "--source", action="append", help='source file')
	parser.add_argument("-d", "--destination", help="destination file")
	parser.add_argument("-v", "--verbose", action="count", default=0, help="turn on verbose output, use more to turn up the verbosity level")
	args = parser.parse_args()

	if args.verbose >= 2:
		logging.basicConfig(level=logging.DEBUG, format='[%(asctime)s] [%(levelname)s] [%(module)s] %(message)s')
	elif args.verbose >= 1:
		logging.basicConfig(level=logging.INFO, format='[%(levelname)s] [%(module)s] %(message)s')

	logging.info("Compiling %s to %s" % (args.source, args.destination))
	
	logging.info("Tokenising..")
	all_tokenised_lines = []
	for filename in args.source:
		try:
			logging.info("Reading and tokenising source %r" % (filename))
			source = open(filename, "rt")
			source_lines = source.readlines()
			all_tokenised_lines.extend(tokenise(filename, source_lines))
		except ScriptException as e:
			logging.error("Compile error: %s@%d.%d %s" % (filename, e._line, e._char, e._message))
			sys.exit(1)

	logging.info("Compiling..")
	try:
		result = compile(all_tokenised_lines)
	except ScriptException as e:
		logging.error("Compile error: %s@%d.%d %s" % (e._filename, e._line, e._char, e._message))
		sys.exit(1)

	logging.info("Writing %s bytes to '%s'" % (len(result), args.destination))
	destination_file = open(args.destination, "wb")
	destination_file.write(bytearray(result))


