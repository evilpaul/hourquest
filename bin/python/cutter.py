import argparse
import logging
import sys
from PIL import Image



def convert(source_filename, destination_filename):
	zigzag = False
	mask = False

	# load the source image
	logging.info("Loading source image '%s'.." % (source_filename))
	try:
		source_image = Image.open(source_filename)
	except:
		raise Exception("Couldn't load source image %r" %  source_filename)
	logging.debug("Converting source image..")
	rgb_image = source_image.convert(mode="RGB")
	bw_image = source_image.convert(mode="1", dither=Image.NONE)

	# gather details about the source image
	bytes_wide = int(source_image.width / 8)
	if bytes_wide * 8  != source_image.width:
		raise Exception("Image width of %s is not a multiple of 8" % (source_image.width))
	height = source_image.height
	logging.info("Source image is %d bytes wide by %d pixels high" % (bytes_wide, height))

	# create binary data
	logging.debug("Reading sprite data..")
	binary_data = []
	for y in range(height):
		reverse = zigzag and (y & 1)
		row = []
		for x_byte in range(bytes_wide):
			data_byte = 0
			mask_byte = 0
			for x in range(x_byte * 8, (x_byte + 1) * 8):
				r, g, b = rgb_image.getpixel((x, y))
				is_mask = (r, g, b) == (255, 0, 255)
				mask_byte = (mask_byte << 1) + (1 if is_mask else 0)
				if is_mask:
					data_pixel = 0
				else: 
					data_pixel = bw_image.getpixel((x, y))
				data_byte = (data_byte << 1) + (1 if data_pixel >= 128 else 0)
			if reverse:
				row.insert(0, data_byte)
				if mask:
					row.insert(0, mask_byte)
			else:
				if mask:
					row.append(mask_byte)
				row.append(data_byte)
		binary_data.extend(row)

	# write destination file
	logging.info("Writing %s bytes to destination file '%s'" % (len(binary_data), destination_filename))
	destination_file = open(destination_filename, "wb")
	destination_file.write(bytearray(binary_data))



if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="--- 8< --- Spare us the cutter --- 8< ---")
	parser.add_argument("source", help='source image file (png, bitmap, etc.)')
	parser.add_argument("destination", help="destination file")
	parser.add_argument("-v", "--verbose", action="count", default=0, help="turn on verbose output, use more to turn up the verbosity level")
	args = parser.parse_args()

	if args.verbose >= 2:
		logging.basicConfig(level=logging.DEBUG, format='[%(asctime)s] [%(levelname)s] [%(module)s] %(message)s')
	elif args.verbose >= 1:
		logging.basicConfig(level=logging.INFO, format='[%(levelname)s] [%(module)s] %(message)s')

	logging.info("Cutting %s to %s" % (args.source, args.destination))
	logging.debug("About to convert..")
	try:
		convert(args.source, args.destination)
	except Exception as e:
		logging.error("Failed: %s" % (e))
		sys.exit(-1)
	
	logging.info("Done")


